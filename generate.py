# https://gitlab.com/strongrandom/awesome-fedora
#
# This is a quick-and-dirty tool to convert packages.yml into README.md. It is not pretty. It is not efficient. It is
# nowhere near my best work... but it works.
#
# Use Python 3. Install PyYAML. (sudo dnf install python3-pyyaml)
#
import operator
import textwrap

import dnf
import yaml


def write_packages(file, dictionary: dict):
    if len(dictionary) > 0:

        packages = "dnf install "
        for wp_key_package_name in sorted(dictionary, key=operator.itemgetter(0)):
            packages += wp_key_package_name + " "

        file.write("\n```\n")

        wp_wrap = textwrap.TextWrapper(break_long_words=False, break_on_hyphens=False, width=70,
                                       initial_indent='', subsequent_indent='  ')

        wp_lines = wp_wrap.wrap(packages)
        count = 0
        for wp_line in wp_lines:
            file.write(wp_line)

            if len(wp_lines) > 1 and count < len(wp_lines) - 1:
                file.write(" \\\n")
            else:
                file.write("\n")

            count += 1

        file.write("```\n\n")

        file.write("[back to index](#index)\n\n")


def parse_yaml(yaml_in, output):
    for key_category, value_dict_packages in yaml_in:

        if "__index" in key_category:
            # *__index indicates where to put the index of groups
            build_index(output, yaml_in)

        elif "__" in key_category:
            # __ in the key indicates plain text to output
            output.write("{}\n\n".format(value_dict_packages))

        elif type(value_dict_packages) is dict:
            # otherwise, if a dict, process the group
            output.write("***\n## {}\n".format(key_category))
            build_group(output, value_dict_packages)


def build_group(output, value_dict_packages):
    dict_group = dict()
    dict_packages_sorted = sorted(value_dict_packages.items(), key=operator.itemgetter(0))
    for key_package_name, value_package_attributes in dict_packages_sorted:

        if "__" in key_package_name:
            output.write("\n  {}\n\n".format(value_package_attributes))
        else:
            dnf_result = dnf_query_available.filter(name=key_package_name)

            if len(dnf_result) > 0:
                # Ignore multiple results, just take the first (dnf.result[0])
                build_package_entry(dict_group, dnf_result[0], output, key_package_name, value_package_attributes)
            else:
                print("  Package not found in DNF:", key_package_name)

    write_packages(output, dict_group)


def build_package_entry(dict_group, dnf_package, output, key_package_name, value_package_attributes):
    description = dnf_package.description
    url = dnf_package.url

    # Look up the repository and mark those from external repos
    repository = dnf_package.repo.id
    if repository == "fedora":
        repository = ""
    elif repository == "updates":
        repository = ""
    else:
        repository = "{} ".format(repository)
    if type(value_package_attributes) is dict:
        if type(value_package_attributes.get("description")) is str:
            description = value_package_attributes.get("description")

        if type(value_package_attributes.get("url")) is str:
            url = value_package_attributes.get("url")

        if value_package_attributes.get("essential"):
            dict_essential[key_package_name] = ""

            # Hack in essential package check mark
            repository = "\u2713 " + repository
    output.write("  * **{}[{}]({}): {}**\n".format(repository, key_package_name, url,
                                                   dnf_package.summary))
    # Process the description field. Ugly, but works.
    description = description.strip()
    description = description.replace("\n", " ")

    while "  " in description:
        description = description.replace('  ', ' ')

    description = description.replace('`', "'")
    description = description.replace('*', "'")
    description = description.replace('>', "&gt;")
    description = description.replace('<', "&lt;")

    # Wrap and write out the description
    wrap = textwrap.TextWrapper(width=70, max_lines=10,
                                initial_indent='      ', subsequent_indent='      ')
    lines = wrap.wrap(description)
    output.write("\n")
    for line in lines:
        output.write(line)
        output.write("\n")
    output.write("\n")

    # Add to the dictionaries
    dict_all[key_package_name] = ""
    dict_group[key_package_name] = ""


def build_index(output, yaml_in):
    # Hack to build an index
    output.write("\n")
    output.write("## Index\n")
    for index_category, _ in yaml_in:
        if "_" not in index_category:
            index_link = str.lower(index_category)
            index_link = index_link.replace(' ', '-')
            index_link = index_link.replace('/', '')
            index_link = index_link.replace('+', '')
            output.write("- [{}](#{})\n".format(index_category, index_link))
    output.write("\n")


if __name__ == '__main__':
    print("Loading YAML ...")
    stream = open('packages.yml', 'r')
    yaml_data = yaml.load(stream, Loader=yaml.SafeLoader)

    file_output = open('README.md', 'w')

    dict_all = dict()
    dict_essential = dict()

    with dnf.Base() as dnf_base:
        print("Setting up DNF ...")

        # DNF boilerplate, see http://dnf.readthedocs.io/en/latest/use_cases.html#id3
        dnf_base.read_all_repos()
        dnf_base.fill_sack()
        dnf_query = dnf_base.sack.query()
        dnf_query_available = dnf_query.available()

        yaml_data_sorted = sorted(yaml_data.items(), key=operator.itemgetter(0))

        print("Parsing ...")
        parse_yaml(yaml_data_sorted, file_output)

    file_output.write("# Everything #\n")
    file_output.write("\nAll {} packages:\n".format(len(dict_all)))
    write_packages(file_output, dict_all)

    if len(dict_essential) > 0:
        file_output.write("# Essential #\n")
        write_packages(file_output, dict_essential)

    file_output.close()
