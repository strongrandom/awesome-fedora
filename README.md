# Awesome Fedora #

This is yet another curated list on GitLab. This one is a list of useful,
unusual, esoteric, and odd packages available in Fedora.

This list comes from several years of notes. It is highly subjective. Some
of the packages are ancient. Some will be preinstalled on a default
installation. While I have used many of these applications, there are plenty
I have not. If you find one that&apos;s terrible, remove it, and send a pull
request letting me know that it sucks.

Notable omissions are applications that are installed by default and show up
in the applications list (i.e. why Firefox is not listed under web browsers).

A few packages, notably multimedia related come from the RPM Fusion
repository. Non-default repositories are marked   in **bold**. See
[RPM Fusion - Configuration](http://rpmfusion.org/Configuration) to enable
RPM Fusion.

At the end you will find an [Essential](#essential) list. These are
packages I install after every fresh install of Fedora. They are essential
to me, but may not be to you. These are also marked with a UTF-8 check
mark: ✓.

I hope this is useful. If you have any suggestions, please submit a pull
request to the YAML file, as this README is generated from it.


## Index
- [Amateur Radio](#amateur-radio)
- [Backup Tools](#backup-tools)
- [Benchmarks](#benchmarks)
- [Chat](#chat)
- [Compression](#compression)
- [Databases and Tools](#databases-and-tools)
- [Development Tools](#development-tools)
- [Development Tools - Build Essentials](#development-tools---build-essentials)
- [Development Tools - C/C++](#development-tools---cc)
- [Diagrams and Drawing](#diagrams-and-drawing)
- [Document Processing](#document-processing)
- [Editors](#editors)
- [Electronics](#electronics)
- [Email](#email)
- [Embedded Development Tools](#embedded-development-tools)
- [Emulators](#emulators)
- [File Managers and Utilities](#file-managers-and-utilities)
- [File Systems](#file-systems)
- [Mathematics](#mathematics)
- [Miscellaneous](#miscellaneous)
- [Multimedia](#multimedia)
- [Multimedia - Audio](#multimedia---audio)
- [Multimedia - Codecs](#multimedia---codecs)
- [Multimedia - Codecs for GStreamer](#multimedia---codecs-for-gstreamer)
- [Multimedia - Images/Photography](#multimedia---imagesphotography)
- [Multimedia - Music](#multimedia---music)
- [Multimedia - Retro/Synthesizers](#multimedia---retrosynthesizers)
- [Multimedia - Video](#multimedia---video)
- [Network Tools](#network-tools)
- [Network Tools - Diagnostics](#network-tools---diagnostics)
- [Network Tools - Monitoring](#network-tools---monitoring)
- [Network Tools - Remote Desktop/Access](#network-tools---remote-desktopaccess)
- [Network Tools - Tunneling/VPNs/Proxies](#network-tools---tunnelingvpnsproxies)
- [Note Taking](#note-taking)
- [Optical Media Tools](#optical-media-tools)
- [Parallel and Distributed Computing](#parallel-and-distributed-computing)
- [Productivity](#productivity)
- [Scientific](#scientific)
- [Security and Forensics](#security-and-forensics)
- [Smart Cards](#smart-cards)
- [System Tools](#system-tools)
- [System Tools - Performance and Tuning](#system-tools---performance-and-tuning)
- [System Tools - Sensors and Monitoring](#system-tools---sensors-and-monitoring)
- [System Tools - Serial Ports](#system-tools---serial-ports)
- [TeX/LaTeX](#texlatex)
- [Terminals](#terminals)
- [VoIP](#voip)
- [Web](#web)
- [Web - Archival](#web---archival)
- [Web - Browsers](#web---browsers)
- [Web - Command Line Tools](#web---command-line-tools)
- [Web - Servers and Server Tools](#web---servers-and-server-tools)
- [Web - Text Mode Browsers](#web---text-mode-browsers)

***
## Amateur Radio

  Various packages useful for amateur radio operators, software defined radio, etc.

  * **[LinLog](http://linlogbook.sourceforge.net/): A ham radio logbook for Linux**

      LinLogBook is a highly configurable amateur radio logbook for
      Linux. It uses an sql-database to store its data. For the ease
      of use sqlite 3 is used but it should be possible to use other
      databases like mysql, for instance.

  * **[aldo](http://aldo.nongnu.org/): A morse tutor**

      Aldo is a morse code learning tool released under GPL, which
      provides four type of training methods: 1. Classic exercise :
      Identify random characters played in morse code. 2. Koch method
      : Two morse characters will be played at full speed (20wpm)
      until you'll be able to identify at least 90 percent of them.
      After that, one more character will be added, and so on. 3. Read
      from file : Identify the morse code generated from a file. 4.
      Callsign exercise : Identify random callsigns played in morse
      code.

  * **[aprsd](http://sourceforge.net/projects/aprsd/): Internet gateway and client access to amateur radio APRS packet data**

      APRSd is an APRS server program that uses amateur radio and
      internet services to convey GPS mapping, weather, and positional
      data. It has been developed by and for amateur radio enthusiasts
      to provide real-time data in an easy to use package.

  * **[aprsdigi](https://github.com/n2ygk/aprsdigi/releases): AX.25 Automatic Position Reporting System**

      Aprsdigi is a specialized Amateur Packet Radio (AX.25) UI-frame
      digipeater for the Automatic Position Reporting Systems,
      APRS(tm). It uses the Linux kernel AX.25 network stack as well
      as the SOCK_PACKET facility to listen for packets on one or more
      radio interfaces (ports) and repeat those packets -- with
      several possible modifications -- on the same or other
      interfaces. Aprsdigi can also use the Internet to tunnel
      connections among other APRS digipeaters and nodes using IPv4 or
      IPv6 UDP unicast or multicast.

  * **[ax25-apps](http://ax25.sourceforge.net/): AX.25 ham radio applications**

      This package provides specific user applications for hamradio
      that use AX.25 Net/ROM or ROSE network protocols: ' axcall: a
      general purpose AX.25, NET/ROM and ROSE connection program. '
      axlisten: a network monitor of all AX.25 traffic heard by the
      system. ' ax25ipd: an RFC1226 compliant daemon which provides
      encapsulation of AX.25 traffic over IP. ' ax25mond: retransmits
      data received from sockets into an AX.25 monitor socket.

  * **[callgit](http://www.hamsoftware.org/): A tool for Ham Radio Operators to look up call-signs on the web**

      This program allows you to search for another Ham's name and
      address without having a web browser up. It is very simple.

  * **[cwdaemon](http://cwdaemon.sourceforge.net): Morse daemon for the parallel or serial port**

      Cwdaemon is a small daemon which uses the pc parallel or serial
      port and a simple transistor switch to output morse code to a
      transmitter from a text message sent to it via the udp internet
      protocol.

  * **[fbb](https://sourceforge.net/projects/linfbb/): Packet radio mailbox and utilities**

      F6FBB BBS software for bulletins and messages distribution via
      Packet Radio and wired networks.

  * **[fldigi](http://www.w1hkj.com/Fldigi.html): Digital modem program for Linux**

      Fldigi is a modem program which supports most of the digital
      modes used by ham radio operators today. You can also use the
      program for calibrating your sound card to WWV or doing a
      frequency measurement test. The program also comes with a CW
      decoder. fldigi is written with the help of the Fast Light
      Toolkit X GUI. Fldigi is a fast moving project many added
      features with each update. Flarq (Fast Light Automatic Repeat
      Request) is a file transfer application that is based on the ARQ
      specification developed by Paul Schmidt, K9PS. It is capable of
      transmitting and receiving frames of ARQ data via fldigi.

  * **[fllog](http://w1hkj.com/fllog-help/index.html): Amateur Radio Log Program**

      Fllog is a transceiver control program for Amateur Radio use. It
      does not use any 3rd party transceiver control libraries. It is
      a c++ pro- gram that encapsulates each transceiver in its own
      class. Where ever possible the transceiver class(s) use
      polymorphism to reuse code that is portable across a series of
      transceivers.

  * **[flmsg](http://www.w1hkj.com/): Fast Light Message Amateur Radio Forms Manager**

      flmsg is a editor / file management tool for ics213 forms which
      form the basis for emergency communications data transfers.

  * **[gnuradio](http://www.gnuradio.org): Software defined radio framework**

      GNU Radio is a collection of software that when combined with
      minimal hardware, allows the construction of radios where the
      actual waveforms transmitted and received are defined by
      software. What this means is that it turns the digital
      modulation schemes used in today's high performance wireless
      devices into software problems.

  * **[gnuradio-examples](http://www.gnuradio.org): GNU Radio**

      GNU Radio examples

  * **[gpredict](http://gpredict.oz9aec.net/): Real-time satellite tracking and orbit prediction program**

      Gpredict is a real time satellite tracking and orbit prediction
      program written using the Gtk+ widgets. Gpredict is targeted
      mainly towards ham radio operators but others interested in
      satellite tracking may find it useful as well. Gpredict uses the
      SGP4/SDP4 algorithms, which are compatible with the NORAD
      Keplerian elements.

  * **[gqrx](http://gqrx.dk/): Software defined radio receiver powered by GNU Radio and Qt**

      Gqrx is a software defined radio receiver powered by the GNU
      Radio SDR framework and the Qt graphical toolkit.

  * **[gr-air-modes](http://github.com/bistromath/gr-air-modes): SDR receiver for Mode S transponder signals (ADS-B)**

      Software defined radio receiver for Mode S transponder signals,
      including ADS-B reports.

  * **[gr-fcdproplus](https://github.com/dl1ksv/gr-fcdproplus): GNURadio support for FUNcube Dongle Pro+**

      GNURadio support for FUNcube Dongle Pro+.

  * **[gr-iqbal](http://cgit.osmocom.org/gr-iqbal/): GNURadio block for suppressing IQ imbalance**

      This GNURadio block can suppress IQ imbalance in the RX path of
      quadrature receivers.

  * **[gr-osmosdr](http://sdr.osmocom.org/trac/wiki/GrOsmoSDR): Common software API for various radio hardware**

      Primarily gr-osmosdr supports the OsmoSDR hardware, but it also
      offers a wrapper functionality for FunCube Dongle, Ettus UHD and
      rtl-sdr radios. By using gr-osmosdr source you can take
      advantage of a common software api in your application(s)
      independent of the underlying radio hardware.

  * **[gr-rds](https://github.com/bastibl/gr-rds): GNU Radio FM RDS Receiver**

      GNU Radio FM RDS Receiver.

  * **[grig](http://groundstation.sourceforge.net/grig/): A Ham Radio Control graphical user interface**

      Grig is a graphical user interface for the Ham Radio Control
      Libraries. It is intended to be simple and generic, presenting
      the user to the same interface regardless of which radio he or
      she uses.

  * **[hackrf](https://greatscottgadgets.com/hackrf/): HackRF Utilities**

      Hardware designs and software for HackRF, a project to produce a
      low cost, open source software radio platform.

  * **[hamlib](http://www.hamlib.org): Run-time library to control radio transceivers and receivers**

      Hamlib provides a standardized programming interface that
      applications can use to send the appropriate commands to a
      radio. Also included in the package is a simple radio control
      program 'rigctl', which lets one control a radio transceiver or
      receiver, either from command line interface or in a text-
      oriented interactive interface.

  * **[klog](http://www.klog.xyz): A Ham radio logging program for KDE**

      KLog is a Ham radio logging program for KDE Some features
      include: ' DXCC award support. ' Basic IOTA support. ' Importing
      from Cabrillo files. ' Importing from TLF. ' Adding/Editing
      QSOs. ' Save/read to/from disk file the log - ADIF format by
      default. ' English/Spanish/Portuguese/Galician/Serbian/Swedish
      support. ' QSL sent/received support. ' Read/Write ADIF. '
      Delete QSOs. ' DX-Cluster support. Some additional features of
      this application are still under development and are not yet
      implemented.

  * **[linsmith](http://jcoppens.com/soft/linsmith/index.en.php): A Smith charting program**

      linSmith is a Smith Charting program. It's main features are: '
      Definition of multiple load impedances ' Addition of discrete
      and line components ' A 'virtual' component switches from
      impedance to admittance to help explaining parallel components '
      The chart works in real impedances ' Load and circuit
      configuration is stored separately, permitting several solutions
      without re-defining the other

  * **[lpsk31](http://www.5b4az.org/): A ncurses application for ham radio communications using PSK31 digital mode**

      lpsk31 is a ncurses console application for ham radio
      communications in the popular PSK31 digital mode. lpsk31 uses
      only integer arithmetic for both signal detection and audio tone
      synthesis, so that it needs no floating point calculations for
      its operation. lpsk31 can keep a log of QSO's in text and ADIF
      format as well as a raw log of all that is typed in the transmit
      window or displayed in the receive window.

  * **[qsstv](http://users.telenet.be/on4qz/): Qt-based slow-scan TV and fax**

      Qsstv is a program for receiving slow-scan television and fax.
      These are modes used by hamradio operators. Qsstv uses a
      soundcard to send and receive images.

  * **[rtl-sdr](http://sdr.osmocom.org/trac/wiki/rtl-sdr): SDR utilities for Realtek RTL2832 based DVB-T dongles**

      This package can turn your RTL2832 based DVB-T dongle into a SDR
      receiver.

  * **[rtlsdr-scanner](http://eartoearoak.com/software/rtlsdr-scanner): Frequency scanning GUI for RTL2832 based DVB-T dongles**

      Frequency scanning GUI for RTL2832 based DVB-T dongles. In other
      words a cheap, simple Spectrum Analyser.

  * **[soundmodem](http://gna.org/projects/soundmodem): Soundcard Packet Radio Modem**

      This package contains the driver and the diagnostic utility for
      userspace SoundModem. It allows you to use soundcards as Amateur
      Packet Radio modems.

  * **[splat](http://www.qsl.net/kd2bd/splat.html): Analyze point-to-point terrestrial RF communication links**

      SPLAT! is a Surface Path Length And Terrain analysis application
      written for Linux and Unix workstations. SPLAT! analyzes point-
      to-point terrestrial RF communication links, and provides
      information useful to communication system designers and site
      engineers.

  * **[trustedqsl](http://sourceforge.net/projects/trustedqsl/): Tool for digitally signing Amateur Radio QSO records**

      The TrustedQSL applications are used for generating digitally
      signed QSO records (records of Amateur Radio contacts). This
      package contains the GUI applications tqslcert and tqsl.

  * **[tucnak2](http://tucnak.nagano.cz/wiki/Main_Page): VHF contest logging program**

      Tucnak2 is VHF/UHF/SHF log for hamradio contests. It supports
      multi bands, free input, networking, voice and CW keyer, WWL
      database and much more.

  * **[twlog](http://wa0eir.home.mchsi.com/twlog.html): Records basic ham radio log information**

      Twlog records basic Ham log information. It was written for day
      to day logging, not contesting. There are no dupe checks or
      contest related features.

  * **[unixcw](http://unixcw.sourceforge.net): Shared library for Morse programs**

      unixcw is a project providing libcw library and a set of
      programs using the library: cw, cwgen, cwcp and xcwcp. The
      programs are intended for people who want to learn receiving and
      sending Morse code. unixcw is developed and tested on GNU/Linux
      system.

  * **[uronode](http://uronode.sourceforge.net): Alternative packet radio system for Linux**

      URONode is an alternative packet radio system for Linux. It
      supports cross-port digipeating, automatic importing of flexnet
      routing, various IP functions, and ANSI colors.

  * **[xastir](http://www.xastir.org): Amateur Station Tracking and Reporting system for amateur radio**

      Xastir is a graphical application that interfaces HAM radio and
      internet access to realtime mapping software. Install XASTIR if
      you are interested in APRS(tm) and HAM radio software.

  * **[xconvers](http://xconvers.sourceforge.net/): Ham radio convers client similar to IRC for X/GTK**

      Xconvers is a client to connect to a convers server (port 3600).
      In a split-screen session you can type text into the bottom
      screen. The reply from the server can be seen in the top screen.
      Features: color support, optional saving of the session to a log
      file, history for the connect dialog and the send widget and
      autologin.

  * **[xfhell](http://5b4az.chronos.org.uk/pages/digital.html): GTK based Ham Radio application for the Hellschreiber communications mode**

      xfhell is a GTK+ application for the "fuzzy" digital amateur
      radio communication mode known as Hellschreiber.

  * **[xlog](http://www.nongnu.org/xlog/): Logging program for Hamradio Operators**

      xlog is a logging program for amateur radio operators. The log
      is stored into a text file. QSO's are presented in a list. Items
      in the list can be added, deleted or updated. For each contact,
      dxcc information is displayed and bearings and distance is
      calculated, both short and long path. xlog supports trlog, adif,
      cabrillo, edit, twlog and editest files.

  * **[xpsk31](http://www.5b4az.org/): GTK+ graphical version of lpsk31 for Ham Radio**

      xpsk31 is a GTK+ graphical version of lpsk31, using the same
      basic signal decoding and encoding engine but controlled by the
      user via the GUI. In addition it has a FFT-derived "waterfall"
      display of the incoming signal and a "magniphase" display that
      shows the magnitude, phase and frequency error of the psk31
      signal.


```
dnf install LinLog aldo aprsd aprsdigi ax25-apps callgit cwdaemon fbb \
  fldigi fllog flmsg gnuradio gnuradio-examples gpredict gqrx \
  gr-air-modes gr-fcdproplus gr-iqbal gr-osmosdr gr-rds grig hackrf \
  hamlib klog linsmith lpsk31 qsstv rtl-sdr rtlsdr-scanner soundmodem \
  splat trustedqsl tucnak2 twlog unixcw uronode xastir xconvers xfhell \
  xlog xpsk31
```

[back to index](#index)

***
## Backup Tools
  * **[amanda](http://www.amanda.org): A network-capable tape backup solution**

      AMANDA, the Advanced Maryland Automatic Network Disk Archiver,
      is a backup system that allows the administrator of a LAN to set
      up a single master backup server to back up multiple hosts to
      one or more tape drives or disk files. AMANDA uses native dump
      and/or GNU tar facilities and can back up a large number of
      workstations running multiple versions of Unix. Newer versions
      of AMANDA (including this version) can use SAMBA to back up
      Microsoft(TM) Windows95/NT hosts. The amanda package contains
      the core AMANDA programs and will need to be installed on both
      AMANDA clients and AMANDA servers. Note that you will have [...]

  * **[backupninja](https://0xacab.org/riseuplabs/backupninja): Lightweight, extensible backup system**

      Backupninja allows you to coordinate system backup by dropping a
      few simple configuration files into /etc/backup.d/. Most
      programs you might use for making backups don't have their own
      configuration file format. Backupninja provides a centralized
      way to configure and schedule many different backup utilities.
      It allows for secure, remote, incremental file system backup
      (via rdiff-backup), compressed incremental data, backup system
      and hardware info, encrypted remote backups (via duplicity),
      safe backup of MySQL/PostgreSQL databases, subversion or trac
      repositories, burn CD/DVDs or create ISOs, incremental [...]

  * **[borgbackup](https://borgbackup.readthedocs.org): A deduplicating backup program with compression and authenticated encryption**

      BorgBackup (short: Borg) is a deduplicating backup program.
      Optionally, it supports compression and authenticated
      encryption.

  * **[btrfs-sxbackup](https://github.com/masc3d/btrfs-sxbackup): Incremental btrfs snapshot backups with push/pull support via SSH**

      Btrfs snapshot backup utility with push/pull support via SSH,
      retention, Email notifications, compression of transferred data,
      and syslog logging.

  * **[dump](http://dump.sourceforge.net/): Programs for backing up and restoring ext2/ext3/ext4 filesystems**

      The dump package contains both dump and restore. Dump examines
      files in a filesystem, determines which ones need to be backed
      up, and copies those files to a specified disk, tape, or other
      storage medium. The restore command performs the inverse
      function of dump; it can restore a full backup of a filesystem.
      Subsequent incremental backups can then be layered on top of the
      full backup. Single files and directory subtrees may also be
      restored from full or partial backups. Install dump if you need
      a system for both backing up filesystems and restoring
      filesystems after backups.

  * **[duplicity](http://www.nongnu.org/duplicity/): Encrypted bandwidth-efficient backup using rsync algorithm**

      Duplicity incrementally backs up files and directory by
      encrypting tar-format volumes with GnuPG and uploading them to a
      remote (or local) file server. In theory many protocols for
      connecting to a file server could be supported; so far ssh/scp,
      local file access, rsync, ftp, HSI, WebDAV and Amazon S3 have
      been written. Because duplicity uses librsync, the incremental
      archives are space efficient and only record the parts of files
      that have changed since the last backup. Currently duplicity
      supports deleted files, full unix permissions, directories,
      symbolic links, fifos, device files, but not hard links.

  * **[kbackup](https://github.com/KDE/kbackup): Back up your data in a simple, user friendly way**

      KBackup is a program that lets you back up any directories or
      files, whereby it uses an easy to use directory tree to select
      the things to back up. The program was designed to be very
      simple in its use so that it can be used by non-computer
      experts. The storage format is the well known TAR format,
      whereby the data is still stored in compressed format (bzip2 or
      gzip).

  * **[rsnapshot](http://www.rsnapshot.org/): Local and remote filesystem snapshot utility**

      This is a remote backup program that uses rsync to take backup
      snapshots of filesystems. It uses hard links to save space on
      disk.


```
dnf install amanda backupninja borgbackup btrfs-sxbackup dump \
  duplicity kbackup rsnapshot
```

[back to index](#index)

***
## Benchmarks
  * **[bonnie++](http://www.coker.com.au/bonnie++/): Filesystem and disk benchmark & burn-in suite**

      bonnie++ filesystem and disk benchmark suite aggressively reads
      & writes in various ways on your filesystem then outputs useful
      benchmark performance data. bonnie++ is also useful as a
      hardware, disk, and filesystem stability test, exposing some
      types of hardware or kernel failures that would otherwise be
      difficult to detect. Do not leave bonnie++ installed on a
      production system. Use only while you test servers.

  * **[fio](http://git.kernel.dk/?p=fio.git;a=summary): Multithreaded IO generation tool**

      fio is an I/O tool that will spawn a number of threads or
      processes doing a particular type of io action as specified by
      the user. fio takes a number of global parameters, each
      inherited by the thread unless otherwise parameters given to
      them overriding that setting is given. The typical use of fio is
      to write a job file matching the io load one wants to simulate.

  * **[phoronix-test-suite](http://phoronix-test-suite.com/): An Automated, Open-Source Testing Framework**

      The Phoronix Test Suite is the most comprehensive testing and
      benchmarking platform available for the Linux operating system.
      This software is designed to effectively carry out both
      qualitative and quantitative benchmarks in a clean,
      reproducible, and easy-to-use manner. The Phoronix Test Suite
      consists of a lightweight processing core (pts-core) with each
      benchmark consisting of an XML-based profile with related
      resource scripts. The process from the benchmark installation,
      to the actual benchmarking, to the parsing of important hardware
      and software components is heavily automated and [...]

  * **[pipebench](http://www.habets.pp.se/synscan/programs.php?prog=pipebench): Measures the speed of STDIN/STDOUT communication**

      Measures the speed of a pipe, by sitting in the middle passing
      the data along to the next process. See the included README for
      example usage.


```
dnf install bonnie++ fio phoronix-test-suite pipebench
```

[back to index](#index)

***
## Chat
  * **[bitlbee](https://www.bitlbee.org/): IRC to other chat networks gateway**

      Bitlbee is an IRC to other chat networks gateway. Bitlbee can be
      used as an IRC server which forwards everything you say to
      people on other chat networks like XMPP/Jabber (including Google
      Talk and Hipchat), MSN/Skype, AIM and ICQ, the Twitter
      microblogging network (and all other Twitter API compatible
      services like status.net).

  * **[bti](https://github.com/gregkh/bti): Bash Twitter Idiocy**

      Allows you to pipe your bash input to twitter in an easy and
      fast manner to annoy the whole world.

  * **[irc-otr](https://github.com/cryptodotis/irssi-otr): Off-The-Record Messaging plugin for irssi**

      This provides modules which implement Off-The-Record (OTR)
      Messaging for the irssi IRC client.

  * **[irssi](http://irssi.org/): Modular text mode IRC client with Perl scripting**

      Irssi is a modular IRC client with Perl scripting. Only text-
      mode frontend is currently supported. The GTK/GNOME frontend is
      no longer being maintained.

  * **[irssi-xmpp](http://cybione.org/~irssi-xmpp/): XMPP plugin into irssi**

      Irssi-xmpp is an irssi plugin to connect to the Jabber network.

  * **[pidgin](http://pidgin.im/): A Gtk+ based multiprotocol instant messaging client**

      Pidgin allows you to talk to anyone using a variety of messaging
      protocols including AIM, MSN, Yahoo!, Jabber, Bonjour, Gadu-
      Gadu, ICQ, IRC, Novell Groupwise, QQ, Lotus Sametime, Simple and
      Zephyr. These protocols are implemented using a modular, easy to
      use design. To use a protocol, just add an account using the
      account editor. Pidgin supports many common features of other
      clients, as well as many unique features, such as perl
      scripting, TCL scripting and C plugins. Pidgin is not affiliated
      with or endorsed by America Online, Inc., Microsoft Corporation,
      Yahoo! Inc., or ICQ Inc.

  * **[pidgin-otr](http://otr.cypherpunks.ca/): Off-The-Record Messaging plugin for Pidgin**

      This is a Pidgin plugin which implements Off-the-Record (OTR)
      Messaging. It is known to work (at least) under the Linux and
      Windows versions of Pidgin.

  * **[quassel](http://quassel-irc.org/): A modern distributed IRC system**

      Quassel IRC is a modern, distributed IRC client, meaning that
      one (or multiple) client(s) can attach to and detach from a
      central core -- much like the popular combination of screen and
      a text-based IRC client such as WeeChat, but graphical

  * **[scudcloud](https://github.com/raelgc/scudcloud): Non official desktop client for Slack**

      ScudCloud improves the Slack integration with Linux desktops
      featuring: ' multiple teams support ' native system
      notifications ' count of unread direct mentions at
      launcher/sytray icon ' alert/wobbling on new messages ' optional
      tray notifications and "Close to Tray" ' follow your desktop
      activity and will stay online while you're logged in

  * **[sendxmpp](http://sendxmpp.hostname.sk/): A Perl script to send XMPP messages**

      Sendxmpp is a Perl script to send XMPP (Jabber) messages from
      the command line, similar to what mail(1) does for mail.
      Messages can be sent both to individual recipients and chat
      rooms.


```
dnf install bitlbee bti irc-otr irssi irssi-xmpp pidgin pidgin-otr \
  quassel scudcloud sendxmpp
```

[back to index](#index)

***
## Compression
  * **[arc](http://arc.sourceforge.net/): Arc archiver**

      Arc file archiver and compressor. Long since superseded by
      zip/unzip but useful if you have old .arc files you need to
      unpack.

  * **[blosc](https://github.com/Blosc/c-blosc): High performance compressor optimized for binary data**

      Blosc is a compression library designed to transmit data to the
      processor cache faster than the traditional non-compressed
      memory fetch. Compression ratios are not very high, but the
      decompression is very fast. Blosc is meant not only to reduce
      the size of large datasets on-disk or in-memory, but also to
      accelerate memory-bound computations.

  * **rpmfusion-nonfree [lha](https://github.com/jca02266/lha): Archiving and compression utility for LHarc/lha/lzh archives**

      LHA is an archiving and compression utility for LHarc/lha/lzh
      format archives.

  * **[lz4](https://lz4.github.io/lz4/): Extremely fast compression algorithm**

      LZ4 is an extremely fast loss-less compression algorithm,
      providing compression speed at 400 MB/s per core, scalable with
      multi-core CPU. It also features an extremely fast decoder, with
      speed in multiple GB/s per core, typically reaching RAM speed
      limits on multi-core systems.

  * **[lzop](https://www.lzop.org/): Real-time file compressor**

      lzop is a compression utility which is designed to be a
      companion to gzip. It is based on the LZO data compression
      library and its main advantages over gzip are much higher
      compression and decompression speed at the cost of some
      compression ratio. The lzop compression utility was designed
      with the goals of reliability, speed, portability and with
      reasonable drop-in compatibility to gzip.

  * **[p7zip](http://p7zip.sourceforge.net/): Very high compression ratio file archiver**

      p7zip is a port of 7za.exe for Unix. 7-Zip is a file archiver
      with a very high compression ratio. The original version can be
      found at http://www.7-zip.org/.

  * **✓ [pbzip2](http://www.compression.ca/pbzip2/): Parallel implementation of bzip2**

      PBZIP2 is a parallel implementation of the bzip2 block-sorting
      file compressor that uses pthreads and achieves near-linear
      speedup on SMP machines. The output of this version is fully
      compatible with bzip2 v1.0.2 or newer (ie: anything compressed
      with pbzip2 can be decompressed with bzip2).

  * **✓ [pigz](http://www.zlib.net/pigz/): Parallel implementation of gzip**

      pigz, which stands for parallel implementation of gzip, is a
      fully functional replacement for gzip that exploits multiple
      processors and multiple cores to the hilt when compressing data.

  * **✓ [pxz](http://jnovy.fedorapeople.org/pxz): Parallel LZMA compressor using XZ**

      Parallel XZ is a compression utility that takes advantage of
      running XZ compression simultaneously on different parts of an
      input file on multiple cores and processors. This significantly
      speeds up compression time.

  * **[rzip](http://rzip.samba.org): A large-file compression program**

      rzip is a compression program, similar in functionality to gzip
      or bzip2, but able to take advantage of long distance
      redundancies in files, which can sometimes allow rzip to produce
      much better compression ratios than other programs.

  * **[unzip](http://www.info-zip.org/UnZip.html): A utility for unpacking zip files**

      The unzip utility is used to list, test, or extract files from a
      zip archive. Zip archives are commonly found on MS-DOS systems.
      The zip utility, included in the zip package, creates zip
      archives. Zip and unzip are both compatible with archives
      created by PKWARE(R)'s PKZIP for MS-DOS, but the programs'
      options and default behaviors do differ in some respects.
      Install the unzip package if you need to list, test or extract
      files from a zip archive.

  * **[xz](http://tukaani.org/xz/): LZMA compression utilities**

      XZ Utils are an attempt to make LZMA compression easy to use on
      free (as in freedom) operating systems. This is achieved by
      providing tools and libraries which are similar to use than the
      equivalents of the most popular existing compression algorithms.
      LZMA is a general purpose compression algorithm designed by Igor
      Pavlov as part of 7-Zip. It provides high compression ratio
      while keeping the decompression speed fast.

  * **[zip](http://www.info-zip.org/Zip.html): A file compression and packaging utility compatible with PKZIP**

      The zip program is a compression and file packaging utility. Zip
      is analogous to a combination of the UNIX tar and compress
      commands and is compatible with PKZIP (a compression and file
      packaging utility for MS-DOS systems). Install the zip package
      if you need to compress files using the zip program.


```
dnf install arc blosc lha lz4 lzop p7zip pbzip2 pigz pxz rzip unzip xz \
  zip
```

[back to index](#index)

***
## Databases and Tools
  * **[innotop](https://github.com/innotop/innotop): A MySQL and InnoDB monitor program**

      innotop connects to a MySQL database server and retrieves
      information from it, then displays it in a manner similar to the
      UNIX top program. innotop uses the data from SHOW VARIABLES,
      SHOW GLOBAL STATUS, SHOW FULL PROCESSLIST, and SHOW ENGINE
      INNODB STATUS, among other things.

  * **[maatkit](http://www.maatkit.org/): Essential command-line utilities for MySQL**

      This toolkit contains essential command-line utilities for
      MySQL, such as a table checksum tool and query profiler. It
      provides missing features such as checking slaves for data
      consistency, with emphasis on quality and scriptability.

  * **[mariadb](http://mariadb.org): A very fast and robust SQL database server**

      MariaDB is a community developed branch of MySQL - a multi-user,
      multi-threaded SQL database server. It is a client/server
      implementation consisting of a server daemon (mysqld) and many
      different client programs and libraries. The base package
      contains the standard MariaDB/MySQL client programs and generic
      MySQL files.

  * **[mysqltuner](http://mysqltuner.com/): MySQL configuration assistant**

      MySQLTuner is a script written in Perl that will assist you with
      your MySQL configuration and make recommendations for increased
      performance and stability. Within seconds, it will display
      statistics about your MySQL installation and the areas where it
      can be improved.

  * **[pg_top](http://pgfoundry.org/projects/ptop): 'top' for PostgreSQL process**

      pg_top is 'top' for PostgreSQL processes. See running queries,
      query plans, issued locks, and table and index statistics.

  * **[pgadmin3](http://www.pgadmin.org/): Graphical client for PostgreSQL**

      pgAdmin III is a powerful administration and development
      platform for the PostgreSQL database, free for any use. It is
      designed to answer the needs of all users, from writing simple
      SQL queries to developing complex databases. The graphical
      interface supports all PostgreSQL features and makes
      administration easy. pgAdmin III is designed to answer the needs
      of all users, from writing simple SQL queries to developing
      complex databases. The graphical interface supports all
      PostgreSQL features and makes administration easy. The
      application also includes a syntax highlighting SQL [...]

  * **[sqliteman](http://sqliteman.yarpen.cz/): Manager for sqlite - Sqlite Databases Made Easy**

      If you are looking for a tool for tuning SQL statements, manage
      tables, views, or triggers, administrate the database space and
      index statistics then Sqliteman is the perfect choice. If you
      are looking for a graphical queries creation wizards, user
      interface designers for your database, or an universal report
      tool try the applications designed for tasks such this (Kexi,
      knoda).


```
dnf install innotop maatkit mariadb mysqltuner pg_top pgadmin3 \
  sqliteman
```

[back to index](#index)

***
## Development Tools
  * **[anjuta](http://www.anjuta.org/): GNOME IDE for various programming languages (including C/C++, Python, Vala and JavaScript)**

      Anjuta DevStudio is a versatile software development studio
      featuring a number of advanced programming facilities including
      project management, application wizard, interactive debugger,
      source editor, version control, GUI designer, profiler and many
      more tools. It focuses on providing simple and usable user
      interface, yet powerful for efficient development.

  * **[buildbot](https://buildbot.net): Build/test automation system**

      The BuildBot is a system to automate the compile/test cycle
      required by most software projects to validate code changes. By
      automatically rebuilding and testing the tree each time
      something has changed, build problems are pinpointed quickly,
      before other developers are inconvenienced by the failure.

  * **[cloc](https://github.com/AlDanial/cloc): Count lines of code**

      A tool to count lines of code in various languages from a given
      directory.

  * **[clutter](http://www.clutter-project.org/): Open Source software library for creating rich graphical user interfaces**

      Clutter is an open source software library for creating fast,
      visually rich graphical user interfaces. The most obvious
      example of potential usage is in media center type applications.
      We hope however it can be used for a lot more.

  * **[code2html](http://www.palfrader.org/code/code2html): Convert source code to HTML**

      Code2HTML converts a program source code to syntax highlighted
      HTML.

  * **[gitg](https://wiki.gnome.org/Apps/Gitg): GTK+ graphical interface for the git revision control system**

      gitg is the GNOME GUI client to view git repositories.

  * **[gitstats](http://gitstats.sourceforge.net): Generates statistics based on GIT repository activity**

      GitStats is a statistics generator for git (a distributed
      revision control system) repositories. It examines the
      repository and produces some interesting statistics from the
      history of it. Currently HTML is the only output format.

  * **[global](http://www.gnu.org/software/global): Source code tag system**

      GNU GLOBAL is a source code tag system that works the same way
      across diverse environments. It supports C, C++, Yacc, Java, PHP
      and assembler source code.

  * **[gource](http://gource.io/): Software version control visualization**

      OpenGL-based 3D visualization tool for source control
      repositories. The repository is displayed as a tree where the
      root of the repository is the centre, directories are branches
      and files are leaves. Contributors to the source code appear and
      disappear as they contribute to specific files and directories.

  * **[gperf](http://www.gnu.org/software/gperf/): A perfect hash function generator**

      Gperf is a perfect hash function generator written in C++.
      Simply stated, a perfect hash function is a hash function and a
      data structure that allows recognition of a key word in a set of
      words using exactly one probe into the data structure.

  * **[highlight](http://www.andre-simon.de/): Universal source code to formatted text converter**

      A utility that converts sourcecode to HTML, XHTML, RTF, LaTeX,
      TeX, XSL-FO, XML or ANSI escape sequences with syntax
      highlighting. It supports several programming and markup
      languages. Language descriptions are configurable and support
      regular expressions. The utility offers indentation and
      reformatting capabilities. It is easily possible to create new
      language definitions and colour themes.

  * **[indent](http://www.gnu.org/software/indent/): A GNU program for formatting C code**

      Indent is a GNU program for beautifying C code, so that it is
      easier to read. Indent can also convert from one C writing style
      to a different one. Indent understands correct C syntax and
      tries to handle incorrect C syntax. Install the indent package
      if you are developing applications in C and you want a program
      to format your code.

  * **[kscope](https://github.com/chaoys/kscope): QT front-end to Cscope**

      KScope is a QT5 front-end to Cscope. It provides a source-
      editing environment for large C projects, such as the Linux
      kernel. KScope is by no means intended to be a replacement to
      any of the leading Linux/KDE IDEs, such as KDevelop. First of
      all, it is not an Integrated Development Environment: it does
      not provide the usual write/compile/debug cycle supported by
      most IDE's. Instead, KScope is focused on source editing and
      analysis.

  * **[ltrace](http://ltrace.alioth.debian.org/): Tracks runtime library calls from dynamically linked executables**

      Ltrace is a debugging program which runs a specified command
      until the command exits. While the command is executing, ltrace
      intercepts and records both the dynamic library calls called by
      the executed process and the signals received by the executed
      process. Ltrace can also intercept and print system calls
      executed by the process. You should install ltrace if you need a
      sysadmin tool for tracking the execution of processes.

  * **[luajit](http://luajit.org/): Just-In-Time Compiler for Lua**

      LuaJIT implements the full set of language features defined by
      Lua 5.1. The virtual machine (VM) is API- and ABI-compatible to
      the standard Lua interpreter and can be deployed as a drop-in
      replacement.

  * **[qjson](https://github.com/flavio/qjson): A qt-based library that maps JSON data to QVariant objects**

      JSON is a lightweight data-interchange format. It can represents
      integer, real number, string, an ordered sequence of value, and
      a collection of name/value pairs.QJson is a qt-based library
      that maps JSON data to QVariant objects.

  * **[snip](http://www.martiansoftware.com/lab/snip/): An Ant task designed to help with the single-sourcing of program documentation**

      An Ant task designed to help with the single-sourcing of program
      documentation.

  * **[stgit](http://www.procode.org/stgit/): Patch stack for Git repositories**

      StGit is a Python application providing similar functionality to
      Quilt (i.e. pushing/popping patches to/from a stack) on top of
      Git. These operations are performed using Git commands and the
      patches are stored as Git commit objects, allowing easy merging
      of the StGit patches into other repositories using standard Git
      functionality. Note that StGit is not an SCM interface on top of
      Git and it expects a previously initialized Git repository. For
      standard SCM operations, either use plain Git commands or the
      Cogito tool.

  * **[udis86](https://github.com/vmt/udis86): A disassembler library for x86 and x86-64**

      udis86 is a disassembler library (libudis86) for x86 and x86-64.
      The primary intent is to aid binary code analysis.

  * **[xdelta](http://xdelta.org/): A binary file delta generator**

      Xdelta (X for XCF: the eXperimental Computing Facility at
      Berkeley) is a binary delta generator (like a diff program for
      binaries) and an RCS version control replacement library. Xdelta
      uses a binary file delta algorithm to replace the standard diff
      program used by RCS


```
dnf install anjuta buildbot cloc clutter code2html gitg gitstats \
  global gource gperf highlight indent kscope ltrace luajit qjson snip \
  stgit udis86 xdelta
```

[back to index](#index)

***
## Development Tools - Build Essentials
  * **✓ [autoconf](http://www.gnu.org/software/autoconf/): A GNU tool for automatically configuring source code**

      GNU's Autoconf is a tool for configuring source code and
      Makefiles. Using Autoconf, programmers can create portable and
      configurable packages, since the person building the package is
      allowed to specify various configuration options. You should
      install Autoconf if you are developing software and would like
      to create shell scripts that configure your source code
      packages. If you are installing Autoconf, you will also need to
      install the GNU m4 package. Note that the Autoconf package is
      not required for the end-user who may be configuring software
      with an Autoconf-generated script; Autoconf is only [...]

  * **✓ [automake](http://www.gnu.org/software/automake/): A GNU tool for automatically creating Makefiles**

      Automake is a tool for automatically generating 'Makefile.in'
      files compliant with the GNU Coding Standards. You should
      install Automake if you are developing software and would like
      to use its ability to automatically generate GNU standard
      Makefiles.

  * **✓ [gcc](http://gcc.gnu.org): Various compilers (C, C++, Objective-C, ...)**

      The gcc package contains the GNU Compiler Collection version 9.
      You'll need this package in order to compile C code.

  * **✓ [git](https://git-scm.com/): Fast Version Control System**

      Git is a fast, scalable, distributed revision control system
      with an unusually rich command set that provides both high-level
      operations and full access to internals. The git rpm installs
      common set of tools which are usually using with small amount of
      dependencies. To install all git packages, including tools for
      integrating with other SCMs, install the git-all meta-package.

  * **✓ [kernel-devel](https://www.kernel.org/): Development package for building kernel modules to match the kernel**

      This package provides kernel headers and makefiles sufficient to
      build modules against the kernel package.


  Also, run `dnf groupinstall "Development Tools"`


```
dnf install autoconf automake gcc git kernel-devel
```

[back to index](#index)

***
## Development Tools - C/C++
  * **[clang](http://llvm.org): A C language family front-end for LLVM**

      C/C++ compiler for LLVM.

  * **[clang-analyzer](http://llvm.org): A source code analysis framework**

      The Clang Static Analyzer consists of both a source code
      analysis framework and a standalone tool that finds bugs in C
      and Objective-C programs. The standalone tool is invoked from
      the command-line, and is intended to run in tandem with a build
      of a project or code base.

  * **[cppcheck](http://cppcheck.wiki.sourceforge.net/): Tool for static C/C++ code analysis**

      Cppcheck is a static analysis tool for C/C++ code. Unlike C/C++
      compilers and many other analysis tools it does not detect
      syntax errors in the code. Cppcheck primarily detects the types
      of bugs that the compilers normally do not detect. The goal is
      to detect only real errors in the code (i.e. have zero false
      positives).

  * **[cppunit](https://www.freedesktop.org/wiki/Software/cppunit/): C++ unit testing framework**

      CppUnit is the C++ port of the famous JUnit framework for unit
      testing. Test output is in XML for automatic testing and GUI
      based for supervised tests.

  * **[frama-c](http://frama-c.com/): Framework for source code analysis of C software**

      Frama-C is a suite of tools dedicated to the analysis of the
      source code of software written in C. Frama-C gathers several
      static analysis techniques in a single collaborative framework.
      The collaborative approach of Frama-C allows static analyzers to
      build upon the results already computed by other analyzers in
      the framework. Thanks to this approach, Frama-C provides
      sophisticated tools, such as a slicer and dependency analysis.

  * **[kdevelop](http://www.kdevelop.org/): Integrated Development Environment for C++/C**

      The KDevelop Integrated Development Environment provides many
      features that developers need as well as providing a unified
      interface to programs like gdb, the C/C++ compiler, and make.
      KDevelop manages or provides: All development tools needed for
      C++ programming like Compiler, Linker, automake and autoconf;
      KAppWizard, which generates complete, ready-to-go sample
      applications; Classgenerator, for creating new classes and
      integrating them into the current project; File management for
      sources, headers, documentation etc. to be included in the
      project; The creation of User-Handbooks written with SGML [...]

  * **[lcdtest](http://www.brouhaha.com/~eric/software/lcdtest/): Displays monitor test patterns**

      lcdtest is a utility to display LCD monitor test patterns. It
      may be useful for adjusting the pixel clock frequency and phase
      on LCD monitors when using analog inputs, and for finding pixels
      that are stuck on or off.

  * **[lcov](https://github.com/linux-test-project/lcov/): LTP GCOV extension code coverage tool**

      LCOV is an extension of GCOV, a GNU tool which provides
      information about what parts of a program are actually executed
      (i.e. "covered") while running a particular test case. The
      extension consists of a set of PERL scripts which build on the
      textual GCOV output to implement HTML output and support for
      large projects.

  * **[llvm](http://llvm.org): The Low Level Virtual Machine**

      LLVM is a compiler infrastructure designed for compile-time,
      link-time, runtime, and idle-time optimization of programs from
      arbitrary programming languages. The compiler infrastructure
      includes mirror sets of programming tools as well as libraries
      with equivalent functionality.

  * **[memtest86+](http://www.memtest.org): Stand-alone memory tester for x86 and x86-64 computers**

      Memtest86+ is a thorough stand-alone memory test for x86 and
      x86-64 architecture computers. BIOS based memory tests are only
      a quick check and often miss many of the failures that are
      detected by Memtest86+. The ELF version should be used for
      booting from grub, and avoids the following errors: "Error 7:
      Loading below 1MB is not supported" "Error 13: Invalid or
      unsupported executable format" "Error 28: Selected item cannot
      fit into memory" The script '/usr/sbin/memtest-setup' can be run
      (as root) to add the memtest86+ entry to your GRUB boot menu.

  * **[memtester](http://pyropus.ca/software/memtester/): Utility to test for faulty memory subsystem**

      memtester is a utility for testing the memory subsystem in a
      computer to determine if it is faulty.

  * **[qt-creator](https://www.qt.io/ide/): Cross-platform IDE for Qt**

      Qt Creator is a cross-platform IDE (integrated development
      environment) tailored to the needs of Qt developers.

  * **[ragel](http://www.colm.net/open-source/ragel/): Finite state machine compiler**

      Ragel compiles executable finite state machines from regular
      languages. Ragel targets C, C++ and ASM. Ragel state machines
      can not only recognize byte sequences as regular expression
      machines do, but can also execute code at arbitrary points in
      the recognition of a regular language. Code embedding is done
      using inline operators that do not disrupt the regular language
      syntax.

  * **[sdcc](http://sdcc.sourceforge.net/): Small Device C Compiler**

      SDCC is a C compiler for 8051 class and similar
      microcontrollers. The package includes the compiler, assemblers
      and linkers, a device simulator and a core library. The
      processors supported (to a varying degree) include the 8051,
      ds390, z80, hc08, and PIC.

  * **[stress](http://people.seas.harvard.edu/~apw/stress/): A tool to put given subsystems under a specified load**

      stress is not a benchmark, but is rather a tool designed to put
      given subsytems under a specified load. Instances in which this
      is useful include those in which a system administrator wishes
      to perform tuning activities, a kernel or libc programmer wishes
      to evaluate denial of service possibilities, etc.

  * **[uncrustify](http://uncrustify.sourceforge.net/): Reformat Source**

      Source Code Beautifier for C, C++, C#, D, Java, and Pawn


```
dnf install clang clang-analyzer cppcheck cppunit frama-c kdevelop \
  lcdtest lcov llvm memtest86+ memtester qt-creator ragel sdcc stress \
  uncrustify
```

[back to index](#index)

***
## Diagrams and Drawing
  * **[asymptote](http://asymptote.sourceforge.net/): Descriptive vector graphics language**

      Asymptote is a powerful descriptive vector graphics language for
      technical drawings, inspired by MetaPost but with an improved
      C++-like syntax. Asymptote provides for figures the same high-
      quality level of typesetting that LaTeX does for scientific
      text.

  * **[dia](https://wiki.gnome.org/Apps/Dia): Diagram drawing program**

      The Dia drawing program can be used to draw different types of
      diagrams, and includes support for UML static structure diagrams
      (class diagrams), entity relationship modeling, and network
      diagrams. Dia can load and save diagrams to a custom file
      format, can load and save in .xml format, and can export to
      PostScript(TM).

  * **✓ [graphviz](http://www.graphviz.org/): Graph Visualization Tools**

      A collection of tools for the manipulation and layout of graphs
      (as in nodes and edges, not as in barcharts).

  * **[timeline](http://thetimelineproj.sourceforge.net/): Displays and navigates events on a timeline**

      Timeline is a cross-platform application for displaying and
      navigating events on a timeline.

  * **[umbrello](https://www.kde.org/applications/development/umbrello/): UML modeler and UML diagram tool**

      GUI for diagramming Unified Modeling Language (UML)


```
dnf install asymptote dia graphviz timeline umbrello
```

[back to index](#index)

***
## Document Processing
  * **[antiword](http://www.winfield.demon.nl/): MS Word to ASCII/Postscript converter**

      Antiword is a free MS-Word reader for Linux, BeOS and RISC OS.
      It converts the documents from Word 6, 7, 97 and 2000 to ASCII
      and Postscript. Antiword tries to keep the layout of the
      document intact.

  * **[diffpdf](http://www.qtrac.eu/diffpdf.html): PDF files comparator**

      DiffPDF is used to compare two PDF files. By default the
      comparison is of the text on each pair of pages, but comparing
      the appearance of pages is also supported (for example, if a
      diagram is changed or a paragraph reformatted). It is also
      possible to compare particular pages or page ranges.

  * **[enscript](http://www.gnu.org/software/enscript): A plain ASCII to PostScript converter**

      GNU enscript is a free replacement for Adobe's Enscript program.
      Enscript converts ASCII files to PostScript(TM) and spools
      generated PostScript output to the specified printer or saves it
      to a file. Enscript can be extended to handle different output
      media and includes many options for customizing printouts

  * **[lyx](http://www.lyx.org/): WYSIWYM (What You See Is What You Mean) document processor**

      LyX is a modern approach to writing documents which breaks with
      the obsolete "typewriter paradigm" of most other document
      preparation systems. It is designed for people who want
      professional quality output with a minimum of time and effort,
      without becoming specialists in typesetting. The major
      innovation in LyX is WYSIWYM (What You See Is What You Mean).
      That is, the author focuses on content, not on the details of
      formatting. This allows for greater productivity, and leaves the
      final typesetting to the backends (like LaTeX) that are
      specifically designed for the task. With LyX, the author [...]

  * **[mpage](http://www.mesa.nl/pub/mpage/): A tool for printing multiple pages of text on each printed page**

      The mpage utility takes plain text files or PostScript(TM)
      documents as input, reduces the size of the text, and prints the
      files on a PostScript printer with several pages on each sheet
      of paper. Mpage is very useful for viewing large printouts
      without using up lots of paper. Mpage supports many different
      layout options for the printed pages.

  * **[okular](https://www.kde.org/applications/graphics/okular/): A document viewer**

      A document viewer.

  * **[pandoc](https://hackage.haskell.org/package/pandoc): Conversion between markup formats**

      Pandoc is a Haskell library for converting from one markup
      format to another, and a command-line tool that uses this
      library. It can read several dialects of Markdown and (subsets
      of) HTML, reStructuredText, LaTeX, DocBook, JATS, MediaWiki
      markup, TWiki markup, TikiWiki markup, Creole 1.0, Haddock
      markup, OPML, Emacs Org-Mode, Emacs Muse, txt2tags, Vimwiki,
      Word Docx, ODT, EPUB, FictionBook2, roff man, and Textile, and
      it can write Markdown, reStructuredText, XHTML, HTML 5, LaTeX,
      ConTeXt, DocBook, JATS, OPML, TEI, OpenDocument, ODT, Word docx,
      PowerPoint pptx, RTF, MediaWiki, DokuWiki, ZimWiki, [...]

  * **[par](http://www.nicemice.net/par/): Paragraph reformatter, vaguely like fmt, but more elaborate**

      par is a filter which copies its input to its output, changing
      all white characters (except newlines) to spaces, and
      reformatting each paragraph. Paragraphs are separated by
      protected, blank, and bodiless lines (see the man page
      Terminology section for definitions), and optionally delimited
      by indentation (see the d option in the Options section). Each
      output paragraph is generated from the corresponding input
      paragraph as follows: 1) An optional prefix and/or suffix is
      removed from each input line. 2) The remainder is divided into
      words (separated by spaces). 3) The words are joined into [...]

  * **[pstoedit](http://www.pstoedit.net/): Translates PostScript and PDF graphics into other vector formats**

      Pstoedit converts PostScript and PDF files to various vector
      graphic formats. The resulting files can be edited or imported
      into various drawing packages. Pstoedit comes with a large set
      of integrated format drivers

  * **[qpdf](http://qpdf.sourceforge.net/): Command-line tools and library for transforming PDF files**

      QPDF is a command-line program that does structural, content-
      preserving transformations on PDF files. It could have been
      called something like pdf-to-pdf. It includes support for
      merging and splitting PDFs and to manipulate the list of pages
      in a PDF file. It is not a PDF viewer or a program capable of
      converting PDF into other formats.

  * **[sane-frontends](http://www.sane-project.org): Graphical frontend to SANE**

      This packages includes the scanadf and xcam programs.

  * **[scribus](http://www.scribus.net/): DeskTop Publishing application written in Qt**

      Scribus is an desktop open source page layout program with the
      aim of producing commercial grade output in PDF and Postscript,
      primarily, though not exclusively for Linux. While the goals of
      the program are for ease of use and simple easy to understand
      tools, Scribus offers support for professional publishing
      features, such as CMYK color, easy PDF creation, Encapsulated
      Postscript import/export and creation of color separations.

  * **[sigil](https://sigil-ebook.com/): WYSIWYG ebook editor**

      Sigil is a multi-platform WYSIWYG ebook editor. It is designed
      to edit books in ePub format. Now what does it have to offer...
      ' Full Unicode support: everything you see in Sigil is in UTF-16
      ' Full EPUB spec support ' WYSIWYG editing ' Multiple Views:
      Book View, Code View and Split View ' Metadata editor with full
      support for all possible metadata entries with full descriptions
      for each ' Table Of Contents editor ' Multi-level TOC support '
      Book View fully supports the display of any XHTML document
      possible under the OPS spec ' SVG support ' Basic XPGT support '
      Advanced automatic conversion of all imported documents to [...]

  * **[simple-scan](https://launchpad.net/simple-scan): Simple scanning utility**

      Simple Scan is an easy-to-use application, designed to let users
      connect their scanner and quickly have the image/document in an
      appropriate format.

  * **[unpaper](http://unpaper.berlios.de/): Post-processing of scanned and photocopied book pages**

      unpaper is a post-processing tool for scanned sheets of paper,
      especially for book pages that have been scanned from previously
      created photocopies. The main purpose is to make scanned book
      pages better readable on screen after conversion to PDF.
      Additionally, unpaper might be useful to enhance the quality of
      scanned pages before performing optical character recognition
      (OCR). unpaper tries to clean scanned images by removing dark
      edges that appeared through scanning or copying on areas outside
      the actual page content (e.g. dark areas between the left-hand-
      side and the right-hand-side of a double- sided book-page [...]

  * **[unrtf](https://www.gnu.org/software/unrtf/unrtf.html): RTF (Rich Text Format) to other formats converter**

      UnRTF is a command-line program written in C which converts
      documents in Rich Text Format (.rtf) to HTML, LaTeX, troff
      macros, and RTF itself. Converting to HTML, it supports a number
      of features of Rich Text Format: ' Changes in the text's font,
      size, weight (bold), and slant (italic) ' Underlines and
      strikethroughs ' Partial support for text shadowing, outlining,
      embossing, or engraving ' Capitalizations ' Superscripts and
      subscripts ' Expanded and condensed text ' Changes in the
      foreground and background colors ' Conversion of special
      characters to HTML entities

  * **[zathura](http://pwmt.org/projects/zathura/): A lightweight document viewer**

      Zathura is a highly customizable and functional document viewer.
      It provides a minimalistic and space saving interface as well as
      an easy usage that mainly focuses on keyboard interaction.
      Zathura requires plugins to support document formats. For
      instance: ' zathura-pdf-poppler to open PDF files, ' zathura-ps
      to open PostScript files, ' zathura-djvu to open DjVu files, or
      ' zathura-cb to open comic book files. All of these are
      available as separate packages in Fedora. A zathura-plugins-all
      package is available should you want to install all available
      plugins.


```
dnf install antiword diffpdf enscript lyx mpage okular pandoc par \
  pstoedit qpdf sane-frontends scribus sigil simple-scan unpaper unrtf \
  zathura
```

[back to index](#index)

***
## Editors
  * **[bluefish](http://bluefish.openoffice.nl/): Web development application for experienced users**

      Bluefish is a powerful editor for experienced web designers and
      programmers. Bluefish supports many programming and markup
      languages, but it focuses on editing dynamic and interactive
      websites.

  * **[bvi](http://bvi.sourceforge.net/): Display-oriented editor for binary files**

      The bvi is a display-oriented editor for binary files, based on
      the vi text-editor. If you are familiar with vi, just start the
      editor and begin to edit! A bmore program is also included in
      the package.

  * **[e3](https://sites.google.com/site/e3editor/): Text editor with key bindings similar to WordStar, Emacs, pico, nedit, or vi**

      e3 is a full-screen, user-friendly text editor with an key
      bindings similar to that of either WordStar, Emacs, pico, nedit,
      or vi.

  * **[ed](http://www.gnu.org/software/ed/): The GNU line editor**

      Ed is a line-oriented text editor, used to create, display, and
      modify text files (both interactively and via shell scripts).
      For most purposes, ed has been replaced in normal usage by full-
      screen editors (emacs and vi, for example). Ed was the original
      UNIX editor, and may be used by some programs. In general,
      however, you probably don't need to install it and you probably
      won't use it.

  * **[focuswriter](http://gottcode.org/focuswriter/): A full screen, distraction-free writing program**

      A full screen, distraction-free writing program. You can
      customize your environment by changing the font, colors, and
      background image to add ambiance as you type. FocusWriter
      features an on-the-fly updating word count, optional auto-save,
      optional daily goals, and an interface that hides away to allow
      you to focus more clearly; additionally, when you open the
      program your current work-in-progress will automatically load
      and position you at the end of your document, so that you can
      immediately jump back in.

  * **✓ [geany](http://www.geany.org/): A fast and lightweight IDE using GTK3**

      Geany is a small and fast integrated development environment
      with basic features and few dependencies to other packages or
      Desktop Environments. Some features: - Syntax highlighting -
      Code completion - Code folding - Construct completion/snippets -
      Auto-closing of XML and HTML tags - Call tips - Support for Many
      languages like C, Java, PHP, HTML, Python, Perl, Pascal - symbol
      lists and symbol name auto-completion - Code navigation - Simple
      project management - Plugin interface

  * **[ghex](https://gitlab.gnome.org/GNOME/ghex): Binary editor for GNOME**

      GHex can load raw data from binary files and display them for
      editing in the traditional hex editor view. The display is split
      in two columns, with hexadecimal values in one column and the
      ASCII representation in the other. A useful tool for working
      with raw data.

  * **✓ [kate](https://cgit.kde.org/kate.git): Advanced Text Editor**

      Advanced Text Editor.

  * **[okteta](https://cgit.kde.org/okteta.git): Binary/hex editor**

      Okteta is a binary/hex editor for KDE

  * **[qscintilla](http://www.riverbankcomputing.com/software/qscintilla/): A Scintilla port to Qt**

      QScintilla is a port of Scintilla to the Qt GUI toolkit. This
      version of QScintilla is based on Scintilla v3.10.1.

  * **[shed](http://shed.sourceforge.net/): Easy to use hex editor**

      shed is an easy to use hex editor written for unix/linux using
      ncurses, with a friendly pico-style interface.

  * **✓ [vim-enhanced](http://www.vim.org/): A version of the VIM editor which includes recent enhancements**

      VIM (VIsual editor iMproved) is an updated and improved version
      of the vi editor. Vi was the first real screen-based editor for
      UNIX, and is still very popular. VIM improves on vi by adding
      new features: multiple windows, multi-level undo, block
      highlighting and more. The vim-enhanced package contains a
      version of VIM with extra, recently introduced features like
      Python and Perl interpreters. Install the vim-enhanced package
      if you'd like to use a version of the VIM editor which includes
      recently added enhancements like interpreters for the Python and
      Perl scripting languages. You'll also need to install the [...]

  * **[xmlcopyeditor](http://xml-copy-editor.sourceforge.net/): A fast, free, validating XML editor**

      XML Copy Editor is a fast, free, validating XML editor.


```
dnf install bluefish bvi e3 ed focuswriter geany ghex kate okteta \
  qscintilla shed vim-enhanced xmlcopyeditor
```

[back to index](#index)

***
## Electronics
  * **[electric](http://www.staticfreesoft.com/): Sophisticated ASIC and MEM CAD System**

      Electric is a sophisticated electrical CAD system that can
      handle many forms of circuit design, including custom IC layout
      (ASICs), schematic drawing, hardware description language
      specifications, and electro-mechanical hybrid layout.

  * **[electronics-menu](http://geda.seul.org/): Electronics Menu for the Desktop**

      The programs from the category Electronics are normally located
      in the Edutainment directory. This Package adds a Electronics
      menu to the xdg menu structure. electronics-menu is listed among
      Fedora Electronic Lab (FEL) packages.

  * **[gerbv](http://gerbv.gpleda.org/): Gerber file viewer from the gEDA toolkit**

      Gerber Viewer (gerbv) is a viewer for Gerber files. Gerber files
      are generated from PCB CAD system and sent to PCB manufacturers
      as basis for the manufacturing process. The standard supported
      by gerbv is RS-274X. gerbv also supports drill files. The format
      supported are known under names as NC-drill or Excellon. The
      format is a bit undefined and different EDA-vendors implement it
      different. gerbv is listed among Fedora Electronic Lab (FEL)
      packages.

  * **[gresistor](https://sourceforge.net/projects/gresistor/): Gnome resistor color code calculator**

      To allow for identification, resistors are usually marked with
      colored bands. Often refereed to as color codes, these markings
      are indicative of their resistance, tolerance and temperature
      coefficient. gResistor is a great program that will help you
      translate a resistor color codes into a readable value.

  * **[gtkwave](http://gtkwave.sourceforge.net/): Waveform Viewer**

      GTKWave is a waveform viewer that can view VCD files produced by
      most Verilog simulation tools, as well as LXT files produced by
      certain Verilog simulation tools.

  * **[kicad](http://www.kicad-pcb.org): EDA software suite for creation of schematic diagrams and PCBs**

      KiCad is EDA software to design electronic schematic diagrams
      and printed circuit board artwork of up to 32 layers.

  * **[magic](http://opencircuitdesign.com/magic/index.html): A very capable VLSI layout tool**

      Magic is a venerable VLSI layout tool. Magic VLSI remains
      popular with universities and small companies. Magic is widely
      cited as being the easiest tool to use for circuit layout, even
      for people who ultimately rely on commercial tools for their
      product design flow.

  * **[pcb](http://pcb.geda-project.org/index.html): An interactive printed circuit board editor**

      PCB is an interactive printed circuit board editor. PCB includes
      a rats nest feature, design rule checking, and can provide
      industry standard RS-274-X (Gerber), NC drill, and centroid data
      (X-Y data) output for use in the board fabrication and assembly
      process. PCB offers high end features such as an auto-router and
      trace optimizer which can tremendously reduce layout time.

  * **[qelectrotech](http://qelectrotech.org/): An electric diagrams editor**

      QElectroTech is a Qt application to design electric diagrams. It
      uses XML files for elements and diagrams, and includes both a
      diagram editor and an element editor.

  * **[xcircuit](http://opencircuitdesign.com/xcircuit): Electronic circuit schematic drawing program**

      Xcircuit is a general-purpose drawing program and also a
      specific-purpose CAD program for circuit schematic drawing and
      schematic capture.


```
dnf install electric electronics-menu gerbv gresistor gtkwave kicad \
  magic pcb qelectrotech xcircuit
```

[back to index](#index)

***
## Email
  * **[claws-mail](http://claws-mail.org): Email client and news reader based on GTK+**

      Claws Mail is an email client (and news reader), based on GTK+,
      featuring quick response, graceful and sophisticated interface,
      easy configuration, intuitive operation, abundant features, and
      extensibility.

  * **[imapsync](http://fedorahosted.org/imapsync): Tool to migrate email between IMAP servers**

      imapsync is a tool for facilitating incremental recursive IMAP
      transfers from one mailbox to another. It is useful for mailbox
      migration, and reduces the amount of data transferred by only
      copying messages that are not present on both servers. Read,
      unread, and deleted flags are preserved, and the process can be
      stopped and resumed. The original messages can optionally be
      deleted after a successful transfer.

  * **[mairix](http://www.rc0.org.uk/mairix): A program for indexing and searching email messages**

      mairix is a program for indexing and searching email messages
      stored in Maildir, MH or mbox folders.

  * **[offlineimap](https://github.com/OfflineIMAP/offlineimap/archive/v7.2.4.tar.gz): Powerful IMAP/Maildir synchronization and reader support**

      OfflineIMAP is a tool to simplify your e-mail reading. With
      OfflineIMAP, you can read the same mailbox from multiple
      computers. You get a current copy of your messages on each
      computer, and changes you make one place will be visible on all
      other systems. For instance, you can delete a message on your
      home computer, and it will appear deleted on your work computer
      as well. OfflineIMAP is also useful if you want to use a mail
      reader that does not have IMAP support, has poor IMAP support,
      or does not provide disconnected operation.

  * **[thunderbird](http://www.mozilla.org/projects/thunderbird/): Mozilla Thunderbird mail/newsgroup client**

      Mozilla Thunderbird is a standalone mail and newsgroup client.

  * **[tnef](https://github.com/verdammelt/tnef): Extract files from email attachments like WINMAIL.DAT**

      This application provides a way to unpack Microsoft MS-TNEF MIME
      attachments. It operates like tar in order to unpack files of
      type "application/ms-tnef", which may have been placed into the
      MS-TNEF attachment instead of being attached separately. Such
      files may have attachment names similar to WINMAIL.DAT


```
dnf install claws-mail imapsync mairix offlineimap thunderbird tnef
```

[back to index](#index)

***
## Embedded Development Tools
  * **[arduino](http://www.arduino.cc/): An IDE for Arduino-compatible electronics prototyping platforms**

      Arduino is an open-source electronics prototyping platform based
      on flexible, easy-to-use hardware and software. It's intended
      for artists, designers, hobbyists, and anyone interested in
      creating interactive objects or environments. This package
      contains an IDE that can be used to develop and upload code to
      the micro-controller.

  * **[avra](http://avra.sourceforge.net/): Atmel AVR assembler**

      Avra is an assembler for Atmel's AVR 8-bit RISC microcontollers.
      It is mostly compatible with Atmel's own assembler, but provides
      new features such as better macro support and additional
      preprocessor directives. This package also contains various
      device definition files.

  * **[gpsim](http://gpsim.sourceforge.net/gpsim.html): A simulator for Microchip (TM) PIC (TM) microcontrollers**

      gpsim is a simulator for Microchip (TM) PIC (TM)
      microcontrollers. It supports most devices in Microchip's
      12-bit, 14bit, and 16-bit core families. In addition, gpsim
      supports dynamically loadable modules such as LED's, LCD's,
      resistors, etc. to extend the simulation environment beyond the
      PIC.

  * **[gputils](http://gputils.sourceforge.net): Development utilities for Microchip (TM) PIC (TM) microcontrollers**

      This is a collection of development tools for Microchip (TM) PIC
      (TM) microcontrollers. Gputils includes gpasm, gplink and gplib
      as well as the utilities gpdasm, gpstrip, gpvc, gpvo.

  * **[openocd](http://sourceforge.net/projects/openocd): Debugging, in-system programming and boundary-scan testing for embedded devices**

      The Open On-Chip Debugger (OpenOCD) provides debugging, in-
      system programming and boundary-scan testing for embedded
      devices. Various different boards, targets, and interfaces are
      supported to ease development time. Install OpenOCD if you are
      looking for an open source solution for hardware debugging.


```
dnf install arduino avra gpsim gputils openocd
```

[back to index](#index)

***
## Emulators
  * **rpmfusion-free-updates [VirtualBox](http://www.virtualbox.org/wiki/VirtualBox): A general-purpose full virtualizer for PC hardware**

      VirtualBox is a powerful x86 and AMD64/Intel64 virtualization
      product for enterprise as well as home use. Not only is
      VirtualBox an extremely feature rich, high performance product
      for enterprise customers, it is also the only professional
      solution that is freely available as Open Source Software under
      the terms of the GNU General Public License (GPL) version 2.
      Presently, VirtualBox runs on Windows, Linux, Macintosh, and
      Solaris hosts and supports a large number of guest operating
      systems including but not limited to Windows (NT 4.0, 2000, XP,
      Server 2003, Vista, Windows 7, Windows 8, Windows 10), [...]

  * **[dosbox](http://www.dosbox.com): x86/DOS emulator with sound and graphics**

      DOSBox is a DOS-emulator using SDL for easy portability to
      different platforms. DOSBox has already been ported to several
      different platforms, such as Windows, BeOS, Linux, Mac OS X...
      DOSBox emulates a 286/386 realmode CPU, Directory
      FileSystem/XMS/EMS, a SoundBlaster card for excellent sound
      compatibility with older games... You can "re-live" the good old
      days with the help of DOSBox, it can run plenty of the old
      classics that don't run on your new computer!

  * **[fbzx](http://www.rastersoft.com/programas/fbzx.html): A ZX Spectrum emulator for FrameBuffer**

      FBZX is a Sinclair Spectrum emulator, designed to work at full
      screen using the FrameBuffer or under X-Windows.

  * **[gxemul](http://gavare.se/gxemul/): Instruction-level machine emulator**

      GXemul is an experimental instruction-level machine emulator. It
      can be used to run binary code for (among others) MIPS-based
      machines, regardless of host platform. Several emulation modes
      are available. For some modes, processors and surrounding
      hardware components are emulated well enough to let unmodified
      operating systems (e.g. NetBSD) run as if they were running on a
      real machine.

  * **[hercstudio](http://www.jacobdekel.com/hercstudio/): GUI front-end to the Hercules mainframe Emulator**

      GUI front-end to the Hercules mainframe Emulator.

  * **[hercules](http://www.hercules-390.eu/): Hercules S/370, ESA/390, and z/Architecture emulator**

      Hercules is an emulator for the IBM System/370, ESA/390, and
      z/Architecture series of mainframe computers. It is capable of
      running any IBM operating system and applications that a real
      system will run, as long as the hardware needed is emulated.
      Hercules can emulate FBA and CKD DASD, tape, printer, card
      reader, card punch, channel-to-channel adapter, LCS Ethernet,
      and printer-keyboard, 3270 terminal, and 3287 printer devices.

  * **[wine](https://www.winehq.org/): A compatibility layer for windows applications**

      Wine as a compatibility layer for UNIX to run Windows
      applications. This package includes a program loader, which
      allows unmodified Windows 3.x/9x/NT binaries to run on x86 and
      x86_64 Unixes. Wine can use native system .dll files if they are
      available. In Fedora wine is a meta-package which will install
      everything needed for wine to work smoothly. Smaller setups can
      be achieved by installing some of the wine-' sub packages.


```
dnf install VirtualBox dosbox fbzx gxemul hercstudio hercules wine
```

[back to index](#index)

***
## File Managers and Utilities
  * **✓ [fdupes](https://github.com/adrianlopezroche/fdupes): Finds duplicate files in a given set of directories**

      FDUPES is a program for identifying duplicate files residing
      within specified directories.

  * **[filelight](http://utils.kde.org/projects/filelight): Graphical disk usage statistics**

      Filelight allows you to quickly understand exactly where your
      diskspace is being used by graphically representing your file
      system.

  * **[krename](https://github.com/KDE/krename): Powerful batch file renamer**

      KRename is a very powerfull batch file renamer by KDE. It allows
      you to easily rename hundreds or even more files in one go. The
      filenames can be created by parts of the original filename,
      numbering the files or accessing hundreds of informations about
      the file, like creation date or Exif informations of an image.

  * **[krusader](http://www.krusader.org/): An advanced twin-panel (commander-style) file-manager for KDE**

      Krusader is an advanced twin panel (commander style) file
      manager for KDE and other desktops in the 'nix world, similar to
      Midnight or Total Commander. It provides all the file management
      features you could possibly want. Plus: extensive archive
      handling, mounted filesystem support, FTP, advanced search
      module, an internal viewer/editor, directory synchronisation,
      file content comparisons, powerful batch renaming and much much
      more. It supports a wide variety of archive formats and can
      handle other KIO slaves such as smb or fish. It is (almost)
      completely customizable, very user friendly, fast and [...]

  * **[lsyncd](https://github.com/axkibe/lsyncd): File change monitoring and synchronization daemon**

      Lsyncd watches a local directory trees event monitor interface
      (inotify). It aggregates and combines events for a few seconds
      and then spawns one (or more) process(es) to synchronize the
      changes. By default this is rsync. Lsyncd is thus a light-weight
      live mirror solution that is comparatively easy to install not
      requiring new file systems or block devices and does not hamper
      local file system performance.

  * **[mc](http://www.midnight-commander.org/): User-friendly text console file manager and visual shell**

      Midnight Commander is a visual shell much like a file manager,
      only with many more features. It is a text mode application, but
      it also includes mouse support. Midnight Commander's best
      features are its ability to FTP, view tar and zip files, and to
      poke into RPMs for specific files.

  * **[mmv](http://packages.qa.debian.org/m/mmv.html): Move/copy/append/link multiple files**

      This is mmv, a program to move/copy/append/link multiple files
      according to a set of wildcard patterns. This multiple action is
      performed safely, i.e. without any unexpected deletion of files
      due to collisions of target names with existing filenames or
      with other target names. Furthermore, before doing anything, mmv
      attempts to detect any errors that would result from the entire
      set of actions specified and gives the user the choice of either
      aborting before beginning, or proceeding by avoiding the
      offending parts.

  * **✓ [ncdu](http://dev.yorhel.nl/ncdu/): Text-based disk usage viewer**

      ncdu (NCurses Disk Usage) is a curses-based version of the well-
      known 'du', and provides a fast way to see what directories are
      using your disk space.

  * **[pydf](https://pypi.python.org/pypi/pydf/12): Fully colorized df clone written in python**

      pydf displays the amount of used and available space on your
      file systems, just like df, but in colors. The output format is
      completely customizable.

  * **✓ [rsync](http://rsync.samba.org/): A program for synchronizing files over a network**

      Rsync uses a reliable algorithm to bring remote and host files
      into sync very quickly. Rsync is fast because it just sends the
      differences in the files over the network instead of sending the
      complete files. Rsync is often used as a very powerful mirroring
      process or just as a more capable replacement for the rcp
      command. A technical report which describes the rsync algorithm
      is included in this package.

  * **[srm](http://srm.sourceforge.net/): Secure file deletion**

      srm is a secure replacement for rm(1). Unlike the standard rm,
      it overwrites the data in the target files before unlinkg them.
      This prevents command-line recovery of the data by examining the
      raw block device. It may also help frustrate physical
      examination of the disk, although it's unlikely that completely
      protects against this type of recovery.

  * **[star](http://freecode.com/projects/star): An archiving tool with ACL support**

      Star saves many files together into a single tape or disk
      archive, and can restore individual files from the archive. Star
      supports ACL.

  * **[symlinks](http://ibiblio.org/pub/Linux/utils/file/): A utility which maintains a system's symbolic links**

      The symlinks utility performs maintenance on symbolic links.
      Symlinks checks for symlink problems, including dangling
      symlinks which point to nonexistent files. Symlinks can also
      automatically convert absolute symlinks to relative symlinks.
      Install the symlinks package if you need a program for
      maintaining symlinks on your system.

  * **[tree](http://mama.indstate.edu/users/ice/tree/): File system tree viewer**

      The tree utility recursively displays the contents of
      directories in a tree-like format. Tree is basically a UNIX port
      of the DOS tree utility.


```
dnf install fdupes filelight krename krusader lsyncd mc mmv ncdu pydf \
  rsync srm star symlinks tree
```

[back to index](#index)

***
## File Systems
  * **[btrfs-progs](http://btrfs.wiki.kernel.org/index.php/Main_Page): Userspace programs for btrfs**

      The btrfs-progs package provides all the userspace programs
      needed to create, check, modify and correct any inconsistencies
      in the btrfs filesystem.

  * **[cachefilesd](http://people.redhat.com/~dhowells/fscache/): CacheFiles user-space management daemon**

      The cachefilesd daemon manages the caching files and directory
      that are that are used by network file systems such a AFS and
      NFS to do persistent caching to the local disk.

  * **[curlftpfs](http://curlftpfs.sourceforge.net/): CurlFtpFS is a filesystem for accessing FTP hosts based on FUSE and libcurl**

      CurlFtpFS is a filesystem for accessing FTP hosts based on FUSE
      and libcurl. It features SSL support, connecting through
      tunneling HTTP proxies, and automatically reconnecting if the
      server times out.

  * **[fuse-dislocker](https://github.com/Aorimn/dislocker): FUSE filesystem to access BitLocker encrypted volumes**

      Dislocker has been designed to read BitLocker encrypted
      partitions ("drives") under a Linux system. The driver has the
      capability to read/write partitions encrypted using Microsoft
      Windows Vista, 7, 8, 8.1 and 10 (AES-CBC, AES-XTS, 128 or 256
      bits, with or without the Elephant diffuser, encrypted
      partitions); BitLocker-To-Go encrypted partitions (USB/FAT32
      partitions). A mount point needs to be given to dislocker-fuse.
      Once keys are decrypted, a file named 'dislocker-file' appears
      into this provided mount point. This file is a virtual NTFS
      partition, it can be mounted as any NTFS partition and [...]

  * **rpmfusion-free [fuse-exfat](https://github.com/relan/exfat): Free exFAT file system implementation**

      This driver is the first free exFAT file system implementation
      with write support. exFAT is a simple file system created by
      Microsoft. It is intended to replace FAT32 removing some of it's
      limitations. exFAT is a standard FS for SDXC memory cards.

  * **[fuse-sshfs](https://github.com/libfuse/sshfs): FUSE-Filesystem to access remote filesystems via SSH**

      This is a FUSE-filesystem client based on the SSH File Transfer
      Protocol. Since most SSH servers already support this protocol
      it is very easy to set up: i.e. on the server side there's
      nothing to do. On the client side mounting the filesystem is as
      easy as logging into the server with ssh.

  * **[fuse-zip](https://bitbucket.org/agalanin/fuse-zip/): Filesystem to navigate, extract, create and modify ZIP archives**

      fuse-zip is a FUSE file system to navigate, extract, create and
      modify ZIP archives based in libzip implemented in C++. With
      fuse-zip you really can work with ZIP archives as real
      directories. Unlike KIO or Gnome VFS, it can be used in any
      application without modifications. Unlike other FUSE
      filesystems, only fuse-zip provides write support to ZIP
      archives. Also, fuse-zip is faster that all known
      implementations on large archives with many files.

  * **[glusterfs](http://docs.gluster.org/): Distributed File System**

      GlusterFS is a distributed file-system capable of scaling to
      several petabytes. It aggregates various storage bricks over
      Infiniband RDMA or TCP/IP interconnect into one large parallel
      network file system. GlusterFS is one of the most sophisticated
      file systems in terms of features and extensibility. It borrows
      a powerful concept called Translators from GNU Hurd kernel. Much
      of the code in GlusterFS is in user space and easily manageable.
      This package includes the glusterfs binary, the glusterfsd
      daemon and the libglusterfs and glusterfs translator modules
      common to both GlusterFS server and client framework.

  * **[gparted](http://gparted.org): Gnome Partition Editor**

      GParted stands for Gnome Partition Editor and is a graphical
      frontend to libparted. Among other features it supports
      creating, resizing, moving and copying of partitions. Also
      several (optional) filesystem tools provide support for
      filesystems not included in libparted. These optional packages
      will be detected at runtime and don't require a rebuild of
      GParted

  * **[kpartx](http://christophe.varoqui.free.fr/): Partition device manager for device-mapper devices**

      kpartx manages partition creation and removal for device-mapper
      devices.

  * **[nilfs-utils](http://nilfs.sourceforge.net): Utilities for managing NILFS v2 filesystems**

      Userspace utilities for creating and mounting NILFS v2
      filesystems.

  * **[ntfsprogs](http://www.ntfs-3g.org/): NTFS filesystem libraries and utilities**

      The ntfsprogs package currently consists of a library and
      utilities such as mkntfs, ntfscat, ntfsls, ntfsresize, and
      ntfsundelete (for a full list of included utilities see man 8
      ntfsprogs after installation).

  * **[parted](http://www.gnu.org/software/parted): The GNU disk partition manipulation program**

      The GNU Parted program allows you to create, destroy, resize,
      move, and copy hard disk partitions. Parted can be used for
      creating space for new operating systems, reorganizing disk
      usage, and copying data to new hard disks.

  * **[pcmanfm](http://pcmanfm.sourceforge.net/): Extremly fast and lightweight file manager**

      PCMan File Manager is an extremly fast and lightweight file
      manager which features tabbed browsing and user-friendly
      interface.

  * **[pmount](http://pmount.alioth.debian.org/): Enable normal user mount**

      pmount ("policy mount") is a wrapper around the standard mount
      program which permits normal users to mount removable devices
      without a matching /etc/fstab entry. Be warned that pmount is
      installed setuid root.

  * **✓ [squashfs-tools](http://squashfs.sourceforge.net/): Utility for the creation of squashfs filesystems**

      Squashfs is a highly compressed read-only filesystem for Linux.
      This package contains the utilities for manipulating squashfs
      filesystems.

  * **[zfs-fuse](https://github.com/gordan-bobic/zfs-fuse): ZFS ported to Linux FUSE**

      ZFS is an advanced modern general-purpose filesystem from Sun
      Microsystems, originally designed for Solaris/OpenSolaris. This
      project is a port of ZFS to the FUSE framework for the Linux
      operating system.


```
dnf install btrfs-progs cachefilesd curlftpfs fuse-dislocker \
  fuse-exfat fuse-sshfs fuse-zip glusterfs gparted kpartx nilfs-utils \
  ntfsprogs parted pcmanfm pmount squashfs-tools zfs-fuse
```

[back to index](#index)

***
## Mathematics
  * **[cantor](https://edu.kde.org/cantor/): KDE Frontend to Mathematical Software**

      KDE Frontend to Mathematical Software.

  * **[coq](https://coq.inria.fr/): Proof management system**

      Coq is a formal proof management system. It allows for the
      development of theorems through first order logic that are
      mechanically checked by the machine. Sets of definitions and
      theorems can be saved as compiled modules and loaded into the
      system. This package provides the main Coq binary without an
      optional IDE, Coqide.

  * **✓ [galculator](http://galculator.mnim.org/): GTK 3 based scientific calculator**

      A GTK 3 based scientific calculator with ordinary notation,
      reverse polish notation, a formula entry mode, different number
      bases, and different units of angular measure.

  * **[giac](http://www-fourier.ujf-grenoble.fr/~parisse/giac.html): Computer Algebra System, Symbolic calculus, Geometry**

      Giac is a Computer Algebra System made by Bernard Parisse. It
      provides features from the C/C++ libraries PARI, NTL
      (arithmetic), GSL (numerics), GMP (big integers), MPFR
      (bigfloats) and also - Efficient algorithms for multivariate
      polynomial operations (product, GCD, factorization, groebner
      bases), - Symbolic computations: solver, simplifications,
      limits/series, integration, - Linear algebra with numerical or
      symbolic coefficients. - Partial Maple and TI compatibility. -
      It has interfaces in texmacs and sagemath. It consists of: - a
      C++ library (libgiac) - a command line interpreter [...]

  * **[mathomatic](http://www.mathomatic.org/math/): Small, portable symbolic math program**

      Mathomatic is a small, portable symbolic math program that can
      automatically solve, simplify, differentiate, combine, and
      compare algebraic equations, perform polynomial and complex
      arithmetic, etc. It was written by George Gesslein II and has
      been under development since 1986.

  * **[maxima](http://maxima.sourceforge.net/): Symbolic Computation Program**

      Maxima is a full symbolic computation program. It is full
      featured doing symbolic manipulation of polynomials, matrices,
      rational functions, integration, Todd-coxeter, graphing,
      bigfloats. It has a symbolic debugger source level debugger for
      maxima code. Maxima is based on the original Macsyma developed
      at MIT in the 1970's.

  * **[octave](http://www.octave.org): A high-level language for numerical computations**

      GNU Octave is a high-level language, primarily intended for
      numerical computations. It provides a convenient command line
      interface for solving linear and nonlinear problems numerically,
      and for performing other numerical experiments using a language
      that is mostly compatible with Matlab. It may also be used as a
      batch-oriented language. Octave has extensive tools for solving
      common numerical linear algebra problems, finding the roots of
      nonlinear equations, integrating ordinary functions,
      manipulating polynomials, and integrating ordinary differential
      and differential-algebraic equations. It is easily [...]

  * **[qhull](http://www.qhull.org): General dimension convex hull programs**

      Qhull is a general dimension convex hull program that reads a
      set of points from stdin, and outputs the smallest convex set
      that contains the points to stdout. It also generates Delaunay
      triangulations, Voronoi diagrams, furthest-site Voronoi
      diagrams, and halfspace intersections about a point.

  * **[speedcrunch](http://www.speedcrunch.org): A fast power user calculator**

      SpeedCrunch is a fast, high precision and powerful desktop
      calculator. Among its distinctive features are a scrollable
      display, up to 50 decimal precisions, unlimited variable
      storage, intelligent automatic completion full keyboard-friendly
      and more than 15 built-in math function.


```
dnf install cantor coq galculator giac mathomatic maxima octave qhull \
  speedcrunch
```

[back to index](#index)

***
## Miscellaneous
  * **[anki](https://apps.ankiweb.net/): Flashcard program for using space repetition learning**

      Anki is a program designed to help you remember facts (such as
      words and phrases in a foreign language) as easily, quickly and
      efficiently as possible. Anki is based on a theory called spaced
      repetition.

  * **[anyremote](http://anyremote.sourceforge.net/): Remote control through Wi-Fi or bluetooth connection**

      Remote control software for applications using Wi-Fi or
      Bluetooth.

  * **[barcode](http://www.gnu.org/software/barcode/): generates barcodes from text strings**

      Barcode is meant to solve most needs in barcode creation with a
      conventional printer. It can create printouts for the
      conventional product tagging standards: UPC-A, UPC-E, EAN-13,
      EAN-8, ISBN, as well as a few other formats. Ouput is generated
      as either Postscript or Encapsulated Postscript.

  * **[colord](https://www.freedesktop.org/software/colord/): Color daemon**

      colord is a low level system activated daemon that maps color
      devices to color profiles in the system context.

  * **[cowsay](https://github.com/tnalpgge/rank-amateur-cowsay): Configurable speaking/thinking cow**

      cowsay is a configurable talking cow, written in Perl. It
      operates much as the figlet program does, and it written in the
      same spirit of silliness. It generates ASCII pictures of a cow
      with a message. It can also generate pictures of other animals.

  * **[dmenu](http://tools.suckless.org/dmenu): Generic menu for X**

      Dynamic menu is a generic menu for X, originally designed for
      dwm. It manages huge amounts (up to 10.000 and more) of user
      defined menu items efficiently.

  * **[dxpc](http://www.vigor.nu/dxpc/): A Differential X Protocol Compressor**

      dxpc is an X protocol compressor designed to improve the speed
      of X11 applications run over low-bandwidth links (such as dialup
      PPP connections or ADSL).

  * **[expect](https://core.tcl.tk/expect/index): A program-script interaction and testing utility**

      Expect is a tcl application for automating and testing
      interactive applications such as telnet, ftp, passwd, fsck,
      rlogin, tip, etc. Expect makes it easy for a script to control
      another program and interact with it. This package contains
      expect and some scripts that use it.

  * **[fossil](https://www.fossil-scm.org/): A distributed SCM with bug tracking and wiki**

      Fossil is a simple, high-reliability, distributed software
      configuration management with distributed bug tracking,
      distributed wiki and built-in web interface.

  * **✓ [gnome-tweaks](https://wiki.gnome.org/action/show/Apps/Tweaks): Customize advanced GNOME 3 options**

      GNOME Tweaks allows adjusting advanced configuration settings in
      GNOME 3. This includes things like the fonts used in user
      interface elements, alternative user interface themes, changes
      in window management behavior, GNOME Shell appearance and
      extension, etc.

  * **[gpsd](http://catb.org/gpsd/): Service daemon for mediating access to a GPS**

      gpsd is a service daemon that mediates access to a GPS sensor
      connected to the host computer by serial or USB interface,
      making its data on the location/course/velocity of the sensor
      available to be queried on TCP port 2947 of the host computer.
      With gpsd, multiple GPS client applications (such as
      navigational and war-driving software) can share access to a GPS
      without contention or loss of data. Also, gpsd responds to
      queries with a format that is substantially easier to parse than
      NMEA 0183.

  * **[gpsdrive](http://www.gpsdrive.de/index.shtml): A GPS based navigation tool**

      Gpsdrive is a map-based navigation system. It displays your
      position on a zoomable map provided from a NMEA-capable GPS
      receiver. The maps are autoselected for the best resolution,
      depending of your position, and the displayed image can be
      zoomed. Maps can be downloaded from the Internet with one mouse
      click. The program provides information about speed, direction,
      bearing, arrival time, actual position, and target position.
      Speech output is also available. MySQL is supported.

  * **[grass](https://grass.osgeo.org): GRASS GIS - Geographic Resources Analysis Support System**

      GRASS (Geographic Resources Analysis Support System) is a
      Geographic Information System (GIS) used for geospatial data
      management and analysis, image processing, graphics/maps
      production, spatial modeling, and visualization. GRASS is
      currently used in academic and commercial settings around the
      world, as well as by many governmental agencies and
      environmental consulting companies.

  * **[ignuit](http://crash.ihug.co.nz/~trmusson/programs.html): Memorization aid based on the Leitner flashcard system**

      Ignuit is a memorization aid based on the Leitner flashcard
      system. It has a GNOME look and feel, a good selection of quiz
      options, and supports UTF-8. Cards can include embedded audio,
      images, and mathematical formulae (via LaTeX). It can import and
      export several file formats, including CSV. Ignuit can be used
      for both long-term learning and cramming.

  * **[iso-codes](https://salsa.debian.org/iso-codes-team/iso-codes): ISO code lists and translations**

      This package provides the ISO 639 Language code list, the ISO
      4217 Currency code list, the ISO 3166 Territory code list, and
      ISO 3166-2 sub-territory lists, and all their translations in
      gettext format.

  * **[josm](https://josm.openstreetmap.de/): An editor for OpenStreetMap (OSM)**

      JOSM is an editor for OpenStreetMap (OSM) written in Java
      Currently it supports loading stand alone GPX track data from
      the OSM database, loading and editing existing nodes, ways,
      metadata tags and relations. OpenStreetMap is a project aimed
      squarely at creating and providing free geographic data such as
      street maps to anyone who wants them. The project was started
      because most maps you think of as free actually have legal or
      technical restrictions on their use, holding back people from
      using them in creative, productive or unexpected ways.

  * **[kanyremote](http://anyremote.sourceforge.net/): KDE frontend for anyRemote**

      kAnyRemote package is KDE GUI frontend for anyRemote
      (http://anyremote.sourceforge.net/) - remote control software
      for applications using Bluetooth or Wi-Fi.

  * **[mapserver](http://www.mapserver.org): Environment for building spatially-enabled internet applications**

      Mapserver is an internet mapping program that converts GIS data
      to map images in real time. With appropriate interface pages,
      Mapserver can provide an interactive internet map based on
      custom GIS data.

  * **[nfoview](http://otsaloma.github.io/nfoview/): A viewer for NFO files**

      NFO Viewer is a simple viewer for NFO files, which are "ASCII"
      art in the CP437 codepage. The advantages of using NFO Viewer
      instead of a text editor are preset font and encoding settings,
      automatic window size and clickable hyperlinks.

  * **[numlockx](http://ktown.kde.org/~seli/numlockx/): Turns on NumLock after starting X**

      Turns on NumLock after starting X

  * **[open-vm-tools](https://github.com/vmware/open-vm-tools): Open Virtual Machine Tools for virtual machines hosted on VMware**

      The open-vm-tools project is an open source implementation of
      VMware Tools. It is a suite of open source virtualization
      utilities and drivers to improve the functionality, user
      experience and administration of VMware virtual machines. This
      package contains only the core user-space programs and libraries
      of open-vm-tools.

  * **[openscad](http://www.openscad.org/): The Programmers Solid 3D CAD Modeller**

      OpenSCAD is a software for creating solid 3D CAD objects. Unlike
      most free software for creating 3D models (such as the famous
      application Blender) it does not focus on the artistic aspects
      of 3D modeling but instead on the CAD aspects. Thus it might be
      the application you are looking for when you are planning to
      create 3D models of machine parts but pretty sure is not what
      you are looking for when you are more interested in creating
      computer-animated movies.

  * **[qgis](http://www.qgis.org): A user friendly Open Source Geographic Information System**

      Geographic Information System (GIS) manages, analyzes, and
      displays databases of geographic information. QGIS supports
      shape file viewing and editing, spatial data storage with
      PostgreSQL/PostGIS, projection on-the-fly, map composition, and
      a number of other features via a plugin interface. QGIS also
      supports display of various geo-referenced raster and Digital
      Elevation Model (DEM) formats including GeoTIFF, Arc/Info ASCII
      Grid, and USGS ASCII DEM.

  * **[qrencode](http://fukuchi.org/works/qrencode/): Generate QR 2D barcodes**

      Qrencode is a utility software using libqrencode to encode
      string data in a QR Code and save as a PNG image.

  * **[redshift](http://jonls.dk/redshift/): Adjusts the color temperature of your screen according to time of day**

      Redshift adjusts the color temperature of your screen according
      to your surroundings. This may help your eyes hurt less if you
      are working in front of the screen at night. The color
      temperature is set according to the position of the sun. A
      different color temperature is set during night and daytime.
      During twilight and early morning, the color temperature
      transitions smoothly from night to daytime temperature to allow
      your eyes to slowly adapt. This package provides the base
      program.

  * **[sbd](https://github.com/clusterlabs/sbd): Storage-based death**

      This package contains the storage-based death functionality.

  * **[sparkleshare](http://www.sparkleshare.org/): Share and collaborate by syncing with any Git repository instantly**

      SparkleShare creates a special folder on your computer. You can
      add remotely hosted folders (or "projects") to this folder.
      These projects will be automatically kept in sync with both the
      host and all of your peers when someone adds, removes or edits a
      file.

  * **[tracker](https://wiki.gnome.org/Projects/Tracker): Desktop-neutral metadata database and search tool**

      Tracker is a powerful desktop-neutral first class object
      database, tag/metadata database and search tool. It consists of
      a common object database that allows entities to have an almost
      infinite number of properties, metadata (both embedded/harvested
      as well as user definable), a comprehensive database of
      keywords/tags and links to other entities. It provides
      additional features for file based objects including context
      linking and audit trails for a file object. Metadata indexers
      are provided by the tracker-miners package.

  * **[workrave](http://www.workrave.org/): Program that assists in the recovery and prevention of RSI**

      Workrave is a program that assists in the recovery and
      prevention of Repetitive Strain Injury (RSI). The program
      frequently alerts you to take micro-pauses, rest breaks and
      restricts you to your daily limit.

  * **[xcowsay](http://www.doof.me.uk/xcowsay): Displays a cute cow and message on your desktop**

      xcowsay displays a cute cow and message on your desktop. The
      message can be text or images (with xcowdream) xcowsay can run
      in daemon mode for sending your cow message with DBus. Inspired
      by the original cowsay.

  * **[xmobar](https://hackage.haskell.org/package/xmobar): A minimalistic text-based status bar**

      Xmobar is a minimalistic text based status bar. Inspired by the
      Ion3 status bar, it supports similar features, like dynamic
      color management, output templates, and extensibility through
      plugins.

  * **[xplanet](http://xplanet.sourceforge.net): Render a planetary image into an X window**

      Xplanet is similar to Xearth, where an image of the earth is
      rendered into an X window. Azimuthal, Mercator, Mollweide,
      orthographic, or rectangular projections can be displayed as
      well as a window with a globe the user can rotate interactively.
      The other terrestrial planets may also be displayed. The Xplanet
      home page has links to locations with map files.

  * **[zenity](https://wiki.gnome.org/Projects/Zenity): Display dialog boxes from shell scripts**

      Zenity lets you display Gtk+ dialog boxes from the command line
      and through shell scripts. It is similar to gdialog, but is
      intended to be saner. It comes from the same family as dialog,
      Xdialog, and cdialog.


```
dnf install anki anyremote barcode colord cowsay dmenu dxpc expect \
  fossil gnome-tweaks gpsd gpsdrive grass ignuit iso-codes josm \
  kanyremote mapserver nfoview numlockx open-vm-tools openscad qgis \
  qrencode redshift sbd sparkleshare tracker workrave xcowsay xmobar \
  xplanet zenity
```

[back to index](#index)

***
## Multimedia
  * **[mediainfo](http://mediaarea.net/MediaInfo): Supplies technical and tag information about a video or audio file (CLI)**

      MediaInfo CLI (Command Line Interface). What information can I
      get from MediaInfo? ' General: title, author, director, album,
      track number, date, duration... ' Video: codec, aspect, fps,
      bitrate... ' Audio: codec, sample rate, channels, language,
      bitrate... ' Text: language of subtitle ' Chapters: number of
      chapters, list of chapters DivX, XviD, H263, H.263, H264, x264,
      ASP, AVC, iTunes, MPEG-1, MPEG1, MPEG-2, MPEG2, MPEG-4, MPEG4,
      MP4, M4A, M4V, QuickTime, RealVideo, RealAudio, RA, RM,
      MSMPEG4v1, MSMPEG4v2, MSMPEG4v3, VOB, DVD, WMA, VMW, ASF, 3GP,
      3GPP, 3GP2 What format (container) does MediaInfo support? [...]

  * **rpmfusion-free [rtmpdump](http://rtmpdump.mplayerhq.hu/): Toolkit for RTMP streams**

      rtmpdump is a toolkit for RTMP streams. All forms of RTMP are
      supported, including rtmp://, rtmpt://, rtmpe://, rtmpte://, and
      rtmps://.

  * **✓ rpmfusion-free-updates [vlc](https://www.videolan.org): The cross-platform open-source multimedia framework, player and server**

      VLC media player is a highly portable multimedia player and
      multimedia framework capable of reading most audio and video
      formats as well as DVDs, Audio CDs VCDs, and various streaming
      protocols. It can also be used as a media converter or a server
      to stream in uni-cast or multi-cast in IPv4 or IPv6 on networks.


```
dnf install mediainfo rtmpdump vlc
```

[back to index](#index)

***
## Multimedia - Audio
  * **[ardour5](http://ardour.org): Digital Audio Workstation**

      Ardour is a multi-channel digital audio workstation, allowing
      users to record, edit, mix and master audio and MIDI projects.
      It is targeted at audio engineers, musicians, soundtrack editors
      and composers.

  * **[audacious](http://audacious-media-player.org/): Advanced audio player**

      Audacious is an advanced audio player. It is free, lightweight,
      currently based on GTK+ 2, runs on Linux and many other 'nix
      platforms and is focused on audio quality and supporting a wide
      range of audio codecs. It still features an alternative skinned
      user interface (based on Winamp 2.x skins). Historically, it
      started as a fork of Beep Media Player (BMP), which itself
      forked from XMMS.

  * **rpmfusion-free [audacity-freeworld](https://www.audacityteam.org): Multitrack audio editor**

      Audacity is a cross-platform multitrack audio editor. It allows
      you to record sounds directly or to import files in various
      formats. It features a few simple effects, all of the editing
      features you should need, and unlimited undo. The GUI was built
      with wxWidgets and the audio I/O supports PulseAudio, OSS and
      ALSA under Linux. This build has support for mp3 and ffmpeg
      import/export.

  * **[beets](http://beets.io): Music library manager and MusicBrainz tagger**

      The purpose of beets is to get your music collection right once
      and for all. It catalogs your collection, automatically
      improving its meta-data as it goes using the MusicBrainz
      database. Then it provides a bouquet of tools for manipulating
      and accessing your music. Because beets is designed as a
      library, it can do almost anything you can imagine for your
      music collection. Via plugins, beets becomes a panacea: - Fetch
      or calculate all the meta-data you could possibly need: album
      art, lyrics, genres, tempos, ReplayGain levels, or acoustic
      fingerprints. - Get meta-data from MusicBrainz, Discogs, [...]

  * **[clementine](https://www.clementine-player.org/): A music player and library organizer**

      Clementine is a multi-platform music player. It is inspired by
      Amarok 1.4, focusing on a fast and easy-to-use interface for
      searching and playing your music.

  * **[flacon](https://flacon.github.io/): Audio File Encoder**

      Flacon extracts individual tracks from one big audio file
      containing the entire album of music and saves them as separate
      audio files. To do this, it uses information from the
      appropriate CUE file. Besides, Flacon makes it possible to
      conveniently revise or specify tags both for all tracks at once
      or for each tag separately.

  * **[kid3](http://kid3.sourceforge.net/): Efficient KDE ID3 tag editor**

      If you want to easily tag multiple MP3, Ogg/Vorbis, FLAC, MPC,
      MP4/AAC, MP2, Speex, TrueAudio, WavPack, WMA, WAV, and AIFF
      files (e.g. full albums) without typing the same information
      again and again and have control over both ID3v1 and ID3v2 tags,
      then Kid3 is the program you are looking for.

  * **[mpc](http://www.musicpd.org/): Command-line client for MPD**

      A client for MPD, the Music Player Daemon. mpc connects to a MPD
      running on a machine via a network.

  * **rpmfusion-free [mpd](https://www.musicpd.org): The Music Player Daemon**

      Music Player Daemon (MPD) is a flexible, powerful, server-side
      application for playing music. Through plugins and libraries it
      can play a variety of sound files (e.g., OGG, MP3, FLAC, AAC,
      WAV) and can be controlled remotely via its network protocol. It
      can be used as a desktop music player, but is also great for
      streaming music to a stereo system over a local network. There
      are many GUI and command-line applications to choose from that
      act as a front-end for browsing and playing your MPD music
      collection.

  * **[ncmpc](http://www.musicpd.org/): A curses client for the Music Player Daemon (MPD)**

      ncmpc is a curses client for the Music Player Daemon (MPD).
      ncmpc connects to a MPD running on a machine on the local
      network, and controls this with an interface inspired by cplay.

  * **[pasystray](https://github.com/christophgysin/pasystray): PulseAudio system tray**

      A replacement for the deprecated padevchooser. pasystray allows
      setting the default PulseAudio source/sink and moving streams on
      the fly between sources/sinks without restarting the client
      applications.

  * **✓ [pavucontrol](http://freedesktop.org/software/pulseaudio/pavucontrol): Volume control for PulseAudio**

      PulseAudio Volume Control (pavucontrol) is a simple GTK based
      volume control tool ("mixer") for the PulseAudio sound server.
      In contrast to classic mixer tools this one allows you to
      control both the volume of hardware devices and of each playback
      stream separately.


  Protip: Use this to route audio to Bluetooth devices, for example.

  * **[picard](https://picard.musicbrainz.org/): MusicBrainz-based audio tagger**

      Picard is an audio tagging application using data from the
      MusicBrainz database. The tagger is album or release oriented,
      rather than track-oriented.

  * **[pogo](https://launchpad.net/pogo): Probably the simplest and fastest audio player for Linux**

      Pogo's elementary-inspired design uses the screen-space very
      efficiently. It is especially well-suited for people who
      organize their music by albums on the harddrive. The main
      interface components are a directory tree and a playlist that
      groups albums in an innovative way. Pogo is a fork of Decibel
      Audio Player.

  * **[qmmp](http://qmmp.ylsoftware.com/): Qt-based multimedia player**

      This program is an audio-player, written with help of Qt
      library. The user interface is similar to winamp or xmms. Main
      opportunities:  ' Winamp and xmms skins support         '
      plugins support       ' MPEG1 layer 2/3 support       ' Ogg
      Vorbis support    ' native FLAC support   ' WavePack support
      ' ModPlug support       ' PCM WAVE support      ' CD Audio
      support      ' CUE sheet support     ' ALSA sound output     '
      JACK sound output     ' OSS sound output      ' PulseAudio
      output     ' Last.fm/Libre.fm scrobbler    ' D-Bus support
      ' Spectrum Analyzer     ' projectM visualization        ' [...]

  * **[rubberband](http://www.breakfastquay.com/rubberband/): Audio time-stretching and pitch-shifting library**

      Rubber Band is a library and utility program that permits you to
      change the tempo and pitch of an audio recording independently
      of one another.

  * **[sound-juicer](https://wiki.gnome.org/Apps/SoundJuicer): Clean and lean CD ripper**

      GStreamer-based CD ripping tool. Saves audio CDs to Ogg/vorbis.

  * **[sox](http://sox.sourceforge.net/): A general purpose sound file conversion tool**

      SoX (Sound eXchange) is a sound file format converter. SoX can
      convert between many different digitized sound formats and
      perform simple sound manipulation functions, including sound
      effects.

  * **[xwax](http://www.xwax.org): Open source vinyl emulation software for Linux**

      xwax is open-source vinyl emulation software for Linux. It
      allows DJs and turntablists to playback digital audio files
      (MP3, Ogg Vorbis, FLAC, AAC and more), controlled using a normal
      pair of turntables via timecoded vinyls. It's designed for both
      beat mixing and scratch mixing. Needle drops, pitch changes,
      scratching, spinbacks and rewinds are all supported, and feel
      just like the audio is pressed onto the vinyl itself. The focus
      is on an accurate vinyl feel which is efficient, stable and
      fast.

  * **[yarock](https://launchpad.net/yarock): Lightweight, beautiful music player**

      Yarock is a music player designed to provide a clean, simple and
      beautiful music collection based on album cover-art.


```
dnf install ardour5 audacious audacity-freeworld beets clementine \
  flacon kid3 mpc mpd ncmpc pasystray pavucontrol picard pogo qmmp \
  rubberband sound-juicer sox xwax yarock
```

[back to index](#index)

***
## Multimedia - Codecs
  * **[codec2](http://rowetel.com/codec2.html): Next-Generation Digital Voice for Two-Way Radio**

      Codec 2 is an open source (LGPL licensed) speech codec for 2400
      bit/s and below. This is the runtime library package.

  * **[flac](https://www.xiph.org/flac/): An encoder/decoder for the Free Lossless Audio Codec**

      FLAC stands for Free Lossless Audio Codec. Grossly
      oversimplified, FLAC is similar to Ogg Vorbis, but lossless. The
      FLAC project consists of the stream format, reference encoders
      and decoders in library form, flac, a command-line program to
      encode and decode FLAC files, metaflac, a command-line metadata
      editor for FLAC files and input plugins for various music
      players. This package contains the command-line tools and
      documentation.

  * **[gsm](http://www.quut.com/gsm/): Shared libraries for GSM speech compressor**

      Contains runtime shared libraries for libgsm, an implementation
      of the European GSM 06.10 provisional standard for full-rate
      speech transcoding, prI-ETS 300 036, which uses RPE/LTP
      (residual pulse excitation/long term prediction) coding at 13
      kbit/s. GSM 06.10 compresses frames of 162 13-bit samples (8 kHz
      sampling rate, i.e. a frame rate of 50 Hz) into 260 bits; for
      compatibility with typical UNIX applications, our implementation
      turns frames of 160 16-bit linear samples into 33-byte frames
      (1650 Bytes/s). The quality of the algorithm is good enough for
      reliable speaker recognition; even music often survives [...]

  * **[opus](https://www.opus-codec.org/): An audio codec for use in low-delay speech and audio communication**

      The Opus codec is designed for interactive speech and audio
      transmission over the Internet. It is designed by the IETF Codec
      Working Group and incorporates technology from Skype's SILK
      codec and Xiph.Org's CELT codec.

  * **[speex](https://www.speex.org/): A voice compression format (codec)**

      Speex is a patent-free compression format designed especially
      for speech. It is specialized for voice communications at low
      bit-rates in the 2-45 kbps range. Possible applications include
      Voice over IP (VoIP), Internet audio streaming, audio books, and
      archiving of speech data (e.g. voice mail).


```
dnf install codec2 flac gsm opus speex
```

[back to index](#index)

***
## Multimedia - Codecs for GStreamer

  Protip: Install these if you cannot play MP3s.

  * **[gstreamer-plugins-base](http://gstreamer.freedesktop.org/): GStreamer streaming media framework base plug-ins**

      GStreamer is a streaming media framework, based on graphs of
      filters which operate on media data. Applications using this
      library can do anything from real-time sound processing to
      playing videos, and just about anything else media-related. Its
      plugin-based architecture means that new data types or
      processing capabilities can be added simply by installing new
      plug-ins. This package contains a set of well-maintained base
      plug-ins.

  * **[gstreamer-plugins-base-tools](http://gstreamer.freedesktop.org/): tools for GStreamer streaming media framework base plugins**

      GStreamer is a streaming media framework, based on graphs of
      filters which operate on media data. Applications using this
      library can do anything from real-time sound processing to
      playing videos, and just about anything else media-related. Its
      plugin-based architecture means that new data types or
      processing capabilities can be added simply by installing new
      plug-ins. This package contains the command-line tools for the
      base plugins. These include: ' gst-discoverer

  * **[gstreamer-plugins-espeak](http://wiki.sugarlabs.org/go/Activity_Team/gst-plugins-espeak): A simple gstreamer plugin to use espeak**

      A simple gstreamer plugin to use espeak as a sound source. It
      was developed to simplify the espeak usage in the Sugar Speak
      activity. The plugin uses given text to produce audio output.

  * **[gstreamer-plugins-fc](http://xmms-fc.sourceforge.net): Future Composer input plugin for GStreamer 0.10.x**

      This is an input plugin for GStreamer which can play back Future
      Composer music files from AMIGA. Song-length detection and seek
      are implemented, too.

  * **[gstreamer1-plugins-bad-free](http://gstreamer.freedesktop.org/): GStreamer streaming media framework "bad" plugins**

      GStreamer is a streaming media framework, based on graphs of
      elements which operate on media data. This package contains
      plug-ins that aren't tested well enough, or the code is not of
      good enough quality.

  * **[gstreamer1-plugins-bad-free-extras](http://gstreamer.freedesktop.org/): Extra GStreamer "bad" plugins (less often used "bad" plugins)**

      GStreamer is a streaming media framework, based on graphs of
      elements which operate on media data. gstreamer-plugins-bad
      contains plug-ins that aren't tested well enough, or the code is
      not of good enough quality. This package (gstreamer1-plugins-
      bad-free-extras) contains extra "bad" plugins for sources
      (mythtv), sinks (fbdev) and effects (pitch) which are not used
      very much and require additional libraries to be installed.

  * **[gstreamer1-plugins-bad-free-fluidsynth](http://gstreamer.freedesktop.org/): GStreamer "bad" plugins fluidsynth plugin**

      GStreamer is a streaming media framework, based on graphs of
      elements which operate on media data. gstreamer-plugins-bad
      contains plug-ins that aren't tested well enough, or the code is
      not of good enough quality. This package (gstreamer1-plugins-
      bad-free-fluidsynth) contains the fluidsynth plugin which allows
      playback of midi files.

  * **rpmfusion-free [gstreamer1-plugins-bad-freeworld](https://gstreamer.freedesktop.org/): GStreamer 1.0 streaming media framework "bad" plug-ins**

      GStreamer is a streaming media framework, based on graphs of
      elements which operate on media data. This package contains
      plug-ins that have licensing issues, aren't tested well enough,
      or the code is not of good enough quality.

  * **[gstreamer1-plugins-base](http://gstreamer.freedesktop.org/): GStreamer streaming media framework base plugins**

      GStreamer is a streaming media framework, based on graphs of
      filters which operate on media data. Applications using this
      library can do anything from real-time sound processing to
      playing videos, and just about anything else media-related. Its
      plugin-based architecture means that new data types or
      processing capabilities can be added simply by installing new
      plug-ins. This package contains a set of well-maintained base
      plug-ins.

  * **[gstreamer1-plugins-base-tools](http://gstreamer.freedesktop.org/): Tools for GStreamer streaming media framework base plugins**

      GStreamer is a streaming media framework, based on graphs of
      filters which operate on media data. Applications using this
      library can do anything from real-time sound processing to
      playing videos, and just about anything else media-related. Its
      plugin-based architecture means that new data types or
      processing capabilities can be added simply by installing new
      plug-ins. This package contains the command-line tools for the
      base plugins. These include: ' gst-discoverer

  * **[gstreamer1-plugins-entrans](http://gentrans.sourceforge.net/): GStreamer plug-ins from GEntrans**

      GEntrans is a software package providing a collection of plug-
      ins and tools for the GStreamer multimedia framework
      specifically geared towards transcoding and recording purposes.
      This package provides several GStreamer plugins from GEntrans.

  * **[gstreamer1-plugins-entrans-docs](http://gentrans.sourceforge.net/): Documentation for GStreamer plug-ins from GEntrans**

      GEntrans is a software package providing a collection of plug-
      ins and tools for the GStreamer multimedia framework
      specifically geared towards transcoding and recording purposes.
      This package provides documentation for several GStreamer
      plugins from GEntrans.

  * **[gstreamer1-plugins-fc](http://xmms-fc.sourceforge.net): Future Composer input plugin for GStreamer 1.0.x**

      This is an input plugin for GStreamer which can play back Future
      Composer music files from AMIGA. Song-length detection and seek
      are implemented, too.

  * **[gstreamer1-plugins-good](http://gstreamer.freedesktop.org/): GStreamer plugins with good code and licensing**

      GStreamer is a streaming media framework, based on graphs of
      filters which operate on media data. Applications using this
      library can do anything from real-time sound processing to
      playing videos, and just about anything else media-related. Its
      plugin-based architecture means that new data types or
      processing capabilities can be added simply by installing new
      plugins. GStreamer Good Plugins is a collection of well-
      supported plugins of good quality and under the LGPL license.

  * **[gstreamer1-plugins-good-extras](http://gstreamer.freedesktop.org/): Extra GStreamer plugins with good code and licensing**

      GStreamer is a streaming media framework, based on graphs of
      filters which operate on media data. GStreamer Good Plugins is a
      collection of well-supported plugins of good quality and under
      the LGPL license. gstreamer1-plugins-good-extras contains extra
      "good" plugins which are not used very much and require
      additional libraries to be installed.

  * **rpmfusion-free [gstreamer1-plugins-ugly](https://gstreamer.freedesktop.org/): GStreamer 1.0 streaming media framework "ugly" plug-ins**

      GStreamer is a streaming media framework, based on graphs of
      elements which operate on media data. This package contains
      well-written plug-ins that can't be shipped in gstreamer-
      plugins-good because: - the license is not LGPL - the license of
      the library is not LGPL - there are possible licensing issues
      with the code.


```
dnf install gstreamer-plugins-base gstreamer-plugins-base-tools \
  gstreamer-plugins-espeak gstreamer-plugins-fc \
  gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free-extras \
  gstreamer1-plugins-bad-free-fluidsynth \
  gstreamer1-plugins-bad-freeworld gstreamer1-plugins-base \
  gstreamer1-plugins-base-tools gstreamer1-plugins-entrans \
  gstreamer1-plugins-entrans-docs gstreamer1-plugins-fc \
  gstreamer1-plugins-good gstreamer1-plugins-good-extras \
  gstreamer1-plugins-ugly
```

[back to index](#index)

***
## Multimedia - Images/Photography
  * **[ImageMagick](http://www.imagemagick.org/): An X application for displaying and manipulating images**

      ImageMagick is an image display and manipulation tool for the X
      Window System. ImageMagick can read and write JPEG, TIFF, PNM,
      GIF, and Photo CD image formats. It can resize, rotate, sharpen,
      color reduce, or add special effects to an image, and when
      finished you can either save the completed work in the original
      format or a different one. ImageMagick also includes command
      line programs for creating animated or transparent .gifs,
      creating composite images, creating thumbnail images, and more.
      ImageMagick is one of your choices if you need a program to
      manipulate and display images. If you want to develop your [...]


  Protip: `mogrify -strip [imagefile]` to strip metadata from an image.

  * **[dcraw](http://www.dechifro.org/dcraw/): Tool for decoding raw image data from digital cameras**

      This package contains dcraw, a command line tool to decode raw
      image data downloaded from digital cameras.

  * **[exif](http://libexif.sourceforge.net/): Utility to show EXIF information hidden in JPEG files**

      Small command-line utility to show EXIF information hidden in
      JPEG files.

  * **[feh](http://feh.finalrewind.org): Fast command line image viewer using Imlib2**

      feh is a versatile and fast image viewer using imlib2, the
      premier image file handling library. feh has many features, from
      simple single file viewing, to multiple file modes using a
      slide-show or multiple windows. feh supports the creation of
      montages as index prints with many user-configurable options.

  * **fedora-modular [gimp](http://www.gimp.org/): GNU Image Manipulation Program**

      GIMP (GNU Image Manipulation Program) is a powerful image
      composition and editing program, which can be extremely useful
      for creating logos and other graphics for web pages. GIMP has
      many of the tools and filters you would expect to find in
      similar commercial offerings, and some interesting extras as
      well. GIMP provides a large image manipulation toolbox,
      including channel operations and layers, effects, sub-pixel
      imaging and anti-aliasing, and conversions, all with multi-level
      undo.

  * **[hugin](http://hugin.sourceforge.net/): A panoramic photo stitcher and more**

      hugin can be used to stitch multiple images together. The
      resulting image can span 360 degrees. Another common use is the
      creation of very high resolution pictures by combining multiple
      images. It uses the Panorama Tools as back-end to create high
      quality images

  * **[jasper](http://www.ece.uvic.ca/~frodo/jasper/): Implementation of the JPEG-2000 standard, Part 1**

      This package contains an implementation of the image compression
      standard JPEG-2000, Part 1. It consists of tools for conversion
      to and from the JP2 and JPC formats.

  * **[jhead](http://www.sentex.net/~mwandel/jhead/): Tool for displaying EXIF data embedded in JPEG images**

      Jhead displays and manipulates the non-image portions of EXIF
      formatted JPEG images, such as the images produced by most
      digital cameras.

  * **[luminance-hdr](http://qtpfsgui.sourceforge.net/): A graphical tool for creating and tone-mapping HDR images**

      Luminance is a graphical program for assembling bracketed photos
      into High Dynamic Range (HDR) images. It also provides a number
      of tone-mapping operators for creating low dynamic range
      versions of HDR images.

  * **[nip2](https://libvips.github.io/libvips/): Interactive tool for working with large images**

      nip2 is a graphical front end to the VIPS package. With nip2,
      rather than directly editing images, you build relationships
      between objects in a spreadsheet-like fashion. When you make a
      change somewhere, nip2 recalculates the objects affected by that
      change. Since it is demand-driven this update is very fast, even
      for very, very large images. nip2 is very good at creating
      pipelines of image manipulation operations. It is not very good
      for image editing tasks like touching up photographs. For that,
      a tool like the GIMP should be used instead.

  * **[nomacs](http://nomacs.org): Lightweight image viewer**

      nomacs is image viewer based on Qt5 library. nomacs is small,
      fast and able to handle the most common image formats.
      Additionally it is possible to synchronize multiple viewers
      running on the same computer or via LAN is possible. It allows
      to compare images and spot the differences e.g. schemes of
      architects to show the progress).

  * **[optipng](http://optipng.sourceforge.net/): PNG optimizer and converter**

      OptiPNG is a PNG optimizer that recompresses image files to a
      smaller size, without losing any information. This program also
      converts external formats (BMP, GIF, PNM and TIFF) to optimized
      PNG, and performs PNG integrity checks and corrections.

  * **[perl-Image-ExifTool](http://www.sno.phy.queensu.ca/%7Ephil/exiftool/): Utility for reading and writing image meta info**

      ExifTool is a Perl module with an included command-line
      application for reading and writing meta information in image,
      audio, and video files. It reads EXIF, GPS, IPTC, XMP, JFIF,
      MakerNotes, GeoTIFF, ICC Profile, Photoshop IRB, FlashPix, AFCP,
      and ID3 meta information from JPG, JP2, TIFF, GIF, PNG, MNG,
      JNG, MIFF, EPS, PS, AI, PDF, PSD, BMP, THM, CRW, CR2, MRW, NEF,
      PEF, ORF, DNG, and many other types of images. ExifTool also
      extracts information from the maker notes of many digital
      cameras by various manufacturers including Canon, Casio,
      FujiFilm, GE, HP, JVC/Victor, Kodak, Leaf, Minolta/Konica- [...]


  Protip: `exiftool -all= dst.jpg` to strip metadata from JPEGs.

  * **[pngcrush](http://pmt.sourceforge.net/pngcrush/): Optimizer for PNG (Portable Network Graphics) files**

      pngcrush is a commandline optimizer for PNG (Portable Network
      Graphics) files. Its main purpose is to reduce the size of the
      PNG IDAT datastream by trying various compression levels and PNG
      filter methods. It also can be used to remove unwanted ancillary
      chunks, or to add certain chunks including gAMA, tRNS, iCCP, and
      textual chunks.

  * **[qiv](http://spiegl.de/qiv/): Quick Image Viewer**

      qiv is a very small and pretty fast gdk2/Imlib2 image viewer.

  * **[rawstudio](http://rawstudio.org): Read, manipulate and convert digital camera raw images**

      Rawstudio is a highly specialized application for processing RAW
      images from digital cameras. It is not a fully featured image
      editing application. The RAW format is often recommended to get
      the best quality out of digital camera images. The format is
      specific to cameras and cannot be read by most image editing
      applications. Rawstudio makes it possible to read and manipulate
      RAW images, experiment with the controls to see how they affect
      the image, and finally export into JPEG, PNG or TIF format
      images from most digital cameras.

  * **[ufraw](http://ufraw.sourceforge.net): Raw image data retrieval tool for digital cameras**

      UFRaw is a tool for opening raw format images of digital
      cameras.

  * **[uniconvertor](https://sk1project.net/modules.php?name=Products&product=uniconvertor): Universal vector graphics translator**

      UniConvertor is a universal vector graphics translator. It uses
      sK1 engine to convert one format to another.

  * **[viewnior](http://siyanpanayotov.com/project/viewnior/): Elegant image viewer**

      Viewnior is an image viewer program. Created to be simple, fast
      and elegant. It's minimalistic interface provides more screen
      space for your images. Among its features are: ' Fullscreen &
      Slideshow ' Rotate, flip, save, delete images ' Animation
      support ' Browse only selected images ' Navigation window '
      Simple interface ' Configurable mouse actions

  * **[vips](https://libvips.github.io/libvips/): C/C++ library for processing large images**

      VIPS is an image processing library. It is good for very large
      images (even larger than the amount of RAM in your machine), and
      for working with color. This package should be installed if you
      want to use a program compiled against VIPS.

  * **[vtk](http://vtk.org/): The Visualization Toolkit - A high level 3D visualization library**

      VTK is an open-source software system for image processing, 3D
      graphics, volume rendering and visualization. VTK includes many
      advanced algorithms (e.g., surface reconstruction, implicit
      modeling, decimation) and rendering techniques (e.g., hardware-
      accelerated volume rendering, LOD control). NOTE: The version in
      this package has NOT been compiled with MPI support. Install the
      vtk-mpich package to get a version compiled with mpich. Install
      the vtk-openmpi package to get a version compiled with openmpi.


```
dnf install ImageMagick dcraw exif feh gimp hugin jasper jhead \
  luminance-hdr nip2 nomacs optipng perl-Image-ExifTool pngcrush qiv \
  rawstudio ufraw uniconvertor viewnior vips vtk
```

[back to index](#index)

***
## Multimedia - Music
  * **[csound](http://csound.github.io/): A sound synthesis language and library**

      Csound is a sound and music synthesis system, providing
      facilities for composition and performance over a wide range of
      platforms. It is not restricted to any style of music, having
      been used for many years in at least classical, pop, techno,
      ambient...

  * **[denemo](http://www.denemo.org/): Graphical music notation program**

      Denemo is a free software (GPL) music notation editor for
      GNU/Linux that lets you rapidly enter notation for typesetting
      via the LilyPond music engraver. You can compose, transcribe,
      arrange, listen to the music and much more.

  * **[frescobaldi](http://www.frescobaldi.org/): Edit LilyPond sheet music with ease!**

      Frescobaldi is a LilyPond sheet music editor. It aims to be
      powerful, yet lightweight and easy to use. It features: ' Enter
      LilyPond scores, build and preview them with a mouse-click '
      Point-and-click support: click on notes or error messages to
      jump to the correct position ' A powerful Score Wizard to
      quickly setup a musical score ' Editing tools to: o manipulate
      the rhythm o hyphenate lyrics o quickly enter or add
      articulations and other symbols to existing music o run the
      document through convert-ly to update it to a newer LilyPond
      version ' Context sensitive auto-complete, helping you to [...]

  * **[lilypond](http://www.lilypond.org): A typesetting system for music notation**

      LilyPond is an automated music engraving system. It formats
      music beautifully and automatically, and has a friendly syntax
      for its input files.

  * **[mup](http://www.arkkra.com): A music notation program that can also generate MIDI files**

      Mup is a program for printing music. It takes an input file
      containing ordinary (ASCII) text describing music, and produces
      PostScript output for printing the musical score described by
      the input.


```
dnf install csound denemo frescobaldi lilypond mup
```

[back to index](#index)

***
## Multimedia - Retro/Synthesizers
  * **[bristol](http://bristol.sourceforge.net): Synthesizer emulator**

      Bristol is an emulation package for a number of different
      'classic' synthesizers including additive and subtractive and a
      few organs. The application consists of the engine, which is
      called bristol, and its own GUI library called brighton that
      represents all the emulations.

  * **[dumb](http://dumb.sourceforge.net/): IT, XM, S3M and MOD player library**

      IT, XM, S3M and MOD player library. Mainly targeted for use with
      the allegro game programming library, but it can be used without
      allegro. Faithful to the original trackers, especially IT.

  * **[fluidsynth](http://www.fluidsynth.org/): Real-time software synthesizer**

      FluidSynth is a real-time software synthesizer based on the
      SoundFont 2 specifications. It is a "software synthesizer".
      FluidSynth can read MIDI events from the MIDI input device and
      render them to the audio device. It features real-time effect
      modulation using SoundFont 2.01 modulators, and a built-in
      command line shell. It can also play MIDI files (note:
      FluidSynth was previously called IIWU Synth).

  * **[timidity++](http://timidity.sourceforge.net): A software wavetable MIDI synthesizer**

      TiMidity++ is a MIDI format to wave table format converter and
      player. Install timidity++ if you'd like to play MIDI files and
      your sound card does not natively support wave table format.

  * **rpmfusion-nonfree [uade](http://zakalwe.fi/uade): Unix Amiga DeliTracker Emulator**

      UADE plays old Amiga tunes through UAE emulation and a cloned
      m68k-assembler Eagleplayer API. The player infrastructure of
      UADE is built on the ground work of the Eagleplayer and
      DeliTracker projects. UADE makes these external players reusable
      on certain UNIX and other platforms. UADE contains a free (as in
      freedom) implementation of Eagleplayer and DeliTracker API for
      UNIX variants.

  * **[vkeybd](http://www.alsa-project.org/~iwai/alsa.html): Virtual MIDI keyboard**

      This is a virtual keyboard for AWE, MIDI and ALSA drivers. It's
      a simple fake of a MIDI keyboard on X-windows system. Enjoy a
      music with your mouse and "computer" keyboard :-)

  * **[wildmidi](http://www.mindwerks.net/projects/wildmidi): Softsynth midi player**

      WildMidi is a software midi player which has a core softsynth
      library that can be used with other applications.

  * **[xmp](http://xmp.sourceforge.net/): A multi-format module player**

      This is the Extended Module Player, a portable module player
      that plays over 90 mainstream and obscure module formats,
      including Protracker MOD, Fasttracker II XM, Scream Tracker 3
      S3M and Impulse Tracker IT files.


```
dnf install bristol dumb fluidsynth timidity++ uade vkeybd wildmidi \
  xmp
```

[back to index](#index)

***
## Multimedia - Video
  * **rpmfusion-free-updates [ffmpeg](http://ffmpeg.org/): Digital VCR and streaming server**

      FFmpeg is a complete and free Internet live audio and video
      broadcasting solution for Linux/Unix. It also includes a digital
      VCR. It can encode in real time in many formats including MPEG1
      audio and video, MPEG4, h263, ac3, asf, avi, real, mjpeg, and
      flash.

  * **rpmfusion-free [ffmpegthumbnailer](http://code.google.com/p/ffmpegthumbnailer/): Lightweight video thumbnailer that can be used by file managers**

      This video thumbnailer can be used to create thumbnails for your
      video files.

  * **rpmfusion-free-updates [kdenlive](http://www.kdenlive.org): Non-linear video editor**

      Kdenlive is an intuitive and powerful multi-track video editor,
      including most recent video technologies.

  * **[mlt](http://www.mltframework.org/): Toolkit for broadcasters, video editors, media players, transcoders**

      MLT is an open source multimedia framework, designed and
      developed for television broadcasting. It provides a toolkit for
      broadcasters, video editors,media players, transcoders, web
      streamers and many more types of applications. The functionality
      of the system is provided via an assortment of ready to use
      tools, xml authoring components, and an extendible plug-in based
      API.

  * **rpmfusion-free [openshot](http://www.openshot.org): Create and edit videos and movies**

      OpenShot Video Editor is a free, open-source, non-linear video
      editor. It can create and edit videos and movies using many
      popular video, audio, image formats. Create videos for YouTube,
      Flickr, Vimeo, Metacafe, iPod, Xbox, and many more common
      formats! Features include: ' Multiple tracks (layers) '
      Compositing, image overlays, and watermarks ' Audio mixing and
      editing ' Support for image sequences (rotoscoping) ' Key-frame
      animation ' Video effects (chroma-key) ' Transitions (lumas and
      masks) ' Titles with integrated editor and templates ' 3D
      animation (titles and effects)

  * **[pitivi](http://www.pitivi.org/): Non-linear video editor**

      Pitivi is an application using the GStreamer multimedia
      framework to manipulate a large set of multimedia sources. At
      this level of development it can be compared to a classic video
      editing program.

  * **[tvtime](http://tvtime.sourceforge.net): A high quality TV viewer**

      tvtime is a high quality television application for use with
      video capture cards. tvtime processes the input from a capture
      card and displays it on a computer monitor or projector. Unlike
      other television applications, tvtime focuses on high visual
      quality making it ideal for videophiles.

  * **[vdr](http://www.tvdr.de/): Video Disk Recorder**

      VDR implements a complete digital set-top-box and video
      recorder. It can work with signals received from satellites
      (DVB-S) as well as cable (DVB-C) and terrestrial (DVB-T)
      signals. At least one DVB card is required to run VDR.

  * **[xawtv](http://linuxtv.org/wiki/index.php/Xawtv): TV applications for video4linux compliant devices**

      Xawtv is a simple xaw-based TV program which uses the bttv
      driver or video4linux. Xawtv contains various command-line
      utilities for grabbing images and .avi movies, for tuning in to
      TV stations, etc. Xawtv also includes a grabber driver for vic.


```
dnf install ffmpeg ffmpegthumbnailer kdenlive mlt openshot pitivi \
  tvtime vdr xawtv
```

[back to index](#index)

***
## Network Tools
  * **[bird](https://bird.network.cz/): BIRD Internet Routing Daemon**

      BIRD is a dynamic IP routing daemon supporting both, IPv4 and
      IPv6, Border Gateway Protocol (BGPv4), Routing Information
      Protocol (RIPv2, RIPng), Open Shortest Path First protocol
      (OSPFv2, OSPFv3), Babel Routing Protocol (Babel), Bidirectional
      Forwarding Detection (BFD), IPv6 router advertisements, static
      routes, inter-table protocol, command-line interface allowing
      on-line control and inspection of the status of the daemon, soft
      reconfiguration as well as a powerful language for route
      filtering.

  * **[dnsmasq](http://www.thekelleys.org.uk/dnsmasq/): A lightweight DHCP/caching DNS server**

      Dnsmasq is lightweight, easy to configure DNS forwarder and DHCP
      server. It is designed to provide DNS and, optionally, DHCP, to
      a small network. It can serve the names of local machines which
      are not in the global DNS. The DHCP server integrates with the
      DNS server and allows machines with DHCP-allocated addresses to
      appear in the DNS with names configured either in each host or
      in a central configuration file. Dnsmasq supports static and
      dynamic DHCP leases and BOOTP for network booting of diskless
      machines.

  * **[ferm](http://ferm.foo-projects.org/): For Easy Rule Making**

      Ferm is a tool to maintain complex firewalls, without having the
      trouble to rewrite the complex rules over and over again. Ferm
      allows the entire firewall rule set to be stored in a separate
      file, and to be loaded with one command. The firewall
      configuration resembles structured programming-like language,
      which can contain levels and lists.

  * **[flow-tools](http://code.google.com/p/flow-tools/): Tool set for working with NetFlow data**

      Flow-tools is library and a collection of programs used to
      collect, send, process, and generate reports from NetFlow data.
      The tools can be used together on a single server or distributed
      to multiple servers for large deployments. The flow-toools
      library provides an API for development of custom applications
      for NetFlow export versions 1,5,6 and the 14 currently defined
      version 8 subversions. A Perl and Python interface have been
      contributed and are included in the distribution.

  * **[ipcalc](https://gitlab.com/ipcalc/ipcalc): IP network address calculator**

      ipcalc provides a simple way to calculate IP information for a
      host or network. Depending on the options specified, it may be
      used to provide IP network information in human readable format,
      in a format suitable for parsing in scripts, generate random
      private addresses, resolve an IP address, or check the validity
      of an address.

  * **[iptstate](http://www.phildev.net/iptstate/): A top-like display of IP Tables state table entries**

      IP Tables State (iptstate) was originally written to implement
      the "state top" feature of IP Filter in IP Tables. "State top"
      displays the states held by your stateful firewall in a top-like
      manner. Since IP Tables doesn't have a built in way to easily
      display this information even once, an option was added to just
      have it display the state table once. Features include: - Top-
      like realtime state table information - Sorting by any field -
      Reversible sorting - Single display of state table -
      Customizable refresh rate - Display filtering - Color-coding -
      Open Source - much more...

  * **[ipvsadm](https://kernel.org/pub/linux/utils/kernel/ipvsadm/): Utility to administer the Linux Virtual Server**

      ipvsadm is used to setup, maintain, and inspect the virtual
      server table in the Linux kernel. The Linux Virtual Server can
      be used to build scalable network services based on a cluster of
      two or more nodes. The active node of the cluster redirects
      service requests to a collection of server hosts that will
      actually perform the services. Supported Features include: - two
      transport layer (layer-4) protocols (TCP and UDP) - three
      packet-forwarding methods (NAT, tunneling, and direct routing) -
      eight load balancing algorithms (round robin, weighted round
      robin, least-connection, weighted least-connection, [...]

  * **[keepalived](http://www.keepalived.org/): High Availability monitor built upon LVS, VRRP and service pollers**

      Keepalived provides simple and robust facilities for load
      balancing and high availability to Linux system and Linux based
      infrastructures. The load balancing framework relies on well-
      known and widely used Linux Virtual Server (IPVS) kernel module
      providing Layer4 load balancing. Keepalived implements a set of
      checkers to dynamically and adaptively maintain and manage load-
      balanced server pool according their health. High availability
      is achieved by VRRP protocol. VRRP is a fundamental brick for
      router failover. In addition, keepalived implements a set of
      hooks to the VRRP finite state machine providing low-level [...]

  * **[knot](https://www.knot-dns.cz): High-performance authoritative DNS server**

      Knot DNS is a high-performance authoritative DNS server
      implementation.

  * **[mbrowse](http://sourceforge.net/projects/mbrowse/): GUI SNMP MIB browser**

      Mbrowse is a graphical SNMP MIB browser based on GTK+ and net-
      snmp.

  * **[netplug](http://www.red-bean.com/~bos/): Daemon that responds to network cables being plugged in and out**

      Netplug is a daemon that manages network interfaces in response
      to link-level events such as cables being plugged in and out.
      When a cable is plugged into an interface, the netplug daemon
      brings that interface up. When the cable is unplugged, the
      daemon brings that interface back down. This is extremely useful
      for systems such as laptops, which are constantly being
      unplugged from one network and plugged into another, and for
      moving systems in a machine room from one switch to another
      without a need for manual intervention.

  * **[netstat-nat](http://www.tweegy.nl/projects/netstat-nat/): A tool that displays NAT connections**

      Netstat-nat is a small program written in C. It displays NAT
      connections, managed by netfilter/iptables which comes with the
      &gt; 2.4.x linux kernels. The program reads its information from
      '/proc/net/nf_conntrack', which is the temporary conntrack-
      storage of netfilter.

  * **[nfdump](https://github.com/phaag/nfdump): NetFlow collecting and processing tools**

      Nfdump is a set of tools to collect and process NetFlow data.
      It's fast and has a powerful filter pcap like syntax. It
      supports NetFlow versions v1, v5, v7, v9 and IPFIX as well as a
      limited set of sflow. It includes support for CISCO ASA (NSEL)
      and CISCO NAT (NEL) devices which export event logging records
      as v9 flows. Nfdump is fully IPv6 compatible.

  * **[ngrep](https://github.com/jpr5/ngrep): Network layer grep tool**

      ngrep strives to provide most of GNU grep's common features,
      applying them to the network layer. ngrep is a pcap-aware tool
      that will allow you to specify extended regular or hexadecimal
      expressions to match against data payloads of packets. It
      currently recognizes TCP, UDP, ICMP, IGMP and Raw protocols
      across Ethernet, PPP, SLIP, FDDI, Token Ring, 802.11 and null
      interfaces, and understands bpf filter logic in the same fashion
      as more common packet sniffing tools, such as tcpdump and snoop.

  * **[pacemaker](http://www.clusterlabs.org): Scalable High-Availability cluster resource manager**

      Pacemaker is an advanced, scalable High-Availability cluster
      resource manager. It supports more than 16 node clusters with
      significant capabilities for managing resources and
      dependencies. It will run scripts at initialization, when
      machines go up or down, when related resources fail and can be
      configured to periodically check resource health. Available
      rpmbuild rebuild options: --with(out) : coverage doc hardening
      pre_release profiling

  * **[rinetd](http://www.boutell.com/rinetd): TCP redirection server**

      rinetd is a daemon which redirects TCP connections from one IP
      address and port to another IP address and port. This daemon is
      often used to access services behind a firewall.

  * **✓ [sipcalc](http://www.routemeister.net/projects/sipcalc): An "advanced" console based ip subnet calculator**

      Sipcalc is an "advanced" console based ip subnet calculator.

  * **[ucarp](http://www.ucarp.org/): Common Address Redundancy Protocol (CARP) for Unix**

      UCARP allows a couple of hosts to share common virtual IP
      addresses in order to provide automatic failover. It is a
      portable userland implementation of the secure and patent-free
      Common Address Redundancy Protocol (CARP, OpenBSD's alternative
      to the patents-bloated VRRP). Strong points of the CARP protocol
      are: very low overhead, cryptographically signed messages,
      interoperability between different operating systems and no need
      for any dedicated extra network link between redundant hosts.

  * **[unbound](https://www.unbound.net/): Validating, recursive, and caching DNS(SEC) resolver**

      Unbound is a validating, recursive, and caching DNS(SEC)
      resolver. The C implementation of Unbound is developed and
      maintained by NLnet Labs. It is based on ideas and algorithms
      taken from a java prototype developed by Verisign labs, Nominet,
      Kirei and ep.net. Unbound is designed as a set of modular
      components, so that also DNSSEC (secure DNS) validation and
      stub-resolvers (that do not run as a server, but are linked into
      an application) are easily possible.

  * **[usbip](https://www.kernel.org): USB/IP user-space**

      USB/IP allows you to share USB devices over a network. With
      USB/IP, you can plug a USB device into one computer and use it
      on a different computer on the network. This package contains
      the user-space tools for USB/IP, both for servers and clients

  * **[usbredir](http://spice-space.org/page/UsbRedir): USB network redirection protocol libraries**

      The usbredir libraries allow USB devices to be used on remote
      and/or virtual hosts over TCP. The following libraries are
      provided: usbredirparser: A library containing the parser for
      the usbredir protocol usbredirhost: A library implementing the
      USB host side of a usbredir connection. All that an application
      wishing to implement a USB host needs to do is: ' Provide a
      libusb device handle for the device ' Provide write and read
      callbacks for the actual transport of usbredir data ' Monitor
      for usbredir and libusb read/write events and call their
      handlers

  * **[vconfig](http://www.candelatech.com/~greear/vlan.html): Linux 802.1q VLAN configuration utility**

      The vconfig program configures and adjusts 802.1q VLAN
      parameters. This tool is deprecated in favor of "ip link"
      command.

  * **[vnstat](http://humdi.net/vnstat/): Console-based network traffic monitor**

      vnStat is a console-based network traffic monitor that keeps a
      log of daily network traffic for the selected interface(s).
      vnStat isn't a packet sniffer. The traffic information is
      analyzed from the /proc file-system, so vnStat can be used
      without root permissions. See the web-page for few
      'screenshots'.

  * **[vtun](http://vtun.sourceforge.net): Virtual tunnel over TCP/IP networks**

      VTun provides a method for creating Virtual Tunnels over TCP/IP
      networks and allows one to shape, compress, and encrypt traffic
      in those tunnels. Supported types of tunnels are: PPP, IP,
      Ethernet and most other serial protocols and programs. VTun is
      easily and highly configurable: it can be used for various
      network tasks like VPN, Mobile IP, Shaped Internet access, IP
      address saving, etc. It is completely a user space
      implementation and does not require modification to any kernel
      parts.


```
dnf install bird dnsmasq ferm flow-tools ipcalc iptstate ipvsadm \
  keepalived knot mbrowse netplug netstat-nat nfdump ngrep pacemaker \
  rinetd sipcalc ucarp unbound usbip usbredir vconfig vnstat vtun
```

[back to index](#index)

***
## Network Tools - Diagnostics
  * **[httping](http://www.vanheusden.com/httping/): Ping alike tool for http requests**

      Httping is like 'ping' but for http-requests. Give it an url,
      and it'll show you how long it takes to connect, send a request
      and retrieve the reply (only the headers). Be aware that the
      transmission across the network also takes time.

  * **[iperf](http://sourceforge.net/projects/iperf2): Measurement tool for TCP/UDP bandwidth performance**

      Iperf is a tool to measure maximum TCP bandwidth, allowing the
      tuning of various parameters and UDP characteristics. Iperf
      reports bandwidth, delay jitter, datagram loss.

  * **[jwhois](http://www.gnu.org/software/jwhois/): Internet whois/nicname client**

      A whois client that accepts both traditional and finger-style
      queries.

  * **✓ [mtr](https://www.bitwizard.nl/mtr/): Network diagnostic tool combining 'traceroute' and 'ping'**

      MTR combines the functionality of the 'traceroute' and 'ping'
      programs in a single network diagnostic tool. When MTR is
      started, it investigates the network connection between the host
      MTR runs on and the user-specified destination host. Afterwards
      it determines the address of each network hop between the
      machines and sends a sequence of ICMP echo requests to each one
      to determine the quality of the link to each machine. While
      doing this, it prints running statistics about each machine. MTR
      provides two user interfaces: an ncurses interface, useful for
      the command line, e.g. for SSH sessions; and a GTK+ [...]

  * **[nbtscan](http://www.inetcat.net/software/nbtscan.html): Tool to gather NetBIOS info from Windows networks**

      NBTscan is a program for scanning IP networks for NetBIOS name
      information. It sends a NetBIOS status query to each address in
      supplied range and lists received information in human readable
      form.

  * **[ndisc6](http://www.remlab.net/ndisc6/): IPv6 diagnostic tools**

      This package gathers a few diagnostic tools for IPv6 networks: -
      ndisc6, which performs ICMPv6 Neighbor Discovery in user-land, -
      rdisc6, which performs ICMPv6 Router Discovery in user-land, -
      rltraceroute6, yet another IPv6 implementation of traceroute, -
      tcptraceroute6, a TCP/IPv6-based traceroute implementation, -
      tracert6, a ICMPv6 Echo Request based traceroute, - tcpspray6, a
      TCP/IP Discard/Echo bandwidth meter.

  * **✓ [net-tools](http://sourceforge.net/projects/net-tools/): Basic networking tools**

      The net-tools package contains basic networking tools, including
      ifconfig, netstat, route, and others. Most of them are obsolete.
      For replacement check iproute package.

  * **✓ [nmap](http://nmap.org/): Network exploration tool and security scanner**

      Nmap is a utility for network exploration or security auditing.
      It supports ping scanning (determine which hosts are up), many
      port scanning techniques (determine what services the hosts are
      offering), and TCP/IP fingerprinting (remote host operating
      system identification). Nmap also offers flexible target and
      port specification, decoy scanning, determination of TCP
      sequence predictability characteristics, reverse-identd
      scanning, and more. In addition to the classic command-line nmap
      executable, the Nmap suite includes a flexible data transfer,
      redirection, and debugging tool (netcat utility ncat), a [...]

  * **✓ [nmap-ncat](http://nmap.org/): Nmap's Netcat replacement**

      Ncat is a feature packed networking utility which will read and
      write data across a network from the command line. It uses both
      TCP and UDP for communication and is designed to be a reliable
      back-end tool to instantly provide network connectivity to other
      applications and users. Ncat will not only work with IPv4 and
      IPv6 but provides the user with a virtually limitless number of
      potential uses.

  * **[packETH](http://sourceforge.net/projects/packeth/): A GUI packet generator tool**

      packETH is a Linux GUI tool that is able to send any packet or
      sequence of packets on the Ethernet. It uses the RAW socket
      option, so it doesn't care about ip, routing, etc. It is
      designed to have all the options available, with all the correct
      and incorrect values (incorrect means, that user can send wrong
      parameters like: incorrect checksum, wrong header length, etc.).

  * **[smokeping](https://oss.oetiker.ch/smokeping/): Latency Logging and Graphing System**

      SmokePing is a latency logging and graphing system. It consists
      of a daemon process which organizes the latency measurements and
      a CGI which presents the graphs.

  * **✓ [socat](http://www.dest-unreach.org/socat/): Bidirectional data relay between two data channels ('netcat++')**

      Socat is a relay for bidirectional data transfer between two
      independent data channels. Each of these data channels may be a
      file, pipe, device (serial line etc. or a pseudo terminal), a
      socket (UNIX, IP4, IP6 - raw, UDP, TCP), an SSL socket, proxy
      CONNECT connection, a file descriptor (stdin etc.), the GNU line
      editor (readline), a program, or a combination of two of these.

  * **[sslscan](http://www.dinotools.org/tag/sslscan.html): A security assessment tool for SSL**

      SSLScan queries SSL services, such as HTTPS, in order to
      determine the ciphers that are supported. SSLScan is designed to
      be easy, lean and fast. The output includes preferred ciphers of
      the SSL service, the certificate and is in Text and XML formats.

  * **✓ [traceroute](http://traceroute.sourceforge.net): Traces the route taken by packets over an IPv4/IPv6 network**

      The traceroute utility displays the route used by IP packets on
      their way to a specified network (or Internet) host. Traceroute
      displays the IP number and host name (if possible) of the
      machines along the route taken by the packets. Traceroute is
      used as a network debugging tool. If you're having network
      connectivity problems, traceroute will show you where the
      trouble is coming from along the route. Install traceroute if
      you need a tool for diagnosing network connectivity problems.

  * **[ttcp](ftp://ftp.sgi.com/sgi/src/ttcp/): A tool for testing TCP connections**

      ttcp is a tool for testing the throughput of TCP connections.
      Unlike other tools which might be used for this purpose (such as
      FTP clients), ttcp does not read or write data from or to a disk
      while operating, which helps ensure more accurate results.


```
dnf install httping iperf jwhois mtr nbtscan ndisc6 net-tools nmap \
  nmap-ncat packETH smokeping socat sslscan traceroute ttcp
```

[back to index](#index)

***
## Network Tools - Monitoring
  * **[bluez-hcidump](http://www.bluez.org/): Bluetooth HCI protocol analyser**

      Protocol analyser for Bluetooth traffic. The BLUETOOTH
      trademarks are owned by Bluetooth SIG, Inc., U.S.A.

  * **[bmon](http://github.com/tgraf/bmon): Bandwidth monitor and rate estimator**

      bmon is a monitoring and debugging tool to capture networking
      related statistics and prepare them visually in a human friendly
      way. It features various output methods including an interactive
      curses user interface and a programmable text output for
      scripting.

  * **[darkstat](http://unix4lyfe.org/darkstat/): Network traffic analyzer**

      darkstat is a network traffic analyzer. It's basically a packet
      sniffer which runs as a background process on a cable/DSL router
      and gathers all sorts of useless but interesting statistics.

  * **[driftnet](http://www.ex-parrot.com/~chris/driftnet/): Network image sniffer**

      Driftnet is a program which listens to network traffic and picks
      out images from TCP streams it observes. Fun to run on a host
      which sees lots of web traffic.

  * **[etherape](http://etherape.sourceforge.net/): Graphical network monitor for Unix**

      EtherApe is a graphical network monitor modeled after etherman.

  * **✓ [httpie](http://httpie.org/): A Curl-like tool for humans**

      HTTPie is a CLI HTTP utility built out of frustration with
      existing tools. The goal is to make CLI interaction with HTTP-
      based services as human-friendly as possible. HTTPie does so by
      providing an http command that allows for issuing arbitrary HTTP
      requests using a simple and natural syntax and displaying
      colorized responses.

  * **[iptraf-ng](https://github.com/iptraf-ng/iptraf-ng/): A console-based network monitoring utility**

      IPTraf-ng is a console-based network monitoring utility. IPTraf
      gathers data like TCP connection packet and byte counts,
      interface statistics and activity indicators, TCP/UDP traffic
      breakdowns, and LAN station packet and byte counts. IPTraf-ng
      features include an IP traffic monitor which shows TCP flag
      information, packet and byte counts, ICMP details, OSPF packet
      types, and oversized IP packet warnings; interface statistics
      showing IP, TCP, UDP, ICMP, non-IP and other IP packet counts,
      IP checksum errors, interface activity and packet size counts; a
      TCP and UDP service monitor showing counts of incoming and [...]

  * **✓ [tcpdump](http://www.tcpdump.org): A network traffic monitoring tool**

      Tcpdump is a command-line tool for monitoring network traffic.
      Tcpdump can capture and display the packet headers on a
      particular network interface or on all interfaces. Tcpdump can
      display all of the packet headers, or just the ones that match
      particular criteria. Install tcpdump if you need a program to
      monitor network traffic.

  * **[tcpreplay](http://tcpreplay.appneta.com/): Replay captured network traffic**

      Tcpreplay is a tool to replay captured network traffic.
      Currently, tcpreplay supports pcap (tcpdump) and snoop capture
      formats. Also included, is tcpprep a tool to pre-process capture
      files to allow increased performance under certain conditions as
      well as capinfo which provides basic information about capture
      files.

  * **[wireshark](http://www.wireshark.org/): Network traffic analyzer**

      Wireshark allows you to examine protocol data stored in files or
      as it is captured from wired or wireless (WiFi or Bluetooth)
      networks, USB devices, and many other sources. It supports
      dozens of protocol capture file formats and understands more
      than a thousand protocols. It has many powerful features
      including a rich display filter language and the ability to
      reassemble multiple protocol packets in order to, for example,
      view a complete TCP stream, save the contents of a file which
      was transferred over HTTP or CIFS, or play back an RTP audio
      stream.


```
dnf install bluez-hcidump bmon darkstat driftnet etherape httpie \
  iptraf-ng tcpdump tcpreplay wireshark
```

[back to index](#index)

***
## Network Tools - Remote Desktop/Access
  * **✓ [freerdp](http://www.freerdp.com/): Free implementation of the Remote Desktop Protocol (RDP)**

      The xfreerdp & wlfreerdp Remote Desktop Protocol (RDP) clients
      from the FreeRDP project. xfreerdp & wlfreerdp can connect to
      RDP servers such as Microsoft Windows machines, xrdp and
      VirtualBox.

  * **✓ [mosh](https://mosh.mit.edu/): Mobile shell that supports roaming and intelligent local echo**

      Mosh is a remote terminal application that supports: -
      intermittent network connectivity, - roaming to different IP
      address without dropping the connection, and - intelligent local
      echo and line editing to reduce the effects of "network lag" on
      high-latency connections.

  * **[pdsh](https://pdsh.googlecode.com/): Parallel remote shell program**

      Pdsh is a multithreaded remote shell client which executes
      commands on multiple remote hosts in parallel. Pdsh can use
      several different remote shell services, including standard
      "rsh", Kerberos IV, and ssh.

  * **[pssh](https://github.com/lilydjwg/pssh): Parallel SSH tools**

      This package provides various parallel tools based on ssh and
      scp. Parallell version includes: o ssh : pssh o scp : pscp o
      nuke : pnuke o rsync : prsync o slurp : pslurp

  * **[remmina](http://remmina.org): Remote Desktop Client**

      Remmina is a remote desktop client written in GTK+, aiming to be
      useful for system administrators and travelers, who need to work
      with lots of remote computers in front of either large monitors
      or tiny net-books. Remmina supports multiple network protocols
      in an integrated and consistent user interface. Currently RDP,
      VNC, XDMCP and SSH are supported. Please don't forget to install
      the plugins for the protocols you want to use.

  * **✓ [telnet](http://web.archive.org/web/20070819111735/www.hcs.harvard.edu/~dholland/computers/old-netkit.html): The client program for the Telnet remote login protocol**

      Telnet is a popular protocol for logging into remote systems
      over the Internet. The package provides a command line Telnet
      client

  * **✓ [tigervnc](http://www.tigervnc.com): A TigerVNC remote display system**

      Virtual Network Computing (VNC) is a remote display system which
      allows you to view a computing 'desktop' environment not only on
      the machine where it is running, but from anywhere on the
      Internet and from a wide variety of machine architectures. This
      package contains a client which will allow you to connect to
      other desktops running a VNC server.

  * **✓ [virt-viewer](http://virt-manager.org/): Virtual Machine Viewer**

      Virtual Machine Viewer provides a graphical console client for
      connecting to virtual machines. It uses the GTK-VNC or SPICE-GTK
      widgets to provide the display, and libvirt for looking up
      VNC/SPICE server details.

  * **[xpra](https://www.xpra.org/): Remote display server for applications and desktops**

      Xpra is "screen for X": it allows you to run X programs, usually
      on a remote host, direct their display to your local machine,
      and then to disconnect from these programs and reconnect from
      the same or another machine, without losing any state. It gives
      you remote access to individual applications. Xpra is "rootless"
      or "seamless": programs you run under it show up on your desktop
      as regular programs, managed by your regular window manager.
      Sessions can be accessed over SSH, or password protected over
      plain TCP sockets. Xpra is usable over reasonably slow links and
      does its best to adapt to changing network bandwidth [...]


```
dnf install freerdp mosh pdsh pssh remmina telnet tigervnc virt-viewer \
  xpra
```

[back to index](#index)

***
## Network Tools - Tunneling/VPNs/Proxies
  * **[autossh](https://www.harding.motd.ca/autossh/): Utility to autorestart SSH tunnels**

      autossh is a utility to start and monitor an ssh tunnel. If the
      tunnel dies or stops passing traffic, autossh will automatically
      restart it.

  * **[corkscrew](http://freshmeat.sourceforge.net/projects/corkscrew): Tool for tunneling SSH through HTTP proxies**

      Corkscrew is a tool for tunneling SSH through HTTP proxies. It
      has been tested with the following HTTP proxies : ' Gauntlet '
      CacheFlow ' JunkBuster ' Apache mod_proxy

  * **[haproxy](http://www.haproxy.org/): HAProxy reverse proxy for high availability environments**

      HAProxy is a TCP/HTTP reverse proxy which is particularly suited
      for high availability environments. Indeed, it can: - route HTTP
      requests depending on statically assigned cookies - spread load
      among several servers while assuring server persistence through
      the use of HTTP cookies - switch to backup servers in the event
      a main one fails - accept connections to special ports dedicated
      to service monitoring - stop accepting connections without
      breaking existing ones - add, modify, and delete HTTP headers in
      both directions - block requests matching particular patterns -
      report detailed status to authenticated users from a URI [...]

  * **[iodine](http://code.kryo.se/iodine/): Solution to tunnel IPv4 data through a DNS server**

      iodine lets you tunnel IPv4 data through a DNS server. This can
      be usable in different situations where internet access is
      firewalled, but DNS queries are allowed. It runs on Linux, Mac
      OS X, FreeBSD, NetBSD, OpenBSD and Windows and needs a TUN/TAP
      device. The bandwidth is asymmetrical with limited upstream and
      up to 1 Mbit/s downstream. This is meta-package to install both
      client and server. It also contain three documantation files:
      CHANGELOG, README, TODO.

  * **✓ [openvpn](https://community.openvpn.net/): A full-featured SSL VPN solution**

      OpenVPN is a robust and highly flexible tunneling application
      that uses all of the encryption, authentication, and
      certification features of the OpenSSL library to securely tunnel
      IP networks over a single UDP or TCP port. It can use the Marcus
      Franz Xaver Johannes Oberhumers LZO library for compression.

  * **[squid](http://www.squid-cache.org): The Squid proxy caching server**

      Squid is a high-performance proxy caching server for Web
      clients, supporting FTP, gopher, and HTTP data objects. Unlike
      traditional caching software, Squid handles all requests in a
      single, non-blocking, I/O-driven process. Squid keeps meta data
      and especially hot objects cached in RAM, caches DNS lookups,
      supports non-blocking DNS lookups, and implements negative
      caching of failed requests. Squid consists of a main server
      program squid, a Domain Name System lookup program (dnsserver),
      a program for retrieving FTP data (ftpget), and some management
      and client tools.

  * **[sshuttle](https://github.com/sshuttle/sshuttle): Transparent Proxy VPN**

      Transparent proxy server that works as a poor man's VPN.
      Forwards over ssh. Doesn't require admin. Works with Linux and
      MacOS. Supports DNS tunneling.

  * **[sslh](https://github.com/yrutschle/sslh): Applicative protocol(SSL/SSH) multiplexer**

      sslh accepts connections on specified ports, and forwards them
      further based on tests performed on the first data packet sent
      by the remote client. Probes for HTTP, SSL, SSH, OpenVPN, tinc,
      XMPP are implemented, and any other protocol that can be tested
      using a regular expression, can be recognized. A typical use
      case is to allow serving several services on port 443 (e.g. to
      connect to ssh from inside a corporate firewall, which almost
      never block port 443) while still serving HTTPS on that port.
      Hence sslh acts as a protocol multiplexer, or a switchboard. Its
      name comes from its original function to serve SSH and [...]

  * **[vpnc](http://www.unix-ag.uni-kl.de/~massar/vpnc/): IPSec VPN client compatible with Cisco equipment**

      A VPN client compatible with Cisco's EasyVPN equipment. Supports
      IPSec (ESP) with Mode Configuration and Xauth. Supports only
      shared-secret IPSec authentication, 3DES, MD5, and IP tunneling.


```
dnf install autossh corkscrew haproxy iodine openvpn squid sshuttle \
  sslh vpnc
```

[back to index](#index)

***
## Note Taking
  * **[Zim](http://zim-wiki.org/): Desktop wiki & notekeeper**

      Zim is a WYSIWYG text editor written in Python which aims to
      bring the concept of a wiki to your desktop. Every page is saved
      as a text file with wiki markup. Pages can contain links to
      other pages, and are saved automatically. Creating a new page is
      as easy as linking to a non-existing page. Pages are ordered in
      a hierarchical structure that gives it the look and feel of an
      outliner. This tool is intended to keep track of TODO lists or
      to serve as a personal scratch book.

  * **[rednotebook](http://rednotebook.sourceforge.net): Daily journal with calendar, templates and keyword searching**

      RedNotebook is a modern desktop journal. It lets you format, tag
      and search your entries. You can also add pictures, links and
      customizable templates, spell check your notes, and export to
      plain text, HTML, Latex or PDF.

  * **[vym](http://www.insilmaril.de/vym/): View your mind**

      VYM (View Your Mind) is a tool to generate and manipulate maps
      which show your thoughts. Such maps can help you to improve your
      creativity and effectivity. You can use them for time
      management, to organize tasks, to get an overview over complex
      contexts.


```
dnf install Zim rednotebook vym
```

[back to index](#index)

***
## Optical Media Tools
  * **[abcde](https://abcde.einval.com/): A Better CD Encoder**

      abcde is a front end command line utility (actually, a shell
      script) that grabs audio tracks off a CD, encodes them to
      various formats, and tags them, all in one go.

  * **[brasero](https://wiki.gnome.org/Apps/Brasero): Gnome CD/DVD burning application**

      Simple and easy to use CD/DVD burning application for the Gnome
      desktop.

  * **[cdrdao](http://cdrdao.sourceforge.net/): Writes audio CD-Rs in disk-at-once (DAO) mode**

      Cdrdao records audio CD-Rs in disk-at-once (DAO) mode, based on
      a textual description of the CD contents. Recording in DAO mode
      writes the complete disc (lead-in, one or more tracks, and lead-
      out) in a single step. DAO allows full control over the length
      and the contents of pre-gaps, the pause areas between tracks.

  * **[cdw](http://cdw.sourceforge.net/): Front-end for tools used for burning data CD/DVD**

      cdw is a ncurses based front-end for some command-line tools
      used for burning data CD and DVD discs and for related tasks.
      The tools are: cdrecord/wodim, mkisofs/genisoimage, growisofs,
      dvd+rw-mediainfo, dvd+rw-format, xorriso. cdw is able to rip
      tracks from your audio CD to raw audio files. Limited support
      for copying content of data CD and DVD discs to image files is
      also provided. cdw can verify correctness of writing ISO9660
      image to CD or DVD disc using md5sum or some of programs that
      verifies SHA hashes.

  * **✓ [k3b](http://www.k3b.org/): CD/DVD/Blu-ray burning application**

      K3b provides a comfortable user interface to perform most CD/DVD
      burning tasks. While the experienced user can take influence in
      all steps of the burning process the beginner may find comfort
      in the automatic settings and the reasonable k3b defaults which
      allow a quick start.

  * **[wodim](http://cdrkit.org/): A command line CD/DVD recording program**

      Wodim is an application for creating audio and data CDs. Wodim
      works with many different brands of CD recorders, fully supports
      multi-sessions and provides human-readable error messages.


```
dnf install abcde brasero cdrdao cdw k3b wodim
```

[back to index](#index)

***
## Parallel and Distributed Computing
  * **[gearmand](http://www.gearman.org): A distributed job system**

      Gearman provides a generic framework to farm out work to other
      machines or dispatch function calls to machines that are better
      suited to do the work. It allows you to do work in parallel, to
      load balance processing, and to call functions between
      languages. It can be used in a variety of applications, from
      high-availability web sites to the transport for database
      replication. In other words, it is the nervous system for how
      distributed processing communicates.

  * **[icecream](https://github.com/icecc/icecream): Distributed compiler**

      Icecream is a distributed compile system. It allows parallel
      compiling by distributing the compile jobs to several nodes of a
      compile network running the icecc daemon. The icecc scheduler
      routes the jobs and provides status and statistics information
      to the icecc monitor. Each compile node can accept one or more
      compile jobs depending on the number of processors and the
      settings of the daemon. Link jobs and other jobs which cannot be
      distributed are executed locally on the node where the
      compilation is started.


```
dnf install gearmand icecream
```

[back to index](#index)

***
## Productivity
  * **[calcurse](https://calcurse.org): Text-based personal organizer**

      Calcurse is a text-based calendar and scheduling application. It
      helps keep track of events, appointments, and everyday tasks. A
      configurable notification system reminds the user of upcoming
      deadlines, and the curses based interface can be customized to
      suit user needs.

  * **[devtodo](http://swapoff.org/DevTodo): Manage a prioritised list of todo items organized by directory**

      Todo is a program to display and manage a hierarchical,
      prioritised list of outstanding work, or just reminders. The
      program itself is assisted by a few shell scripts that override
      default builtins. Specifically, cd, pushd and popd are
      overridden so that when using one of these commands to enter a
      directory, the todo will display any outstanding items in that
      directory. For much more complete information please refer to
      the man page (devtodo(1)).

  * **[glabels](http://www.glabels.org): A program for creating labels and business cards for GNOME**

      gLabels is a lightweight program for creating labels and
      business cards for the GNOME desktop environment. It is designed
      to work with various laser/ink-jet peel-off label and business
      card sheets that you'll find at most office supply stores.

  * **[gnucash](http://gnucash.org/): Finance management application**

      GnuCash is a personal finance manager. A check-book like
      register GUI allows you to enter and track bank accounts,
      stocks, income and even currency trades. The interface is
      designed to be simple and easy to use, but is backed with
      double-entry accounting principles to ensure balanced books.

  * **[gnumeric](http://projects.gnome.org/gnumeric/): Spreadsheet program for GNOME**

      Gnumeric is a spreadsheet program for the GNOME GUI desktop
      environment.

  * **[grisbi](http://www.grisbi.org): Personal finances manager**

      Grisbi is a very functional personal financial management
      program with a lot of features: checking, cash and liabilities
      accounts, several accounts with automatic contra entries,
      several currencies, including euro, arbitrary currency for every
      operation, money interchange fees, switch to euro account per
      account, description of the transactions with third parties,
      categories, sub-categories, financial year, notes, breakdown,
      transfers between accounts, even for accounts of different
      currencies, bank reconciliation, scheduled transactions,
      automatic recall of last transaction for every third [...]

  * **[link-grammar](http://abisource.com/projects/link-grammar/): A full-service natural language dependency parser**

      A full-service natural language dependency parser for English
      and Russian, with prototypes for other assorted languages.

  * **[osmo](http://clayo.org/osmo/): Personal organizer**

      Osmo is a handy personal organizer which includes calendar,
      tasks manager and address book modules. It was designed to be a
      small, easy to use and good looking PIM tool to help to manage
      personal information. In current state the organizer is quite
      convenient in use - for example, user can perform nearly all
      operations using keyboard. Also, a lot of parameters are
      configurable to meet user preferences.

  * **[ots](http://libots.sourceforge.net/): A text summarizer**

      The open text summarizer is an open source tool for summarizing
      texts. The program reads a text and decides which sentences are
      important and which are not.

  * **[radicale](https://radicale.org): A simple CalDAV (calendar) and CardDAV (contact) server**

      The Radicale Project is a CalDAV (calendar) and CardDAV
      (contact) server. It aims to be a light solution, easy to use,
      easy to install, easy to configure. As a consequence, it
      requires few software dependencies and is pre-configured to work
      out-of-the-box. The Radicale Project runs on most of the UNIX-
      like platforms (Linux, BSD, MacOS X) and Windows. It is known to
      work with Evolution, Lightning, iPhone and Android clients. It
      is free and open-source software, released under GPL version 3.

  * **[skrooge](http://skrooge.org): Personal finances manager**

      skrooge is a personal finances manager, aiming at being simple
      and intuitive. It allows you to keep track of your expenses and
      incomes, categorize them, and build reports of them.

  * **[task](https://taskwarrior.org): Taskwarrior - a command-line TODO list manager**

      Taskwarrior is a command-line TODO list manager. It is flexible,
      fast, efficient, unobtrusive, does its job then gets out of your
      way. Taskwarrior scales to fit your workflow. Use it as a simple
      app that captures tasks, shows you the list, and removes tasks
      from that list. Leverage its capabilities though, and it becomes
      a sophisticated data query tool that can help you stay
      organized, and get through your work.

  * **[taskcoach](http://www.taskcoach.org/): Your friendly task manager**

      Task Coach is a simple open source todo manager to manage
      personal tasks and todo lists. It grew out of a frustration that
      well-known task managers, such as those provided with Outlook or
      Lotus Notes, do not provide facilities for composite tasks.
      Often, tasks and other things todo consist of several
      activities. Task Coach is designed to deal with composite tasks.

  * **[tpp](http://www.ngolde.de/tpp.html): A ncurses-based presentation tool**

      tpp stands for text presentation program and is a ncurses-based
      presentation tool. The presentation can be written with your
      favorite editor in a simple description format and then shown on
      any text terminal that is supported by ncurses - ranging from an
      old VT100 to the Linux framebuffer to an xterm.


```
dnf install calcurse devtodo glabels gnucash gnumeric grisbi \
  link-grammar osmo ots radicale skrooge task taskcoach tpp
```

[back to index](#index)

***
## Scientific
  * **[LabPlot](http://labplot.sourceforge.net/): Data Analysis and Visualization**

      LabPlot is for scientific 2D and 3D data and function plotting.
      The various display and analysis functions are explained in the
      handbook (KDE help center). LabPlot also provides a component
      for easily viewing the project files in Konqueror.

  * **[avogadro](http://avogadro.openmolecules.net/): An advanced molecular editor for chemical purposes**

      An advanced molecular editor designed for cross-platform use in
      computational chemistry, molecular modeling, bioinformatics,
      materials science, and related areas, which offers flexible
      rendering and a powerful plugin architecture.

  * **[engauge-digitizer](http://markummitchell.github.io/engauge-digitizer/): Convert graphs or map files into numbers**

      The Engauge Digitizer tool accepts image files (like PNG, JPEG
      and TIFF) containing graphs, and recovers the data points from
      those graphs. The resulting data points are usually used as
      input to other software applications. Conceptually, Engauge
      Digitizer is the opposite of a graphing tool that converts data
      points to graphs. The process is shown below - an image file is
      imported, digitized within Engauge, and exported as a table of
      numeric data to a text file. Work can be saved into an Engauge
      DIG file. New features already added to Engauge: - Grid lines
      are displayed for fine adjustments of the axis points that [...]

  * **[grace](http://plasma-gate.weizmann.ac.il/Grace/): Numerical Data Processing and Visualization Tool**

      Grace is a Motif application for two-dimensional data
      visualization. Grace can transform the data using free
      equations, FFT, cross- and auto-correlation, differences,
      integrals, histograms, and much more. The generated figures are
      of high quality. Grace is a very convenient tool for data
      inspection, data transformation, and for making figures for
      publications.

  * **[hdf5](https://portal.hdfgroup.org/display/HDF5/HDF5): A general purpose library and file format for storing scientific data**

      HDF5 is a general purpose library and file format for storing
      scientific data. HDF5 can store two primary objects: datasets
      and groups. A dataset is essentially a multidimensional array of
      data elements, and a group is a structure for organizing objects
      in an HDF5 file. Using these two basic objects, one can create
      and store almost any kind of scientific data structure, such as
      images, arrays of vectors, and structured and unstructured
      grids. You can also mix and match them in HDF5 files according
      to your needs.

  * **[paraview](http://www.paraview.org/): Parallel visualization application**

      ParaView is an open-source, multi-platform data analysis and
      visualization application. ParaView users can quickly build
      visualizations to analyze their data using qualitative and
      quantitative techniques. The data exploration can be done
      interactively in 3D or programmatically using ParaView’s batch
      processing capabilities. ParaView was developed to analyze
      extremely large datasets using distributed memory computing
      resources. It can be run on supercomputers to analyze datasets
      of petascale size as well as on laptops for smaller data. NOTE:
      The version in this package has NOT been compiled with MPI [...]

  * **[pari](http://pari.math.u-bordeaux.fr/): Number Theory-oriented Computer Algebra System**

      PARI is a widely used computer algebra system designed for fast
      computations in number theory (factorizations, algebraic number
      theory, elliptic curves...), but also contains a large number of
      other useful functions to compute with mathematical entities
      such as matrices, polynomials, power series, algebraic numbers,
      etc., and a lot of transcendental functions. This package
      contains the shared libraries. The interactive calculator
      PARI/GP is in package pari-gp.

  * **[veusz](https://veusz.github.io/): GUI scientific plotting package**

      Veusz is a 2D and 3D scientific plotting package, designed to
      create publication-ready vector PDF and SVG output. It features
      GUI, command-line, and scripting interfaces. Graphs are
      constructed from widgets, allowing complex layouts to be
      designed. Veusz supports plotting functions, data with error
      bars, keys, labels, stacked plots, ternary plots, vector plots,
      contours, images, shapes and fitting data. 3D point, surface,
      volume and function plots are also supported.

  * **[zanshin](http://zanshin.kde.org/): Todo/action management software**

      Zanshin Todo is a powerful yet simple application for managing
      your day to day actions. It helps you organize and reduce the
      cognitive pressure of what one has to do in his job and personal
      life. You'll never forget anything anymore, getting your mind
      like water.


```
dnf install LabPlot avogadro engauge-digitizer grace hdf5 paraview \
  pari veusz zanshin
```

[back to index](#index)

***
## Security and Forensics
  * **[ArpON](http://arpon.sourceforge.net/): ARP handler inspection**

      ArpON (ARP handler inspection) is a Host-based solution that
      make the ARP standardized protocol secure in order to avoid the
      Man In The Middle (MITM) attack through the ARP spoofing, ARP
      cache poisoning or ARP poison routing attack.

  * **[aide](http://sourceforge.net/projects/aide): Intrusion detection environment**

      AIDE (Advanced Intrusion Detection Environment) is a file
      integrity checker and intrusion detection program.

  * **[argus](http://qosient.com/argus): Network transaction audit tool**

      Argus (Audit Record Generation and Utilization System) is an IP
      network transaction audit tool. The data generated by argus can
      be used for a wide range of tasks such as network operations,
      security and performance management.

  * **[argus-clients](http://qosient.com/argus): Client tools for argus network audit**

      Clients to the argus probe which process and display
      information.

  * **[arp-scan](http://www.nta-monitor.com/tools/arp-scan/): Scanning and fingerprinting tool**

      arp-scan is a command-line tool that uses the ARP protocol to
      discover and fingerprint IP hosts on the local network.

  * **[arpwatch](http://ee.lbl.gov/): Network monitoring tools for tracking IP addresses on a network**

      The arpwatch package contains arpwatch and arpsnmp. Arpwatch and
      arpsnmp are both network monitoring tools. Both utilities
      monitor Ethernet or FDDI network traffic and build databases of
      Ethernet/IP address pairs, and can report certain changes via
      email. Install the arpwatch package if you need networking
      monitoring devices which will automatically keep track of the IP
      addresses on your network.

  * **✓ [ccrypt](http://ccrypt.sourceforge.net/): Secure encryption and decryption of files and streams**

      ccrypt is a utility for encrypting and decrypting files and
      streams. It was designed as a replacement for the standard unix
      crypt utility, which is notorious for using a very weak
      encryption algorithm.

  * **[chntpw](http://pogostick.net/~pnh/ntpasswd/): Change passwords in Windows SAM files**

      This is a utility to (re)set the password of any user that has a
      valid (local) account on your Windows NT/2k/XP/Vista etc system.
      You do not need to know the old password to set a new one. It
      works offline, that is, you have to shutdown your computer and
      boot off a floppy disk or CD or another system. Will detect and
      offer to unlock locked or disabled out user accounts! There is
      also a registry editor and other registry utilities that works
      under Linux/Unix, and can be used for other things than password
      editing.

  * **✓ [clamav](https://www.clamav.net/): End-user tools for the Clam Antivirus scanner**

      Clam AntiVirus is an anti-virus toolkit for UNIX. The main
      purpose of this software is the integration with mail servers
      (attachment scanning). The package provides a flexible and
      scalable multi-threaded daemon, a command line scanner, and a
      tool for automatic updating via Internet. The programs are based
      on a shared library distributed with the Clam AntiVirus package,
      which you can use with your own software. The virus database is
      based on the virus database from OpenAntiVirus, but contains
      additional signatures (including signatures for popular
      polymorphic viruses, too) and is KEPT UP TO DATE.

  * **✓ [clamav-update](https://www.clamav.net/): Auto-updater for the Clam Antivirus scanner data-files**

      This package contains programs which can be used to update the
      clamav anti-virus database automatically. It uses the
      freshclam(1) utility for this task. To activate it, uncomment
      the entry in /etc/cron.d/clamav-update. Use this package when
      you go updating the virus database regulary and do not want to
      download a &gt;120MB sized rpm-package with outdated virus
      definitions.

  * **✓ [cryptsetup](https://gitlab.com/cryptsetup/cryptsetup): A utility for setting up encrypted disks**

      The cryptsetup package contains a utility for setting up disk
      encryption using dm-crypt kernel module.

  * **✓ [dcfldd](http://dcfldd.sourceforge.net/): Improved dd, useful for forensics and security**

      dcfldd is an enhanced version of GNU dd with features useful for
      forensics and security. dcfldd has the following additional
      features: ' Hashing on-the-fly - dcfldd can hash the input data
      as it is being transferred, helping to ensure data integrity. '
      Status output - dcfldd can update the user of its progress in
      terms of the amount of data transferred and how much longer
      operation will take. ' Flexible disk wipes - dcfldd can be used
      to wipe disks quickly and with a known pattern if desired. '
      Image/wipe Verify - dcfldd can verify that a target drive is a
      bit-for-bit match of the specified input file or pattern. [...]

  * **[ddrescue](http://www.gnu.org/software/ddrescue/ddrescue.html): Data recovery tool trying hard to rescue data in case of read errors**

      GNU ddrescue is a data recovery tool. It copies data from one
      file or block device (hard disc, cd-rom, etc) to another, trying
      hard to rescue data in case of read errors. GNU ddrescue does
      not truncate the output file if not asked to. So, every time you
      run it on the same output file, it tries to fill in the gaps.

  * **[dsniff](http://www.monkey.org/~dugsong/dsniff/): Tools for network auditing and penetration testing**

      A collection of tools for network auditing and penetration
      testing. Dsniff, filesnarf, mailsnarf, msgsnarf, urlsnarf and
      webspy allow to passively monitor a network for interesting data
      (passwords, e-mail, files). Arpspoof, dnsspoof and macof
      facilitate the interception of network traffic normally
      unavailable to an attacker (e.g, due to layer-2 switching).
      Sshmitm and webmitm implement active monkey-in-the-middle
      attacks against redirected SSH and HTTPS sessions by exploiting
      weak bindings in ad-hoc PKI.

  * **[extundelete](http://extundelete.sourceforge.net): An ext3 and ext4 file system undeletion utility**

      extundelete is a utility that can recover deleted files from an
      ext3 or ext4 partition. extundelete uses the information stored
      in the partition's journal to attempt to recover a file that has
      been deleted from the partition. There is no guarantee that any
      particular file will be able to be undeleted, so always try to
      have a good backup system in place, or at least put one in place
      after recovering your files!

  * **[firewalk](http://www.packetfactory.net/projects/firewalk/): Active reconnaissance network security tool**

      Firewalk is an active reconnaissance network security tool that
      attempts to determine what layer 4 protocols a given IP
      forwarding device will pass.

  * **[foremost](http://foremost.sf.net): Recover files by "carving" them from a raw disk**

      Foremost recovers files files based on their headers, footers,
      and internal data structures. This process is commonly referred
      to as data carving. Foremost can work on a raw disk drive or
      image file generated by dd. The headers and footers can be
      specified by a configuration file or you can use command line
      switches to specify built-in file types. These built-in types
      look at the data structures of a given file format allowing for
      a more reliable and faster recovery.

  * **[hydra](https://github.com/vanhauser-thc/thc-hydra): Very fast network log-on cracker**

      Hydra is a parallelized log-in cracker which supports numerous
      protocols to attack. New modules are easy to add, beside that,
      it is flexible and very fast. This tool gives researchers and
      security consultants the possibility to show how easy it would
      be to gain unauthorized access from remote to a system.

  * **✓ [keepassxc](http://www.keepassxc.org/): Cross-platform password manager**

      KeePassXC is a community fork of KeePassX KeePassXC is an
      application for people with extremely high demands on secure
      personal data management. KeePassXC saves many different
      information e.g. user names, passwords, urls, attachemts and
      comments in one single database. For a better management user-
      defined titles and icons can be specified for each single entry.
      Furthermore the entries are sorted in groups, which are
      customizable as well. The integrated search function allows to
      search in a single group or the complete database. KeePassXC
      offers a little utility for secure password generation. [...]

  * **[lynis](https://cisofy.com/lynis/): Security and system auditing tool**

      Lynis is an auditing and hardening tool for Unix/Linux and you
      might even call it a compliance tool. It scans the system and
      installed software. Then it performs many individual security
      control checks. It determines the hardening state of the
      machine, detects security issues and provides suggestions to
      improve the security defense of the system.

  * **[mcrypt](http://mcrypt.sourceforge.net/): Replacement for crypt()**

      MCrypt is a replacement for the old crypt() package and crypt(1)
      command, with extensions. It allows developers to use a wide
      range of encryption functions, without making drastic changes to
      their code. It allows users to encrypt files or data streams
      without having to be cryptographers.

  * **[nikto](https://www.cirt.net/Nikto2): Web server scanner**

      Nikto is a web server scanner which performs comprehensive tests
      against web servers for multiple items, including over 3300
      potentially dangerous files/CGIs, versions on over 625 servers,
      and version specific problems on over 230 servers. Scan items
      and plugins are frequently updated and can be automatically
      updated (if desired).

  * **✓ [nwipe](https://github.com/martijnvanbrummelen/nwipe): Securely erase disks using a variety of recognized methods**

      The nwipe is a command that will securely erase disks using a
      variety of recognized methods. It is a fork of the dwipe command
      used by Darik's Boot and Nuke (dban). Nwipe was created out of
      need to run the DBAN dwipe command outside of DBAN. This allows
      it to use any host distribution which gives better hardware
      support. It is essentially the same as dwipe, with a few
      changes: - pthreads is used instead of fork - The parted library
      is used to detect drives - The code is designed to be compiled
      with gcc

  * **[psad](https://www.cipherdyne.org/psad/): Port Scan Attack Detector (psad) watches for suspect traffic**

      Port Scan Attack Detector (psad) is a lightweight system daemon
      written in Perl designed to work with Linux iptables firewalling
      code to detect port scans and other suspect traffic. It features
      a set of highly configurable danger thresholds (with sensible
      defaults provided), verbose alert messages that include the
      source, destination, scanned port range, begin and end times,
      tcp flags and corresponding nmap options, reverse DNS info,
      email and syslog alerting, automatic blocking of offending ip
      addresses via dynamic configuration of iptables rulesets, and
      passive operating system fingerprinting. In addition, psad [...]

  * **✓ [pwgen](http://sf.net/projects/pwgen): Automatic password generation**

      pwgen generates random, meaningless but pronounceable passwords.
      These passwords contain either only lowercase letters, or upper
      and lower case, or upper case, lower case and numeric digits.
      Upper case letters and numeric digits are placed in a way that
      eases memorizing the password.

  * **[radamsa](http://code.google.com/p/ouspg/wiki/Radamsa): Test case generator for robustness testing**

      Radamsa is a test case generator for robustness testing. It can
      be used to test how well a program can stand malformed and
      potentially malicious inputs.

  * **[rkhunter](http://rkhunter.sourceforge.net/): A host-based tool to scan for rootkits, backdoors and local exploits**

      Rootkit Hunter (RKH) is an easy-to-use tool which checks
      computers running UNIX (clones) for the presence of rootkits and
      other unwanted tools.

  * **[safecopy](http://safecopy.sourceforge.net/): Safe copying of files and partitions**

      safecopy is a data recovery tool which tries to extract as much
      data as possible from a problematic (i.e. damaged sectors)
      source - like floppy drives, harddisk partitions, CDs, tape
      devices, ..., where other tools like dd would fail doe to I/O
      errors.

  * **[scrub](http://code.google.com/p/diskscrub/): Disk scrubbing program**

      Scrub writes patterns on files or disk devices to make
      retrieving the data more difficult. It operates in one of three
      modes: 1) the special file corresponding to an entire disk is
      scrubbed and all data on it is destroyed; 2) a regular file is
      scrubbed and only the data in the file (and optionally its name
      in the directory entry) is destroyed; or 3) a regular file is
      created, expanded until the file system is full, then scrubbed
      as in 2).

  * **[skipfish](http://code.google.com/p/skipfish/): Web application security scanner**

      High-performance, easy, and sophisticated Web application
      security testing tool. It features a single-threaded
      multiplexing HTTP stack, heuristic detection of obscure Web
      frameworks, and advanced, differential security checks capable
      of detecting blind injection vulnerabilities, stored XSS, and so
      forth.

  * **[steghide](http://steghide.sourceforge.net): A steganography program**

      Steghide is a steganography program that is able to hide data in
      various kinds of image- and audio-files. The color- respectivly
      sample-frequencies are not changed thus making the embedding
      resistant against first-order statistical tests. Features of
      steghide include compression and encryption of embedded data,
      embedding of a checksum to verify the integrity of the extracted
      data and support for jpeg, bmp, wav and au files.

  * **[usbguard](https://usbguard.github.io/): A tool for implementing USB device usage policy**

      The USBGuard software framework helps to protect your computer
      against rogue USB devices by implementing basic
      whitelisting/blacklisting capabilities based on USB device
      attributes.

  * **[xca](https://hohnstaedt.de/xca/): Graphical X.509 certificate management tool**

      X Certificate and Key management is a graphic interface for
      managing asymmetric keys like RSA or DSA, certificates and
      revocation lists. It is intended as a small CA for creation and
      signing certificates. It uses the OpenSSL library for the
      cryptographic operations. Certificate signing requests
      (PKCS#10), certificates (X509v3), the signing of requests, the
      creation of self-signed certificates, certificate revocation
      lists and SmartCards are supported. For an easy company-wide
      use, customizable templates can be used for certificate and
      request generation. The PKI structures can be imported and [...]

  * **[zzuf](http://sam.zoy.org/zzuf/): Transparent application input fuzzer**

      zzuf is a transparent application input fuzzer. It works by
      intercepting file operations and changing random bits in the
      program's input. zzuf's behaviour is deterministic, making it
      easy to reproduce bugs.


```
dnf install ArpON aide argus argus-clients arp-scan arpwatch ccrypt \
  chntpw clamav clamav-update cryptsetup dcfldd ddrescue dsniff \
  extundelete firewalk foremost hydra keepassxc lynis mcrypt nikto \
  nwipe psad pwgen radamsa rkhunter safecopy scrub skipfish steghide \
  usbguard xca zzuf
```

[back to index](#index)

***
## Smart Cards

  If using the YubiKey 4 as a PIV smart card, be sure and run
`sudo systemctl enable pcscd; sudo systemctl start pcscd`.

See also https://www.yubico.com/faq/enable-u2f-linux/

  * **[opensc](https://github.com/OpenSC/OpenSC/wiki): Smart card library and applications**

      OpenSC provides a set of libraries and utilities to work with
      smart cards. Its main focus is on cards that support
      cryptographic operations, and facilitate their use in security
      applications such as authentication, mail encryption and digital
      signatures. OpenSC implements the PKCS#11 API so applications
      supporting this API (such as Mozilla Firefox and Thunderbird)
      can use it. On the card OpenSC implements the PKCS#15 standard
      and aims to be compatible with every software/card that does so,
      too.

  * **[pcsc-tools](http://ludovic.rousseau.free.fr/softwares/pcsc-tools/): Tools to be used with smart cards and PC/SC**

      The pcsc-tools package contains some useful tools for a PC/SC
      user: pcsc_scan regularly scans connected PC/SC smart card
      readers and prints detected events, ATR_analysis analyzes smart
      card ATRs (Anwser To Reset), and scriptor sends commands to a
      smart card.

  * **[yubico-piv-tool](https://developers.yubico.com/yubico-piv-tool/): Tool for interacting with the PIV applet on a YubiKey NEO**

      The Yubico PIV tool is used for interacting with the Privilege
      and Identification Card (PIV) applet on a YubiKey NEO. With it
      you may generate keys on the device, importing keys and
      certificates, and create certificate requests, and other
      operations. A shared library and a command-line tool is
      included.

  * **[yubikey-personalization-gui](http://opensource.yubico.com/yubikey-personalization-gui/): GUI for Yubikey personalization**

      Yubico's YubiKey can be re-programmed with a new AES key. This
      is a graphical tool that makes this an easy task.

  * **[yubioath-desktop](https://github.com/Yubico/yubioath-desktop): Yubikey tool for generating OATH event-based HOTP and time-based TOTP codes**

      The Yubico Authenticator is a graphical desktop tool and CLI for
      generating Open AuTHentication (OATH) event-based HOTP and time-
      based TOTP one-time password codes, with the help of a YubiKey
      that protects the shared secrets.


```
dnf install opensc pcsc-tools yubico-piv-tool \
  yubikey-personalization-gui yubioath-desktop
```

[back to index](#index)

***
## System Tools
  * **✓ [ack](http://beyondgrep.com/): Grep-like text finder**

      Ack is designed as a replacement for grep.

  * **[alien](https://sourceforge.net/projects/alien-pkg-convert/): Converter between the rpm, dpkg, stampede slp, and Slackware tgz file formats**

      Alien is a program that converts between the rpm, dpkg, stampede
      slp, and Slackware tgz file formats. If you want to use a
      package from another distribution than the one you have
      installed on your system, you can use alien to convert it to
      your preferred package format and install it.

  * **[augeas](http://augeas.net/): A library for changing configuration files**

      A library for programmatically editing configuration files.
      Augeas parses configuration files into a tree structure, which
      it exposes through its public API. Changes made through the API
      are written back to the initially read files. The transformation
      works very hard to preserve comments and formatting details. It
      is controlled by ''lens'' definitions that describe the file
      format and the transformation into a tree.

  * **[bash-completion](https://github.com/scop/bash-completion): Programmable completion for Bash**

      bash-completion is a collection of shell functions that take
      advantage of the programmable completion feature of bash.

  * **[biosdevname](http://linux.dell.com/files/biosdevname): Udev helper for naming devices per BIOS names**

      biosdevname in its simplest form takes a kernel device name as
      an argument, and returns the BIOS-given name it "should" be.
      This is necessary on systems where the BIOS name for a given
      device (e.g. the label on the chassis is "Gb1") doesn't map
      directly and obviously to the kernel name (e.g. eth0).

  * **[ccze](http://bonehunter.rulez.org/CCZE.html): A robust log colorizer**

      CCZE is a roboust and modular log colorizer, with plugins for
      apm, exim, fetchmail, httpd, postfix, procmail, squid, syslog,
      ulogd, vsftpd, xferlog and more.

  * **[collectd](https://collectd.org/): Statistics collection daemon for filling RRD files**

      collectd is a daemon which collects system performance
      statistics periodically and provides mechanisms to store the
      values in a variety of ways, for example in RRD files.

  * **[collectl](http://collectl.sourceforge.net): A utility to collect various Linux performance data**

      A utility to collect Linux performance data

  * **[colordiff](http://www.colordiff.org/): Color terminal highlighter for diff files**

      Colordiff is a wrapper for diff and produces the same output but
      with pretty syntax highlighting. Color schemes can be
      customized.

  * **[daemonize](http://www.clapper.org/software/daemonize/): Run a command as a Unix daemon**

      daemonize runs a command as a Unix daemon. As defined in W.
      Richard Stevens' 1990 book, Unix Network Programming (Addison-
      Wesley, 1990), a daemon is "a process that executes 'in the
      background' (i.e., without an associated terminal or login
      shell) either waiting for some event to occur, or waiting to
      perform some specified task on a periodic basis." Upon startup,
      a typical daemon program will: - Close all open file descriptors
      (especially standard input, standard output and standard error)
      - Change its working directory to the root filesystem, to ensure
      that it doesn’t tie up another filesystem and prevent it [...]

  * **[datefudge](http://packages.qa.debian.org/d/datefudge.html): Fake the system date**

      This program (and preload library) fakes the system date so that
      programs think the wall clock is ... different. The faking is
      not complete; time-stamp on files are not affected in any way.
      This package is useful if you want to test the date handling of
      your programs without changing the system clock.

  * **[debootstrap](https://wiki.debian.org/Debootstrap): Debian GNU/Linux bootstrapper**

      debootstrap is used to create a Debian base system from scratch,
      without requiring the availability of dpkg or apt. It does this
      by downloading .deb files from a mirror site, and carefully
      unpacking them into a directory which can eventually be chrooted
      into. This might be often useful coupled with virtualization
      techniques to run Debian GNU/Linux guest system.

  * **[dos2unix](http://waterlan.home.xs4all.nl/dos2unix.html): Text file format converters**

      Convert text files with DOS or Mac line endings to Unix line
      endings and vice versa.

  * **[facter](https://puppetlabs.com/facter): Command and ruby library for gathering system information**

      Facter is a lightweight program that gathers basic node
      information about the hardware and operating system. Facter is
      especially useful for retrieving things like operating system
      names, hardware characteristics, IP addresses, MAC addresses,
      and SSH keys. Facter is extensible and allows gathering of node
      information that may be custom or site specific. It is easy to
      extend by including your own custom facts. Facter can also be
      used to create conditional expressions in Puppet that key off
      the values returned by facts.

  * **[fakechroot](https://github.com/dex4er/fakechroot): Gives a fake chroot environment**

      fakechroot runs a command in an environment were is additionally
      possible to use the chroot(8) call without root privileges. This
      is useful for allowing users to create their own chrooted
      environment with possibility to install another packages without
      need for root privileges.

  * **[fakeroot](https://tracker.debian.org/pkg/fakeroot): Gives a fake root environment**

      fakeroot runs a command in an environment wherein it appears to
      have root privileges for file manipulation. fakeroot works by
      replacing the file manipulation library functions (chmod(2),
      stat(2) etc.) by ones that simulate the effect the real library
      functions would have had, had the user really been root.

  * **fedora-modular [fish](https://fishshell.com): Friendly interactive shell**

      fish is a fully-equipped command line shell (like bash or zsh)
      that is smart and user-friendly. fish supports powerful features
      like syntax highlighting, autosuggestions, and tab completions
      that just work, with nothing to learn or configure.

  * **[lshw](http://ezix.org/project/wiki/HardwareLiSter): Hardware lister**

      lshw is a small tool to provide detailed informaton on the
      hardware configuration of the machine. It can report exact
      memory configuration, firmware version, mainboard configuration,
      CPU version and speed, cache configuration, bus speed, etc. on
      DMI-capable x86 systems and on some PowerPC machines (PowerMac
      G4 is known to work). Information can be output in plain text,
      XML or HTML.

  * **✓ [lsof](https://github.com/lsof-org/lsof): A utility which lists open files on a Linux/UNIX system**

      Lsof stands for LiSt Open Files, and it does just that: it lists
      information about files that are open by the processes running
      on a UNIX system.

  * **✓ [lsscsi](http://sg.danny.cz/scsi/lsscsi.html): List SCSI devices (or hosts) and associated information**

      Uses information provided by the sysfs pseudo file system in
      Linux kernel 2.6 series to list SCSI devices or all SCSI hosts.
      Includes a "classic" option to mimic the output of "cat
      /proc/scsi/scsi" that has been widely used prior to the lk 2.6
      series. Author: -------- Doug Gilbert
      &lt;dgilbert(at)interlog(dot)com&gt;

  * **[moreutils](http://kitenet.net/~joey/code/moreutils/): Additional unix utilities**

      This is a growing collection of the unix tools that nobody
      thought to write thirty years ago. So far, it includes the
      following utilities: - chronic: runs a command quietly, unless
      it fails - combine: combine the lines in two files using boolean
      operations - errno: look up errno names and descriptions -
      ifdata: get network interface info without parsing ifconfig
      output - ifne: run a program if the standard input is not empty
      - isutf8: check if a file or standard input is utf-8 - lckdo:
      execute a program with a lock held - mispipe: pipe two commands,
      returning the exit status of the first - parallel: run [...]

  * **✓ [multitail](http://www.vanheusden.com/multitail/): View one or multiple files like tail but with multiple windows**

      MultiTail lets you view one or multiple files like the original
      tail program. The difference is that it creates multiple windows
      on your console (with ncurses). It can also monitor wildcards:
      if another file matching the wildcard has a more recent
      modification date, it will automatically switch to that file.
      That way you can, for example, monitor a complete directory of
      files. Merging of 2 or even more logfiles is possible. It can
      also use colors while displaying the logfiles (through regular
      expressions), for faster recognition of what is important and
      what not. Multitail can also filter lines (again with [...]

  * **✓ [ntpdate](http://www.ntp.org): Utility to set the date and time via NTP**

      ntpdate is a program for retrieving the date and time from NTP
      servers.

  * **[openrdate](http://sourceforge.net/projects/openrdate): Good-old rdate date and time-setting software**

      Good-old date- and time-setting rdate software implementing RFC
      868 (inetd time) and RFC 2030 (SNTP/NTP) protocols. An
      independent package of OpenBSD's rdate program.

  * **[psmisc](https://gitlab.com/psmisc/psmisc): Utilities for managing processes on your system**

      The psmisc package contains utilities for managing processes on
      your system: pstree, killall, fuser and pslog. The pstree
      command displays a tree structure of all of the running
      processes on your system. The killall command sends a specified
      signal (SIGTERM if nothing is specified) to processes identified
      by name. The fuser command identifies the PIDs of processes that
      are using specified files or filesystems. The pslog command
      shows the path of log files owned by a given process.

  * **[rng-tools](https://github.com/nhorman/rng-tools): Random number generator related utilities**

      Hardware random number generation tools.

  * **[rpmlint](https://github.com/rpm-software-management/rpmlint): Tool for checking common errors in RPM packages**

      rpmlint is a tool for checking common errors in RPM packages.
      Binary and source packages as well as spec files can be checked.

  * **[schroot](http://packages.debian.org/schroot): Execute commands in a chroot environment**

      schroot allows users to execute commands or interactive shells
      in different chroots. Any number of named chroots may be
      created, and access permissions given to each, including root
      access for normal users, on a per-user or per-group basis.
      Additionally, schroot can switch to a different user in the
      chroot, using PAM for authentication and authorisation. All
      operations are logged for security. Several different types of
      chroot are supported, including normal directories in the
      filesystem, and also block devices. Sessions, persistent chroots
      created on the fly from files (tar with optional [...]

  * **[screen](http://www.gnu.org/software/screen): A screen manager that supports multiple logins on one terminal**

      The screen utility allows you to have multiple logins on just
      one terminal. Screen is useful for users who telnet into a
      machine or are connected via a dumb terminal, but want to use
      more than just one login. Install the screen package if you need
      a screen manager that can support multiple logins on one
      terminal.

  * **[screenfetch](https://github.com/KittyKatt/screenFetch): A "Bash Screenshot Information Tool"**

      This handy Bash script can be used to generate one of those
      nifty terminal theme information + ASCII distribution logos you
      see in everyone's screen-shots nowadays. It will auto-detect
      your distribution and display an ASCII version of that
      distribution's logo and some valuable information to the right.
      There are options to specify no ASCII art, colors, taking a
      screen-shot upon displaying info, and even customizing the
      screen-shot command! This script is very easy to add to and can
      easily be extended.

  * **[shutter](http://shutter-project.org): GTK+2-based screenshot application written in Perl**

      Shutter is a GTK+ 2.x based screenshot application written in
      Perl. Shutter covers all features of common command line tools
      like scrot or import and adds reasonable new features combined
      with a comfortable GUI using the GTK+ 2.x framework.

  * **✓ [solaar](https://github.com/pwr/Solaar): Device manager for a wide range of Logitech devices**

      Solaar is a device manager for Logitech's Unifying Receiver
      peripherals. It is able to pair/unpair devices to the receiver
      and, for most devices, read battery status. gtk3 is recommended.
      Without it, you can run solaar commands to view the
      configuration of the devices and pair/unpair peripherals but you
      cannot use the graphical interface.

  * **[sshpass](http://sshpass.sourceforge.net/): Non-interactive SSH authentication utility**

      Tool for non-interactively performing password authentication
      with so called "interactive keyboard password authentication" of
      SSH. Most users should use more secure public key authentication
      of SSH instead.

  * **[sysstat](http://sebastien.godard.pagesperso-orange.fr/): Collection of performance monitoring tools for Linux**

      The sysstat package contains the sar, sadf, mpstat, iostat,
      tapestat, pidstat, cifsiostat and sa tools for Linux. The sar
      command collects and reports system activity information. The
      information collected by sar can be saved in a file in a binary
      format for future inspection. The statistics reported by sar
      concern I/O transfer rates, paging activity, process-related
      activities, interrupts, network activity, memory and swap space
      utilization, CPU utilization, kernel activities and TTY
      statistics, among others. Both UP and SMP machines are fully
      supported. The sadf command may be used to display data [...]

  * **[tmpwatch](https://fedorahosted.org/tmpwatch/): A utility for removing files based on when they were last accessed**

      The tmpwatch utility recursively searches through specified
      directories and removes files which have not been accessed in a
      specified period of time. Tmpwatch is normally used to clean up
      directories which are used for temporarily holding files (for
      example, /tmp). Tmpwatch ignores symlinks, won't switch
      filesystems and only removes empty directories and regular
      files.

  * **✓ [tmux](https://tmux.github.io/): A terminal multiplexer**

      tmux is a "terminal multiplexer." It enables a number of
      terminals (or windows) to be accessed and controlled from a
      single terminal. tmux is intended to be a simple, modern, BSD-
      licensed alternative to programs such as GNU Screen.

  * **[trousers](http://trousers.sourceforge.net): TCG's Software Stack v1.2**

      TrouSerS is an implementation of the Trusted Computing Group's
      Software Stack (TSS) specification. You can use TrouSerS to
      write applications that make use of your TPM hardware. TPM
      hardware can create, store and use RSA keys securely (without
      ever being exposed in memory), verify a platform's software
      state using cryptographic hashes and more.

  * **[udisks](http://www.freedesktop.org/wiki/Software/udisks): Storage Management Service**

      udisks provides a daemon, D-Bus API and command line tools for
      managing disks and storage devices.

  * **[uid_wrapper](http://cwrap.org/): A wrapper for privilege separation**

      Some projects like a file server need privilege separation to be
      able to switch to the connection user and do file operations.
      uid_wrapper convincingly lies to the application letting it
      believe it is operating as root and even switching between UIDs
      and GIDs as needed. To use it set the following environment
      variables: LD_PRELOAD=libuid_wrapper.so UID_WRAPPER=1 This
      package doesn't have a devel package cause this project is for
      development/testing.

  * **[unetbootin](https://unetbootin.github.io/): Create bootable Live USB drives for a variety of Linux distributions**

      UNetbootin allows you to create bootable Live USB drives for a
      variety of Linux distributions from Windows or Linux, without
      requiring you to burn a CD. You can either let it download one
      of the many distributions supported out-of-the-box for you, or
      supply your own Linux .iso file if you've already downloaded one
      or your preferred distribution isn't on the list.

  * **[usbview](http://www.kroah.com/linux-usb/): USB topology and device viewer**

      Display information about the topology of the devices connected
      to the USB bus on a Linux machine. It also displays detailed
      information on the individual devices.

  * **[wdiff](http://www.gnu.org/software/wdiff/): A front-end to GNU diff**

      'wdiff' is a front-end to GNU 'diff'. It compares two files,
      finding which words have been deleted or added to the first in
      order to create the second. It has many output formats and
      interacts well with terminals and pagers (notably with 'less').
      'wdiff' is particularly useful when two texts differ only by a
      few words and paragraphs have been refilled.

  * **[xdotool](http://www.semicomplete.com/projects/xdotool/): Fake keyboard/mouse input**

      This tool lets you programmatically (or manually) simulate
      keyboard input and mouse activity, move and re-size windows,
      etc.

  * **[yamllint](https://github.com/adrienverge/yamllint): A linter for YAML files**

      A linter for YAML files. yamllint does not only check for syntax
      validity, but for weirdnesses like key repetition and cosmetic
      problems such as lines length, trailing spaces, indentation,
      etc.


```
dnf install ack alien augeas bash-completion biosdevname ccze collectd \
  collectl colordiff daemonize datefudge debootstrap dos2unix facter \
  fakechroot fakeroot fish lshw lsof lsscsi moreutils multitail \
  ntpdate openrdate psmisc rng-tools rpmlint schroot screen \
  screenfetch shutter solaar sshpass sysstat tmpwatch tmux trousers \
  udisks uid_wrapper unetbootin usbview wdiff xdotool yamllint
```

[back to index](#index)

***
## System Tools - Performance and Tuning
  * **[atop](http://www.atcomputing.nl/Tools/atop/): An advanced interactive monitor to view the load on system and process level**

      An advanced interactive monitor for Linux-systems to view the
      load on system-level and process-level. The command atop has
      some major advantages compared to other performance-monitors: -
      Resource consumption by all processes - Utilization of all
      relevant resources - Permanent logging of resource utilization -
      Highlight critical resources - Watch activity only - Watch
      deviations only - Accumulated process activity per user -
      Accumulated process activity per program For more informations:
      http://www.atcomputing.nl/Tools/atop The package does not make
      use of the patches available at [...]

  * **✓ [htop](http://hisham.hm/htop/): Interactive process viewer**

      htop is an interactive text-mode process viewer for Linux,
      similar to top(1).

  * **✓ [iotop](http://guichaz.free.fr/iotop/): Top like utility for I/O**

      Linux has always been able to show how much I/O was going on
      (the bi and bo columns of the vmstat 1 command). iotop is a
      Python program with a top like UI used to show of behalf of
      which process is the I/O going on.

  * **✓ [kernel-tools](http://www.kernel.org/): Assortment of tools for the Linux kernel**

      This package contains the tools/ directory from the kernel
      source and the supporting documentation.


  Protip: `sudo cpupower frequency-set -g powersave` to conserve battery usage.

  * **[nmon](http://nmon.sourceforge.net): Nigel's performance Monitor for Linux**

      nmon is a systems administrator, tuner, benchmark tool, which
      provides information about CPU, disks, network, etc., all in one
      view.

  * **✓ [powertop](http://01.org/powertop/): Power consumption monitor**

      PowerTOP is a tool that finds the software component(s) that
      make your computer use more power than necessary while it is
      idle.


  Protip: `sudo powertop --auto-tune` to conserve battery usage.

  * **[sysprof](http://www.sysprof.com): A system-wide Linux profiler**

      Sysprof is a sampling CPU profiler for Linux that collects
      accurate, high-precision data and provides efficient access to
      the sampled calltrees.


```
dnf install atop htop iotop kernel-tools nmon powertop sysprof
```

[back to index](#index)

***
## System Tools - Sensors and Monitoring
  * **[freeipmi](http://www.gnu.org/software/freeipmi/): IPMI remote console and system management software**

      The FreeIPMI project provides "Remote-Console" (out-of-band) and
      "System Management Software" (in-band) based on Intelligent
      Platform Management Interface specification.

  * **[ganglia](http://ganglia.sourceforge.net/): Distributed Monitoring System**

      Ganglia is a scalable, real-time monitoring and execution
      environment with all execution requests and statistics expressed
      in an open well-defined XML format.

  * **[hddtemp](http://savannah.nongnu.org/projects/hddtemp/): Hard disk temperature tool**

      hddtemp is a tool that gives you the temperature of your hard
      drive by reading S.M.A.R.T. information.

  * **[hdparm](https://sourceforge.net/projects/hdparm/): A utility for displaying and/or setting hard disk parameters**

      Hdparm is a useful system utility for setting (E)IDE hard drive
      parameters. For example, hdparm can be used to tweak hard drive
      performance and to spin down hard drives for power conservation.

  * **[ipmiutil](http://ipmiutil.sourceforge.net): Easy-to-use IPMI server management utilities**

      The ipmiutil package provides easy-to-use utilities to view the
      SEL, perform an IPMI chassis reset, set up the IPMI LAN and
      Platform Event Filter entries to allow SNMP alerts, Serial-Over-
      LAN console, event daemon, and other IPMI tasks. These can be
      invoked with the metacommand ipmiutil, or via subcommand
      shortcuts as well. IPMIUTIL can also write sensor thresholds,
      FRU asset tags, and has a full IPMI configuration save/restore.
      An IPMI driver can be provided by either the OpenIPMI driver
      (/dev/ipmi0) or the Intel IPMI driver (/dev/imb), etc. If used
      locally and no driver is detected, ipmiutil will use user- [...]

  * **[mcelog](https://github.com/andikleen/mcelog): Tool to translate x86-64 CPU Machine Check Exception data**

      mcelog is a utility that collects and decodes Machine Check
      Exception data on x86-32 and x86-64 systems.

  * **[monit](http://mmonit.com/monit/): Manages and monitors processes, files, directories and devices**

      monit is a utility for managing and monitoring, processes,
      files, directories and devices on a UNIX system. Monit conducts
      automatic maintenance and repair and can execute meaningful
      causal actions in error situations.

  * **[smartmontools](http://smartmontools.sourceforge.net/): Tools for monitoring SMART capable hard disks**

      The smartmontools package contains two utility programs
      (smartctl and smartd) to control and monitor storage systems
      using the Self- Monitoring, Analysis and Reporting Technology
      System (SMART) built into most modern ATA and SCSI hard disks.
      In many cases, these utilities will provide advanced warning of
      disk degradation and failure.

  * **[xsensors](https://github.com/Mystro256/xsensors): An X11 interface to lm_sensors**

      Xsensors is a simple GUI program that allows you to read useful
      data from the lm_sensors library in a digital read-out like
      fashion, such as the temperature, voltage ratings and fan speeds
      of the running computer.


```
dnf install freeipmi ganglia hddtemp hdparm ipmiutil mcelog monit \
  smartmontools xsensors
```

[back to index](#index)

***
## System Tools - Serial Ports
  * **[logserial](http://www.ibiblio.org/pub/Linux/system/serial/logserial-0.4.2.lsm): Package for logging incoming bytes on asynchronous serial ports**

      Package for logging incoming bytes on asynchronous serial ports.
      It was written for loging calls on our telephone central, but
      you can use it for any devices connected to serial ports. From
      version 0.4 it can be used to send data through serial line.

  * **[lrzsz](http://www.ohse.de/uwe/software/lrzsz.html): The lrz and lsz modem communications programs**

      Lrzsz (consisting of lrz and lsz) is a cosmetically modified
      zmodem/ymodem/xmodem package built from the public-domain
      version of the rzsz package. Lrzsz was created to provide a
      working GNU copylefted Zmodem solution for Linux systems.

  * **[minicom](http://alioth.debian.org/projects/minicom/): A text-based modem control and terminal emulation program**

      Minicom is a simple text-based modem control and terminal
      emulation program somewhat similar to MSDOS Telix. Minicom
      includes a dialing directory, full ANSI and VT100 emulation, an
      (external) scripting language, and other features.

  * **[picocom](https://github.com/npat-efault/picocom): Minimal serial communications program**

      As its name suggests, [picocom] is a minimal dumb-terminal
      emulation program. It is, in principle, very much like minicom,
      only it's "pico" instead of "mini"! It was designed to serve as
      a simple, manual, modem configuration, testing, and debugging
      tool. It has also served (quite well) as a low-tech "terminal-
      window" to allow operator intervention in PPP connection scripts
      (something like the ms-windows "open terminal window before /
      after dialing" feature). It could also prove useful in many
      other similar tasks. It is ideal for embedded systems since its
      memory footprint is minimal (less than 20K, when stripped).

  * **[ser2net](http://ser2net.sourceforge.net/): Proxy that allows tcp connections to serial ports**

      ser2net provides a way for a user to connect from a network
      connection to a serial port. It provides all the serial port
      setup, a configuration file to configure the ports, a control
      login for modifying port parameters, monitoring ports, and
      controlling ports.


```
dnf install logserial lrzsz minicom picocom ser2net
```

[back to index](#index)

***
## TeX/LaTeX

  Tex/LaTeX is an old-school typesetting system. It generates, among other
things, those two column academic papers you have seen. You probably
don&apos;t need this, but it can be useful.

  * **[kile](https://kile.sourceforge.io/): (La)TeX source editor and TeX shell**

      Kile is a user friendly (La)TeX editor. The main features are: '
      Compile, convert and view your document with one click. ' Auto-
      completion of (La)TeX commands ' Templates and wizards makes
      starting a new document very little work. ' Easy insertion of
      many standard tags and symbols and the option to define (an
      arbitrary number of) user defined tags. ' Inverse and forward
      search: click in the DVI viewer and jump to the corresponding
      LaTeX line in the editor, or jump from the editor to the
      corresponding page in the viewer. ' Finding chapter or sections
      is very easy, Kile constructs a list of all the chapter [...]

  * **[okular](https://www.kde.org/applications/graphics/okular/): A document viewer**

      Kile defaults to using the KDE PDF viewer, okular, so install
      this along with it.

  * **[texlive](http://tug.org/texlive/): TeX formatting system**

      The TeX Live software distribution offers a complete TeX system
      for a variety of Unix, Macintosh, Windows and other platforms.
      It encompasses programs for editing, typesetting, previewing and
      printing of TeX documents in many different languages, and a
      large collection of TeX macros and font libraries. The
      distribution includes extensive general documentation about TeX,
      as well as the documentation for the included software packages.

  * **[texlive-calcage](http://tug.org/texlive/): Calculate the age of something, in years**

      The package calculates the age of someone or something in years.
      Internally it uses the datenumber package to calculate the age
      in days; conversion from days to years is then performed, taking
      care of leap years and such odd things.

  * **[texlive-fancyhdr](http://tug.org/texlive/): Extensive control of page headers and footers in LaTeX2e**

      The package provides extensive facilities, both for constructing
      headers and footers, and for controlling their use (for example,
      at times when LaTeX would automatically change the heading style
      in use).

  * **[texlive-fnumprint](http://tug.org/texlive/): Print a number in 'appropriate' format**

      The package defines two macros which decide to typeset a number
      either as an Arabic number or as a word (or words) for the
      number. If the number is between zero and twelve (including zero
      and twelve) then words will be used; if the number is outside
      that range, it will be typeset using the package numprint Words
      for English representation of numbers are generated within the
      package, while those for German are generated using the package
      zahl2string.


```
dnf install kile okular texlive texlive-calcage texlive-fancyhdr \
  texlive-fnumprint
```

[back to index](#index)

***
## Terminals
  * **[qterm](https://github.com/qterm/qterm): BBS client for X Window System written in Qt**

      QTerm is a BBS client for X Window System. It supports Telnet,
      SSH1 and SSH2 protocols. It also supports ZMODEM, URL analysis
      and mouse gesture.

  * **[roxterm](https://github.com/realh/roxterm): Terminal emulator**

      ROXTerm is a terminal emulator intended to provide similar
      features to gnome-terminal, based on the same VTE library. It is
      more configurable than gnome-terminal and aimed more at "power"
      users who make heavy use of terminals.

  * **[terminator](http://gnometerminator.blogspot.com/p/introduction.html): Store and run multiple GNOME terminals in one window**

      Multiple GNOME terminals in one window. This is a project to
      produce an efficient way of filling a large area of screen space
      with terminals. This is done by splitting the window into a
      resizeable grid of terminals. As such, you can produce a very
      flexible arrangements of terminals for different tasks.

  * **[tilda](http://github.com/lanoxx/tilda): A Gtk based drop down terminal for Linux and Unix**

      Tilda is a Linux terminal taking after the likeness of many
      classic terminals from first person shooter games, Quake, Doom
      and Half-Life (to name a few), where the terminal has no border
      and is hidden from the desktop until a key is pressed.

  * **✓ [tilix](https://github.com/gnunn1/tilix): Tiling terminal emulator**

      Tilix is a tiling terminal emulator with the following features:
      - Layout terminals in any fashion by splitting them horizontally
      or vertically - Terminals can be re-arranged using drag and drop
      both within and between windows - Terminals can be detached into
      a new window via drag and drop - Input can be synchronized
      between terminals so commands typed in one terminal are
      replicated to the others - The grouping of terminals can be
      saved and loaded from disk - Terminals support custom titles -
      Color schemes are stored in files and custom color schemes can
      be created by simply creating a new file - Transparent [...]

  * **[vte](http://developer.gnome.org/vte/): A terminal emulator**

      VTE is a terminal emulator widget for use with GTK+ 2.0.


```
dnf install qterm roxterm terminator tilda tilix vte
```

[back to index](#index)

***
## VoIP
  * **[linphone](http://www.linphone.org/): Phone anywhere in the whole world by using the Internet**

      Linphone is mostly sip compliant. It works successfully with
      these implementations: ' eStara softphone (commercial software
      for windows) ' Pingtel phones (with DNS enabled and VLAN QOS
      support disabled). ' Hotsip, a free of charge phone for Windows.
      ' Vocal, an open source SIP stack from Vovida that includes a
      SIP proxy that works with linphone since version 0.7.1. '
      Siproxd is a free sip proxy being developed by Thomas Ries
      because he would like to have linphone working behind his
      firewall. Siproxd is simple to setup and works perfectly with
      linphone. ' Partysip aims at being a generic and fully [...]

  * **[twinkle](https://github.com/LubosD/twinkle): SIP-based VoIP client**

      Twinkle is a SIP-based VoIP client.


```
dnf install linphone twinkle
```

[back to index](#index)

***
## Web
  * **[filezilla](https://filezilla-project.org/): FTP, FTPS and SFTP client**

      FileZilla is a FTP, FTPS and SFTP client for Linux with a lot of
      features. - Supports FTP, FTP over SSL/TLS (FTPS) and SSH File
      Transfer Protocol (SFTP) - Cross-platform - Available in many
      languages - Supports resume and transfer of large files greater
      than 4GB - Easy to use Site Manager and transfer queue - Drag &
      drop support - Speed limits - Filename filters - Network
      configuration wizard

  * **[qbittorrent](http://www.qbittorrent.org): A Bittorrent Client**

      A Bittorrent client using rb_libtorrent and a Qt4 Graphical User
      Interface. It aims to be as fast as possible and to provide
      multi-OS, unicode support.


```
dnf install filezilla qbittorrent
```

[back to index](#index)

***
## Web - Archival
  * **✓ [CutyCapt](http://cutycapt.sourceforge.net/): A small command-line utility to capture WebKit's rendering of a web page**

      CutyCapt is a small cross-platform command-line utility to
      capture WebKit's rendering of a web page into a variety of
      vector and bitmap formats, including SVG, PDF, PS, PNG, JPEG,
      TIFF, GIF, and BMP.

  * **[httrack](http://www.httrack.com): Website copier and offline browser**

      HTTrack is a free and easy-to-use offline browser utility. It
      allows the user to download a World Wide Web site from the
      Internet to a local directory, building recursively all
      directories, getting HTML, images, and other files from the
      server to your computer. HTTrack arranges the original site's
      relative link-structure. HTTrack can also update an existing
      mirrored site, and resume interrupted downloads. HTTrack is
      fully configurable, and has an integrated help system.

  * **✓ [wkhtmltopdf](http://wkhtmltopdf.org/): Simple shell utility to convert html to pdf**

      Simple shell utility to convert html to pdf using the webkit
      rendering engine, and qt.


```
dnf install CutyCapt httrack wkhtmltopdf
```

[back to index](#index)

***
## Web - Browsers
  * **[arora](http://code.google.com/p/arora/): A cross platform web browser**

      Arora is a simple, cross platform web browser based on the
      QtWebKit engine. Currently, Arora is still under development,
      but it already has support for browsing and other common
      features such as web history and bookmarks.

  * **[chromium](http://www.chromium.org/Home): A WebKit (Blink) powered web browser**

      Chromium is an open-source web browser, powered by WebKit
      (Blink).

  * **[dillo](http://www.dillo.org/): Very small and fast GUI web browser**

      Dillo is a very small and fast web browser using GTK. Currently:
      no frames,https,javascript.

  * **[icecat](http://www.gnu.org/software/gnuzilla/): GNU version of Firefox browser**

      GNUZilla Icecat is a fully-free fork of Mozilla Firefox ESR.
      Extensions included to this version of IceCat: ' AboutIceCat
      Adds a custom "about:icecat" homepage with links to information
      about the free software and privacy features in IceCat, and
      check-boxes to enable and disable the ones more prone to break
      websites ' LibreJS GNU LibreJS aims to address the JavaScript
      problem described in Richard Stallman's article The JavaScript
      Trap ' HTTPS Everywhere HTTPS Everywhere is an extension that
      encrypts your communications with many major websites, making
      your browsing more secure ' Searxes Third-party Request Blocker

  * **[rekonq](http://rekonq.kde.org/): KDE browser based on QtWebkit**

      rekonq is a KDE browser based on QtWebkit. Its code is based on
      Nokia QtDemoBrowser, just like Arora. It's implementation is
      going to embrace KDE technologies to have a full-featured KDE
      web browser.


```
dnf install arora chromium dillo icecat rekonq
```

[back to index](#index)

***
## Web - Command Line Tools
  * **[aria2](http://aria2.github.io/): High speed download utility with resuming and segmented downloading**

      aria2 is a download utility with resuming and segmented
      downloading. Supported protocols are HTTP/HTTPS/FTP/BitTorrent.
      It also supports Metalink version 3.0. Currently it has
      following features: - HTTP/HTTPS GET support - HTTP Proxy
      support - HTTP BASIC authentication support - HTTP Proxy
      authentication support - FTP support(active, passive mode) - FTP
      through HTTP proxy(GET command or tunneling) - Segmented
      download - Cookie support - It can run as a daemon process. -
      BitTorrent protocol support with fast extension. - Selective
      download in multi-file torrent - Metalink version 3.0 [...]

  * **[axel](https://github.com/axel-download-accelerator/axel): Light command line download accelerator for Linux and Unix**

      Axel tries to accelerate HTTP/FTP downloading process by using
      multiple connections for one file. It can use multiple mirrors
      for a download. Axel has no dependencies and is lightweight, so
      it might be useful as a wget clone on byte-critical systems.

  * **[cadaver](http://www.webdav.org/cadaver/): Command-line WebDAV client**

      cadaver is a command-line WebDAV client, with support for file
      upload, download, on-screen display, in-place editing, namespace
      operations (move/copy), collection creation and deletion,
      property manipulation, and resource locking.

  * **[ctorrent](http://www.rahul.net/dholmes/ctorrent/): Command line BitTorrent client for unix-like environments**

      Enhanced CTorrent is a BitTorrent client for unix-like
      environments. High performance with minimal system resources and
      dependencies are a priority.

  * **✓ [curl](https://curl.haxx.se/): A utility for getting files from remote servers (FTP, HTTP, and others)**

      curl is a command line tool for transferring data with URL
      syntax, supporting FTP, FTPS, HTTP, HTTPS, SCP, SFTP, TFTP,
      TELNET, DICT, LDAP, LDAPS, FILE, IMAP, SMTP, POP3 and RTSP. curl
      supports SSL certificates, HTTP POST, HTTP PUT, FTP uploading,
      HTTP form based upload, proxies, cookies, user+password
      authentication (Basic, Digest, NTLM, Negotiate, kerberos...),
      file transfer resume, proxy tunneling and a busload of other
      useful tricks.

  * **✓ [lftp](http://lftp.yar.ru/): A sophisticated file transfer program**

      LFTP is a sophisticated ftp/http file transfer program. Like
      bash, it has job control and uses the readline library for
      input. It has bookmarks, built-in mirroring, and can transfer
      several files in parallel. It is designed with reliability in
      mind.


  Protip: ... also supports BitTorrent

  * **[mktorrent](https://github.com/Rudde/mktorrent): Command line utility to create BitTorrent metainfo files**

      Command line utility to create BitTorrent metainfo files. See
      --help option for mktorrent command for details on usage.

  * **[ncftp](http://www.ncftp.com/ncftp/): Improved console FTP client**

      Ncftp is an improved FTP client. Ncftp's improvements include
      support for command line editing, command histories, recursive
      gets, automatic anonymous logins, and more.

  * **[snownews](https://kiza.eu/media/software/snownews): A text mode RSS/RDF newsreader**

      Snownews is a text mode RSS/RDF newsreader. It supports all
      versions of RSS natively and supports other formats via plugins.
      The program depends on ncurses for the user interface and uses
      libxml2 for XML parsing. ncurses must be at least version 5.0.
      It should work with any version of libxml2.

  * **[surfraw](https://gitlab.com/surfraw/Surfraw): Shell Users Revolutionary Front Rage Against the Web**

      Surfraw provides a fast unix command line interface to a variety
      of popular WWW search engines and other artifacts of power. It
      reclaims google, altavista, babelfish, dejanews, freshmeat,
      research index, slashdot and many others from the false-prophet,
      pox-infested heathen lands of html-forms, placing these wonders
      where they belong, deep in unix heartland, as god loving
      extensions to the shell. Surfraw abstracts the browser away from
      input. Doing so lets it get on with what it's good at. Browsing.
      Interpretation of linguistic forms is handed back to the shell,
      which is what it, and human beings are good at. Combined [...]

  * **✓ [wget](http://www.gnu.org/software/wget/): A utility for retrieving files using the HTTP or FTP protocols**

      GNU Wget is a file retrieval utility which can use either the
      HTTP or FTP protocols. Wget features include the ability to work
      in the background while you are logged out, recursive retrieval
      of directories, file name wildcard matching, remote file
      timestamp storage and comparison, use of Rest with FTP servers
      and Range with HTTP servers to retrieve files over slow or
      unstable connections, support for Proxy servers, and
      configurability.

  * **✓ [youtube-dl](https://yt-dl.org): A small command-line program to download online videos**

      Small command-line program to download videos from YouTube and
      other sites.


```
dnf install aria2 axel cadaver ctorrent curl lftp mktorrent ncftp \
  snownews surfraw wget youtube-dl
```

[back to index](#index)

***
## Web - Servers and Server Tools

  Protip: `python -m SimpleHTTPServer` to serve the current directory

  * **[awstats](http://awstats.sourceforge.net): Advanced Web Statistics**

      Advanced Web Statistics is a powerful and featureful tool that
      generates advanced web server graphic statistics. This server
      log analyzer works from command line or as a CGI and shows you
      all information your log contains, in graphical web pages. It
      can analyze a lot of web/wap/proxy servers like Apache, IIS,
      Weblogic, Webstar, Squid, ... but also mail or ftp servers. This
      program can measure visits, unique vistors, authenticated users,
      pages, domains/countries, OS busiest times, robot visits, type
      of files, search engines/keywords used, visits duration, HTTP
      errors and more... Statistics can be updated from a [...]

  * **[lighttpd](http://www.lighttpd.net/): Lightning fast webserver with light system requirements**

      Secure, fast, compliant and very flexible web-server which has
      been optimized for high-performance environments. It has a very
      low memory footprint compared to other webservers and takes care
      of cpu-load. Its advanced feature-set (FastCGI, CGI, Auth,
      Output-Compression, URL-Rewriting and many more) make it the
      perfect webserver-software for every server that is suffering
      load problems.

  * **[linkchecker](https://linkcheck.github.io/linkchecker/): Check HTML documents for broken links**

      LinkChecker is a website validator. LinkChecker checks links in
      web documents or full websites. Features: - Recursive and
      multithreaded checking and site crawling - Output in colored or
      normal text, HTML, SQL, CSV, XML or a sitemap graph in different
      formats - HTTP/1.1, HTTPS, FTP, mailto:, news:, nntp:, Telnet
      and local file links support - Restriction of link checking with
      regular expression filters for URLs - Proxy support -
      Username/password authorization for HTTP and FTP and Telnet -
      Honors robots.txt exclusion protocol - Cookie support - HTML5
      support - HTML and CSS syntax check - Antivirus check - [...]


```
dnf install awstats lighttpd linkchecker
```

[back to index](#index)

***
## Web - Text Mode Browsers
  * **[elinks](http://elinks.or.cz): A text-mode Web browser**

      Elinks is a text-based Web browser. Elinks does not display any
      images, but it does support frames, tables and most other HTML
      tags. Elinks' advantage over graphical browsers is its speed--
      Elinks starts and exits quickly and swiftly displays Web pages.

  * **[links](http://links.twibright.com/): Web browser running in both graphics and text mode**

      Links is a web browser capable of running in either graphics or
      text mode. It provides a pull-down menu system, renders complex
      pages, has partial HTML 4.0 support (including tables, frames
      and support for multiple character sets and UTF-8), supports
      color and monochrome terminals and allows horizontal scrolling.

  * **[lynx](http://lynx.browser.org/): A text-based Web browser**

      Lynx is a text-based Web browser. Lynx does not display any
      images, but it does support frames, tables, and most other HTML
      tags. One advantage Lynx has over graphical browsers is speed;
      Lynx starts and exits quickly and swiftly displays web pages.


```
dnf install elinks links lynx
```

[back to index](#index)

# Everything #

All 593 packages:

```
dnf install ArpON CutyCapt ImageMagick LinLog LabPlot VirtualBox Zim \
  aldo aprsd aprsdigi ax25-apps amanda arc anjuta autoconf automake \
  asymptote antiword arduino avra anki anyremote ardour5 audacious \
  audacity-freeworld autossh abcde avogadro aide argus argus-clients \
  arp-scan arpwatch ack alien augeas atop arora aria2 axel awstats \
  backupninja borgbackup btrfs-sxbackup bonnie++ bitlbee bti blosc \
  buildbot bluefish bvi btrfs-progs barcode beets bristol bird \
  bluez-hcidump bmon brasero bash-completion biosdevname callgit \
  cwdaemon cloc clutter code2html clang clang-analyzer cppcheck \
  cppunit claws-mail cachefilesd curlftpfs cantor coq colord cowsay \
  clementine codec2 csound corkscrew cdrdao cdw calcurse ccrypt chntpw \
  clamav clamav-update cryptsetup ccze collectd collectl colordiff \
  chromium cadaver ctorrent curl dump duplicity dia diffpdf dosbox \
  dmenu dxpc dcraw denemo dumb dnsmasq darkstat driftnet devtodo \
  dcfldd ddrescue dsniff daemonize datefudge debootstrap dos2unix \
  dillo enscript e3 ed electric electronics-menu expect exif etherape \
  engauge-digitizer extundelete elinks fbb fldigi fllog flmsg fio \
  frama-c focuswriter fbzx fdupes filelight fuse-dislocker fuse-exfat \
  fuse-sshfs fuse-zip fossil flacon flac feh frescobaldi fluidsynth \
  ffmpeg ffmpegthumbnailer ferm flow-tools freerdp firewalk foremost \
  facter fakechroot fakeroot fish freeipmi filezilla gnuradio \
  gnuradio-examples gpredict gqrx gr-air-modes gr-fcdproplus gr-iqbal \
  gr-osmosdr gr-rds grig gitg gitstats global gource gperf gcc git \
  graphviz geany ghex gerbv gresistor gtkwave gpsim gputils gxemul \
  glusterfs gparted galculator giac gnome-tweaks gpsd gpsdrive grass \
  gsm gstreamer-plugins-base gstreamer-plugins-base-tools \
  gstreamer-plugins-espeak gstreamer-plugins-fc \
  gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free-extras \
  gstreamer1-plugins-bad-free-fluidsynth \
  gstreamer1-plugins-bad-freeworld gstreamer1-plugins-base \
  gstreamer1-plugins-base-tools gstreamer1-plugins-entrans \
  gstreamer1-plugins-entrans-docs gstreamer1-plugins-fc \
  gstreamer1-plugins-good gstreamer1-plugins-good-extras \
  gstreamer1-plugins-ugly gimp gearmand glabels gnucash gnumeric \
  grisbi grace ganglia hackrf hamlib highlight hercstudio hercules \
  hugin httping httpie haproxy hdf5 hydra htop hddtemp hdparm httrack \
  irc-otr irssi irssi-xmpp innotop indent imapsync ignuit iso-codes \
  ipcalc iptstate ipvsadm iperf iptraf-ng iodine icecream iotop \
  ipmiutil icecat josm jasper jhead jwhois klog kbackup kscope \
  kernel-devel kdevelop kate kicad krename krusader kpartx kanyremote \
  kid3 kdenlive keepalived knot k3b keepassxc kernel-tools kile \
  linsmith lpsk31 lha lz4 lzop ltrace luajit lcdtest lcov llvm lyx \
  lsyncd luminance-hdr lilypond link-grammar lynis lshw lsof lsscsi \
  logserial lrzsz linphone lftp lighttpd linkchecker links lynx \
  maatkit mariadb mysqltuner memtest86+ memtester mpage magic mairix \
  mc mmv mathomatic maxima mapserver mediainfo mpc mpd mup mlt mbrowse \
  mtr mosh mcrypt moreutils multitail mcelog monit minicom mktorrent \
  ncdu nilfs-utils ntfsprogs nfoview numlockx ncmpc nip2 nomacs \
  netplug netstat-nat nfdump ngrep nbtscan ndisc6 net-tools nmap \
  nmap-ncat nikto nwipe ntpdate nmon ncftp okular okteta offlineimap \
  openocd octave open-vm-tools openscad opus optipng openshot openvpn \
  osmo ots opensc openrdate phoronix-test-suite pipebench pidgin \
  pidgin-otr p7zip pbzip2 pigz pxz pg_top pgadmin3 pandoc par pstoedit \
  pcb pydf parted pcmanfm pmount pasystray pavucontrol picard pogo \
  perl-Image-ExifTool pngcrush pitivi pacemaker packETH pdsh pssh \
  paraview pari psad pwgen pcsc-tools psmisc powertop picocom qsstv \
  quassel qjson qt-creator qpdf qscintilla qelectrotech qhull qgis \
  qrencode qmmp qiv qterm qbittorrent rtl-sdr rtlsdr-scanner rsnapshot \
  rzip ragel rsync redshift rtmpdump rubberband rawstudio rinetd \
  remmina rednotebook radicale radamsa rkhunter rng-tools rpmlint \
  roxterm rekonq soundmodem splat scudcloud sendxmpp sqliteman snip \
  stgit sdcc stress sane-frontends scribus sigil simple-scan shed srm \
  star symlinks squashfs-tools speedcrunch sbd sparkleshare \
  sound-juicer sox speex sipcalc smokeping socat sslscan squid \
  sshuttle sslh skrooge safecopy scrub skipfish steghide schroot \
  screen screenfetch shutter solaar sshpass sysstat sysprof \
  smartmontools ser2net snownews surfraw trustedqsl tucnak2 twlog \
  timeline thunderbird tnef tree tracker timidity++ tvtime traceroute \
  ttcp tcpdump tcpreplay telnet tigervnc task taskcoach tpp tmpwatch \
  tmux trousers texlive texlive-calcage texlive-fancyhdr \
  texlive-fnumprint terminator tilda tilix twinkle unixcw uronode \
  unzip udis86 uncrustify umbrello unpaper unrtf ufraw uniconvertor \
  uade ucarp unbound usbip usbredir usbguard udisks uid_wrapper \
  unetbootin usbview vim-enhanced vlc viewnior vips vtk vkeybd vdr \
  vconfig vnstat vtun virt-viewer vpnc vym veusz vte wine workrave \
  wildmidi wireshark wodim wdiff wkhtmltopdf wget xastir xconvers \
  xfhell xlog xpsk31 xz xdelta xmlcopyeditor xcircuit xcowsay xmobar \
  xplanet xwax xmp xawtv xpra xca xdotool xsensors yarock \
  yubico-piv-tool yubikey-personalization-gui yubioath-desktop \
  yamllint youtube-dl zip zathura zfs-fuse zenity zanshin zzuf
```

[back to index](#index)

# Essential #

```
dnf install CutyCapt autoconf automake ack ccrypt clamav clamav-update \
  cryptsetup curl dcfldd fdupes freerdp gcc git graphviz geany \
  galculator gnome-tweaks httpie htop iotop kernel-devel kate k3b \
  keepassxc kernel-tools lsof lsscsi lftp mtr mosh multitail ncdu \
  net-tools nmap nmap-ncat nwipe ntpdate openvpn pbzip2 pigz pxz \
  pavucontrol pwgen powertop rsync squashfs-tools sipcalc socat solaar \
  traceroute tcpdump telnet tigervnc tmux tilix vim-enhanced vlc \
  virt-viewer wkhtmltopdf wget youtube-dl
```

[back to index](#index)

